# 张天才

## 2021.9.15

1. 文章阅读

- [JavaScript小面试~什么是深拷贝，什么是浅拷贝，深拷贝和浅拷贝的区别，如何实现深拷贝](https://blog.csdn.net/qq_31539817/article/details/120169348?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)
- [Vue 中过滤器的使用和定义](https://blog.csdn.net/chentony123/article/details/80934160)

2. 源码阅读

- [vue中vuex的详解](https://blog.csdn.net/fu983531588/article/details/89522446?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163158328616780269859680%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163158328616780269859680&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-89522446.first_rank_v2_pc_rank_v29&utm_term=vuex%E8%A7%A3%E6%9E%90&spm=1018.2226.3001.4187)

3. leetcode 刷题

- [2020最新-精选基础算法100题（面试必备）](https://blog.csdn.net/ATFWUS/article/details/106193067?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163126978016780261919759%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163126978016780261919759&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-106193067.first_rank_v2_pc_rank_v29&utm_term=%E7%AE%97%E6%B3%95%E9%A2%98&spm=1018.2226.3001.4187)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.14

1. 文章阅读

- [前端面试知识点（一）](https://juejin.cn/post/6987549240436195364)
- [Varlet-Cli | Vue3组件库快速成型工具](https://juejin.cn/post/7007635457936719909)

2. 源码阅读

- [redux源码解读](https://juejin.cn/post/6844903600456466445)

3. leetcode 刷题

- [最全的手写JS面试题](https://juejin.cn/post/6968713283884974088)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.13

1. 文章阅读

- [谈谈我这些年对前端框架的理解](https://juejin.cn/post/7007048306438176799)
- [原来 3D 感空间行星轨迹是这样画的](https://juejin.cn/post/7007203118798618661)

2. 源码阅读

- [Axios 源码解析](https://juejin.cn/post/6844903824583294984)

3. leetcode 刷题

- [面试题速记汇总版](https://juejin.cn/post/7002361841905041438)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.12

1. 文章阅读

- [JS 字符串方法（包含 ES6 新增方法）](https://blog.csdn.net/weixin_30847865/article/details/101166659)
- [Dom 树的加载过程](https://www.jianshu.com/p/11c8e36ff89e)

2. 源码阅读

- [mybatis 源码分析](https://juejin.cn/post/6992211565533478949)

3. leetcode 刷题

- [js 算法题目收集](https://juejin.cn/post/6995880908905512990)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.10

1. 文章阅读

- [JavaScript 小面试~什么是深拷贝，什么是浅拷贝，深拷贝和浅拷贝的区别，如何实现深拷贝](https://blog.csdn.net/qq_31539817/article/details/120169348?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)
- [❤️npm 常用命令以及 npm publish 常见问题处理方法 ❤️](https://blog.csdn.net/qq_32442973/article/details/120189060?utm_medium=distribute.pc_category.none-task-blog-hot-8.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-8.nonecase)

2. 源码阅读

- [二进制数的补码及运算](https://blog.csdn.net/edward_zcl/article/details/89449191?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163127809416780265412862%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163127809416780265412862&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-4-89449191.first_rank_v2_pc_rank_v29&utm_term=%E6%BA%90%E7%A0%81&spm=1018.2226.3001.4187)

3. leetcode 刷题

- [2020 最新-精选基础算法 100 题（面试必备）](https://blog.csdn.net/ATFWUS/article/details/106193067?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163126978016780261919759%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163126978016780261919759&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-106193067.first_rank_v2_pc_rank_v29&utm_term=%E7%AE%97%E6%B3%95%E9%A2%98&spm=1018.2226.3001.4187)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.9

1. 文章阅读

- [什么是回流，什么是重绘，有什么区别？](https://www.jianshu.com/p/e081f9aa03fb)
- [JavaScript 原型系列（三）Function、Object、null 等等的关系和鸡蛋问题](https://juejin.cn/post/6844903937418461198)

2. 源码阅读

- [React 源码解析](https://www.bilibili.com/video/BV1cE411B7by?from=search&seid=16211156106337678564&spm_id_from=333.337.0.0)

3. leetcode 刷题

- [几道 JS 代码手写题以及一些代码实现](https://juejin.cn/post/6844903575559077895)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.8

1. 文章阅读

- [Vue 中过滤器的使用和定义](https://blog.csdn.net/chentony123/article/details/80934160)
- [清除浮动的最常用的四种方法，以及优缺点](https://blog.csdn.net/h_qingyi/article/details/81269667)

2. 源码阅读

- [mybatis 源码分析](https://juejin.cn/post/6992211565533478949)

3. leetcode 刷题

- [前端常见算法的 JS 实现](https://juejin.cn/post/6844903466180018190)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.7

1. 文章阅读

- [史上最通俗易懂的 Promise](https://juejin.cn/post/6844903607968481287)
- [react+flexible 适配移动端项目的配置](https://juejin.cn/post/6951366350572879903)

2. 源码阅读

- [React 面试必知必会 Day12](https://juejin.cn/post/6975120676982095886)

3. leecode 刷题

- [Fizz Buzz](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xngt85/)
- [计数质数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnzlu6/)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

---

## 2021.9.6

1. 文章阅读

- [Vue 3.2 发布了](https://juejin.cn/post/6997943192851054606)

2. 源码阅读

- [Vue3 源码导读](https://juejin.cn/post/6844903957421096967)

3. leetcode 刷题

- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.5

1. 文章阅读

- [什么是跨域？跨域解决方法](https://blog.csdn.net/qq_38128179/article/details/84956552)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.3

1. 文章阅读

- [编码之道（一）：程序员的“圣经“](https://blog.csdn.net/taoofcode/article/details/120053996?utm_medium=distribute.pc_category.none-task-blog-hot-3.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-3.nonecase)
- [vue 项目中引入 vue-router](https://blog.csdn.net/qq_43277404/article/details/120053734?utm_medium=distribute.pc_category.none-task-blog-hot-16.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-16.nonecase)

2. 源码阅读

- [我所有的项目，源码全在这了！](https://blog.csdn.net/CSDNedu/article/details/112292930?ops_request_misc=&request_id=&biz_id=102&utm_term=%E6%BA%90%E7%A0%81&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-112292930.first_rank_v2_pc_rank_v29&spm=1018.2226.3001.4187)

3. leetcode 刷题

- [字节跳动算法题](https://blog.csdn.net/qq_28303495/article/details/100169594?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163067579116780262529184%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=163067579116780262529184&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v29_ecpm-28-100169594.first_rank_v2_pc_rank_v29&utm_term=%E7%AE%97%E6%B3%95%E9%A2%98&spm=1018.2226.3001.4187)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.2

1. 文章阅读

- [前端高效开发不得不知道的一些 JavaScript 库！](https://blog.csdn.net/qq_32442973/article/details/120004973?utm_medium=distribute.pc_category.none-task-blog-hot-5.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-5.nonecase)
- [JavaScript 进阶第八章（闭包）](https://blog.csdn.net/m0_56344602/article/details/120049242?utm_medium=distribute.pc_category.none-task-blog-hot-6.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-6.nonecase)

2. 源码阅读

- [二进制数的补码及运算](https://blog.csdn.net/edward_zcl/article/details/89449191?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163058975816780265440689%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163058975816780265440689&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-3-89449191.first_rank_v2_pc_rank_v29&utm_term=%E6%BA%90%E7%A0%81&spm=1018.2226.3001.4187)

3. leetcode 刷题

- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.9.1

1. 文章阅读

- [vuex 的基本概念](https://blog.csdn.net/MYmayue/article/details/119998812?utm_medium=distribute.pc_category.none-task-blog-hot-14.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-14.nonecase)
- [Cookie、Session、Token 和 JWT 的区别讲解](https://blog.csdn.net/qq_45488981/article/details/120007959?utm_medium=distribute.pc_category.none-task-blog-hot-18.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-18.nonecase)

2. 源码阅读

- [二进制(原码、反码、补码)](https://blog.csdn.net/dabing69221/article/details/17333743?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522163050312216780261923025%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=163050312216780261923025&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-4-17333743.first_rank_v2_pc_rank_v29&utm_term=%E6%BA%90%E7%A0%81%0A++++++++++++++++++++++++++&spm=1018.2226.3001.4187)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

4. 项目进度

- [x] 首页排版
- [x] 路由
- [x] 调取数据接口

## 2021.8.31

1. 文章阅读

- [Mobx 使用详解](https://www.jianshu.com/p/505d9d9fe36a)
- [切图仔？你知道 V8 是如何执行 JS 代码的吗？](https://blog.csdn.net/m0_50855872/article/details/119963754?utm_medium=distribute.pc_category.none-task-blog-hot-2.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-2.nonecase)

2. 源码阅读

- [Redux 中的 reducer 到底是什么，以及它为什么叫 reducer？](https://zhuanlan.zhihu.com/p/25863768)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

4. 项目进度

- [x] 首页排版
- [x] 路由

## 2021.8.30

1. 文章阅读

- [React SSR 服务器渲染原理解析与实践](https://juejin.cn/post/7001411229323362335)
- [React useEffect 使用指南](https://juejin.cn/post/7001441619123798029)

2. 源码阅读

- [React 源码解析](https://juejin.cn/post/6844903568487497741)

3. leetcode 刷题

- [如何准备 20K+的大厂前端面试](https://juejin.cn/post/6844904041915351048)

4. 项目进度

- [x] 首页排版
- [x] 路由

## 2021.8.28

1. 文章阅读

- [Pnpm: 最先进的包管理工具](https://juejin.cn/post/7001794162970361892)
- [一个"剑气"加载 🌪️](https://juejin.cn/post/7001779766852321287)

2. 源码阅读

- [VueRouter 源码分析](https://juejin.cn/post/6844903818203758600)

3. leetcode 刷题

- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)

4. 项目进度

- [x] 首页排版
- [x] 路由

## 2021.8.27

1. 文章阅读

- [16 个工程必备的 JavaScript 代码片段（建议添加到项目中）](https://juejin.cn/post/7000919400249294862)
- [前端游戏巨制! CSS 居然可以做 3D 游戏了](https://juejin.cn/post/7000963575573381134)

2. 源码阅读

- [写给初中级前端的高级进阶指南](https://juejin.cn/post/6844904103504527374)

3. leetcode 刷题

- [JS 闭包经典使用场景和含闭包必刷题](https://juejin.cn/post/6937469222251560990)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果
- [x] 详情跳阅读详情

## 2021.8.26

1. 文章阅读

- [连夜爆肝只为将它送到你的面前，写给初级前端快速转 TypeScript 指南](https://juejin.cn/post/7000610903615864869)
- [React 开发必须知道的 34 个技巧](https://juejin.cn/post/6844903993278201870)

2. 源码阅读

- [axios 核心源码解读](https://juejin.cn/post/6844903728412098574)

3. leetcode 刷题

- [codewars](https://www.codewars.com/dashboard)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果
- [x] 详情跳阅读详情

## 2021.8.25

1. 文章阅读

- [JS 字符串方法（包含 ES6 新增方法）](https://blog.csdn.net/weixin_30847865/article/details/101166659)
- [JS 中修改 this 指向的方法有哪些？](http://www.itcast.cn/news/20200914/17292015107.shtml)

2. 源码阅读

- [js 源码理解](https://www.jianshu.com/p/70464d787b47)

3. leetcode 刷题

- [JS 常见算法题目](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果
- [x] 详情跳阅读详情

## 2021.8.24

1. 文章阅读

- [css 粘性定位 position：sticky 问题采坑](https://blog.csdn.net/qq_35585701/article/details/81040901)
- [Hooks](https://www.jianshu.com/p/89f2cf94a7c2)

2. 源码阅读

- [github1s: 1 秒极速用 VS Code 阅读 GitHub 源代码](https://juejin.cn/post/6986860544972029959)

3. leetcode 刷题

- [十道简单算法题](https://juejin.cn/post/6844903585445052430)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果
- [x] 详情跳阅读详情

## 2021.8.23

1. 文章阅读

- [React Hooks 钩子函数的使用](https://blog.csdn.net/shiningchen322/article/details/110136802)
- [vue 中的 methods,watch 和 computer 区别](https://blog.csdn.net/zhouzy539/article/details/96340814?utm_term=vue%E7%9A%84computer%E5%B1%9E%E6%80%A7&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-0-96340814&spm=3001.4430)

2. 源码阅读

- [redux 源码解读](https://juejin.cn/post/6844903600456466445)

3. leetcode 刷题

- [刷了 1000 多道算法题，一点心得](https://juejin.cn/post/6960639432139964446)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果
- [x] 详情跳阅读详情

## 2021.8.22

1. 文章阅读

- [彻底弄懂 CSS 必考题](https://juejin.cn/post/6999160403066355743)
- [浏览器 Location 对象，URL 对象，URLSearchParams 对象简析](https://juejin.cn/post/6999077663587434533)

2. 源码阅读

- [JS 数组 API 源码浅析](https://juejin.cn/post/6844903568634462221)

3. leetcode 刷题

- [🌲 树+8 道前端算法面试高频题解](https://juejin.cn/post/6938385018267893767)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果
- [x] 详情跳阅读详情

## 2021.8.20

1. 文章阅读

- [一文搞懂 CSS 中的字体单位大小(px,em,rem...)](https://juejin.cn/post/6844903897421578253)
- [了解 JavaScript 的递归](https://juejin.cn/post/6844903584027394061)

2. 源码阅读

- [redux 源码小结](https://juejin.cn/post/6994393869957726215)

3. leetcode 刷题

- [用 JavaScript 刷 LeetCode 的正确姿势](https://juejin.cn/post/6844903876206805005)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果

## 2021.8.19

1. 文章阅读

- [一文搞定 echarts 地图轮播高亮 ⚡](https://juejin.cn/post/6997978246839042079)
- [居然不知道 CSS 能做 3D？天空盒子了解一下，颠覆想象 👽](https://juejin.cn/post/6997697496176820255)

2. 源码阅读

- [Vue 3 源码导读](https://juejin.cn/post/6844903957421096967)

3. leetcode 刷题

- [寻找两个有序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/solution/xun-zhao-liang-ge-you-xu-shu-zu-de-zhong-wei-s-114/)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果

## 2021.8.18

1. 文章阅读

- [关于 HTTP 和 HTTPS 的区别](https://mp.weixin.qq.com/s/UE7Zw0aSbxLuFFSraSUIOQ)
- [http3 次握手和 4 次挥手](https://juejin.cn/post/6997673511149895711)

2. 源码阅读

- [Axios 源码解析](https://juejin.cn/post/6844903824583294984)

3. leetcode 刷题

- [反转链表](https://leetcode-cn.com/problems/reverse-linked-list/solution/fan-zhuan-lian-biao-by-leetcode-solution-d1k2/)

4. 项目进度

- [x] 首页排版
- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果

## 2021.8.17

1. 文章阅读

- [彻底理解浏览器的跨域](https://juejin.cn/post/6844903816060469262)
- [umijs 的国际化——中英文转换](https://juejin.cn/post/6981327610097696775)

2. 源码阅读

- [React - setState 源码分析](https://juejin.cn/post/6844903573453537287)

3. leetcode 刷题

- [LeetCode 算法题刷题](https://www.jianshu.com/p/8876704ea9c8)

4. 项目进度

- [x] 首页排版

## 2021.8.16

1. 文章阅读

- [React 入门笔记（一）-- 基础知识以及 jsx 语法](https://juejin.cn/post/6996316715525079076)
- [基础很好？22 个高频 JavaScript 手写代码总结了解一下](https://juejin.cn/post/6996289669851774984)

2. 源码阅读

- [Vue 源码解读（1）—— 前言](https://juejin.cn/post/6949370458793836580)

3. leetcode 刷题

- [社招面经总结——算法题篇](https://juejin.cn/post/6844903814831538183)

4. 项目进度

- [x] 首页排版

## 2021.8.15

1. 文章阅读

- [图解 React 源码 - Hook 原理(状态 Hook)](https://juejin.cn/post/6996559213577109534)
- [你真的弄懂 React 了吗？](https://juejin.cn/post/6996478115488727053)

2. 源码阅读

- [从未看过源码，到底该如何入手？分享一次完整的源码阅读过程](https://juejin.cn/post/6922616138169843725)

3. leetcode 刷题

- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)

4. 项目进度

- 无

## 2021.8.13

1. 文章阅读

- [react 框架总结](https://blog.csdn.net/halations/article/details/84389261?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162884213416780261994666%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162884213416780261994666&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-84389261.pc_search_result_cache&utm_term=react&spm=1018.2226.3001.4187)
- [ES6 模块化与异步编程高级用法总结](https://blog.csdn.net/qq_45337939/article/details/119605169?utm_medium=distribute.pc_category.none-task-blog-hot-1.nonecase&depth_1-utm_source=distribute.pc_category.none-task-blog-hot-1.nonecase)

2. 源码阅读
3. leetcode 刷题

- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)

4. 项目进度

- 无

## 2021.8.12

1. 文章阅读

- [Git 命令]()
- [UmiJS](https://umijs.org/zh-CN/docs/getting-started)

2. 源码阅读
3. leetcode 刷题

- [两数之和](https://leetcode-cn.com/problems/two-sum/solution/)

4. 项目进度

- 无

## 2021.8.11

1. 文章阅读

- [md 文件的编辑](https://blog.csdn.net/weishuai528/article/details/83827459)

2. 源码阅读。

3. leetcode 刷题

4. 项目进度
<!--

- [x] 班级管理页面布局
- [x] 添加班级 删除班级
  - [x] 教室管理页面布局
- [x] 添加教室号 删除教室
- [x] 学生管理页面布局 -->

---

# 管学鹏

## 2021.9.14

1. 文章阅读

- [JavaScript的堆栈和数据类型介绍以及如何使用递归去实现一个引用数据类型的深拷贝](https://blog.csdn.net/KrisZ2573136399/article/details/119814567)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1013/)

---

## 2021.9.13

1. 文章阅读

- [uniapp 官网](https://uniapp.dcloud.io/quickstart-cli)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

---

## 2021.9.12

1. 文章阅读

- [chrome](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [反转链表](https://leetcode-cn.com/leetbook/read/tencent/x5xg2m/)


## 2021.9.10

1. 文章阅读

- [uni-app 小程序手把手项目实战](https://juejin.cn/post/6844903999984893966)

2. 源码阅读

- [uniapp](https://uniapp.dcloud.io/)

3. leecode 刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

## 2021.9.9

1. 文章阅读

- [必须要会的 50 个 React 面试题](https://juejin.cn/post/6844903806715559943#heading-3)

2. 源码阅读

- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/utils/mockHooks.js)

3. leecode 刷题

- [除自身以外数组的乘积](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

## 2021.9.8

1. 文章阅读

- [TCP 三次握手、四次挥手的理解及面试题（图解过程）](https://blog.csdn.net/sinat_41144773/article/details/88314735)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
- [验证回文字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)

## 2021.9.2

1. 文章阅读

- [umijs 官网](https://umijs.org/zh-CN/docs/load-on-demand)
- [hooks 官网](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

2. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leecode 刷题

- 无

4. 项目进度

- [x] 评论管理

## 2021.8.31

1. 文章阅读

- [复习浏览器缓存总结语雀笔记](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E6%A6%82%E5%BF%B5)
- [Es6 新增的字符串方法](https://blog.csdn.net/weixin_30847865/article/details/101166659)

2. 源码阅读

- [Hash](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/Hash.md)
- [堆](https://github.com/jasonandjay/js-code/blob/master/algorithm/heap.js)

3. leecode 刷题

- [统计字符串中出现最多的字母](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)

4. 项目进度

- [x]评论管理

## 2021.8.30

1. 文章阅读

- [Dvn 官网](https://dvajs.com/guide/#%E5%91%BD%E5%90%8D%E7%94%B1%E6%9D%A5%EF%BC%9F)

2. 源码阅读

- [React 的数据结构](https://react.jokcy.me/book/api/react-structure.html)
- [React.render](https://react.jokcy.me/book/update/react-dom-render.html)

3. leecode 刷题

- [统计字符串中出现最多的字母](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)

4. 项目进度

- [x] 分配项目

## 2021.8.25

1. 文章阅读

- [应该了解前端监控和埋点](https://juejin.cn/post/6844904130163507214)
- [前端埋点统计方案思考](https://juejin.cn/post/6844903741775151111)

1. 源码阅读

- 无

3. leetcode 刷题

- [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/)
-

4. 项目进度

- [x] 精细排版

## 2021.8.24

1. 文章阅读

- [「用前端重返童年 🥤」为黑神话悟空定制红白机版游戏开始动画](https://juejin.cn/post/6999413377692860430)
- [我用 index 作为 key 也没啥问题啊](https://juejin.cn/post/6999932053466644517)

1. 源码阅读

- 无

3. leetcode 刷题

- - [字符串的排列](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1016/)
-

4. 项目进度

- [x] 精细排版

## 2021.8.23

1. 文章阅读

- [react-color](https://www.npmjs.com/package/react-color)
- [viewerjs](https://www.npmjs.com/package/viewerjs)

1. 源码阅读

- 无

3. leetcode 刷题

- [两数相加](https://leetcode-cn.com/problems/add-two-numbers/)
-

4. 项目进度

- [x] 搜索页面的排版和效果

## 2021.8.19

1. 文章阅读

- [跨域](https://jasonandjay.github.io/study/zh/book/Ajax%E7%AF%87.html#%E5%A6%82%E4%BD%95%E8%A7%A3%E5%86%B3%E8%B7%A8%E5%9F%9F%E9%97%AE%E9%A2%98)
- [hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

1. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leetcode 刷题

- [排列序列](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1021/)

4. 项目进度

- [x] 搜索页面的排版和效果

## 2021.8.18

1. 文章阅读

- [umi 入门和一些使用心得](https://blog.csdn.net/wangweiren_get/article/details/85987336)
- [前端人的成长可以有多快？](https://zhuanlan.zhihu.com/p/113461692)

1. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leetcode 刷题

- [x 的平方根](https://leetcode-cn.com/explore/interview/card/bytedance/247/bonus/1045/)

4. 项目进度

- [x] 详情页面排版

## 2021.8.17

1. 文章阅读

- [好的前端的学习方法](https://www.zhihu.com/question/322164361/answer/694544292)
- [集中注意力](https://zhuanlan.zhihu.com/p/30477725)

1. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leetcode 刷题

- [字符串的排列](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1016/)

4. 项目进度

- [x] 详情页面排版

## 2021.8.16

1. 文章阅读

- [React Hooks,彻底颠覆 React,它的未来应该是这样的](https://www.jianshu.com/p/7e778adec7d1)
- [Hooks 入门教程](http://www.ruanyifeng.com/blog/2019/09/react-hooks.html)

1. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leetcode 刷题

- [排列序列](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1021/)

4. 项目进度

- [x] 项目排版

## 2021.8.15

1. 文章阅读

- [掘金小册 —— chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [Umi 官网](https://umijs.org/zh-CN/docs/directory-structure)

2. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leetcode 刷题

- [字符串相乘](https://blog.csdn.net/weixin_40920953/article/details/103362401)

4. 项目进度

- [x] 首页排版

---

## 2021.8.13

1. 文章阅读

- [前端劝退知识体系](https://juejin.cn/post/6994657097220620319)
- [2021 年我的前端面试准备](https://juejin.cn/post/6989422484722286600)

2. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leetcode 刷题

- [字符串的排列](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1016/)

4. 项目进度

- [x] 首页排版

---

## 2021.8.12

1. 文章阅读

- [umijs 的国际化——中英文转换](https://juejin.cn/post/6981327610097696775)
- [2020 最后一篇技术文：可爱的乌咪 UmiJS](https://juejin.cn/post/6912239567978528782)

2. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leecode 刷题

- [搜索旋转排序数组](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1017/)

4. 项目进度

- 无

---

## 2021.8.11

1. 文章阅读

- [20 分钟吃透 Diff 算法核心原理，我说的！！！](https://juejin.cn/post/6994959998283907102)
- [es6 解构赋值 [a,b] = [b,a]的几个问题](https://juejin.cn/post/6994634734634696734)

2. 源码阅读

- 无

3. leecode 刷题

- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4. 项目进度

- 无

---

# 王姣姣

## 2021.9.13

1. 文章阅读

- [vue](https://juejin.cn/post/6961222829979697165#heading-7)

2. 源码阅读

- vue源码阅读

3. leetCode 刷题

- [只出现一次数字](https://leetcode-cn.com/problems/add-two-numbers/)

4. 项目进度

- [x] 路由导航
- [x] 分配项目

1. 文章阅读

- [微信小程序文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)

2. 源码阅读

- 无

3. leetCode 刷题

- [只出现一次数字](https://leetcode-cn.com/problems/add-two-numbers/)

4. 项目进度

- [x] 导入文件
- [x] 构建路由

## 2021.9.12

1. 文章阅读

- [微信小程序文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)

2. 源码阅读

- 无

3. leetCode 刷题

- [只出现一次数字](https://leetcode-cn.com/problems/add-two-numbers/)

4. 项目进度

- 无

## 2021.9.10

1. 文章阅读

- [闭包](https://jasonandjay.github.io/study/zh/book/JavaScript%E7%AF%87.html)

2. 源码阅读

- 无

3. leetCode 刷题

- [只出现一次数字](https://leetcode-cn.com/problems/add-two-numbers/)

4. 项目进度

- [x] 个人中心数据点击图片浏览图片
- [x] 面包屑的实现

## 2021.9.9

1. 文章阅读

- [react 的生命周期](https://juejin.cn/post/6844903510538977287)

## 2021.9.8

1. 文章阅读

- [性能优化](https://juejin.cn/post/6844904116339261447#heading-32)

2. 源码阅读

- 无

3. leetCode 刷题

- [搜索旋转排序数组](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1017/)

4. 项目进度

- [x] 评论管理的页面高度还原
- [x] 前端发送邮件

2. 源码阅读

- 无

3. leetCode 刷题

- [搜索旋转排序数组](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1017/)

4. 项目进度

- [x] 所有文章的搜索功能
- [x] 渲染标签分类的页面
- [x] 发送邮件

## 2021.9.1

1. 文章阅读

- [闭包](https://jasonandjay.github.io/study/zh/book/JavaScript%E7%AF%87.html)

2. 源码阅读

- 无

3. leetCode 刷题

- [只出现一次数字](https://leetcode-cn.com/problems/add-two-numbers/)

4. 项目进度

- [x] 个人中心数据点击图片浏览图片
- [x] 面包屑的实现

## 2021.8.31

1. 文章阅读

- [typescript 的基础](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读

- 无

3. leetCode 刷题

- [只出现一次数字](https://leetcode-cn.com/problems/add-two-numbers/)

4. 项目进度

- [x] 个人中心排版
- [x] 实现个人中心抽屉功能
- [x] 实现登录注册功能

## 2021.8.30

1. 文章阅读

- [iconfont 图标的使用](https://www.jianshu.com/p/514fe21b9914)
- [文件上传](https://juejin.cn/post/7002041657189203999)

2. 源码阅读

- 无

3. leetCode 刷题

- [检测大写字母](https://leetcode-cn.com/problems/detect-capital/submissions/)

4. 项目进度

- [x] 书写不同页面
- [x] 实现登录功能

## 2021.8.29

1. 文章阅读

- [防抖节流](https://www.yuque.com/heinan/luckbody/ndupzi)

2. 源码阅读

- 无

3. leetCode 刷题

- [检测大写字母](https://leetcode-cn.com/problems/detect-capital/submissions/)

4. 项目进度

- [x] 搭建框架
- [x] 约定是路由

## 2021.8.27

1. 文章阅读

- [闭包](https://jasonandjay.github.io/study/zh/book/JavaScript%E7%AF%87.html)
- [前端开发规范](https://www.yuque.com/heinan/luckbody/opflt6)

2. 源码阅读

- react

3. leetCode 刷题

- [x 的平方根](https://leetcode-cn.com/explore/interview/card/bytedance/247/bonus/1045/)

4. 项目进度
   无

## 2021.8.26

1. 文章阅读

- [答辩流程](https://www.yuque.com/heinan/luckbody/rqu3b1)
- [如何更好的规划组件](https://www.yuque.com/heinan/luckbody/xqxvf1)

2. 源码阅读

- vue

3. leetCode 刷题

- [搜索旋转排序数组](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1017/)

4. 项目进度

无

## 2021.8.25

1. 文章阅读

- [hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
- [浏览器缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html)

2. 源码阅读

- react

3. leetCode 刷题

- [搜索旋转排序数组](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1017/)

4. 项目进度

- [x] 点击分享页面跳转
- [x] 主题变化
- [x] 优化代码

## 2021.8.24

1. 文章阅读

- [hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
- [viewerjs](https://www.npmjs.com/package/viewerjs)

2. 源码阅读

- react

3. leetCode 刷题

- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4. 项目进度

- [x] 点击分享页面跳转
- [x] 主题变化
- [x] 中汉互译

## 2021.8.23

1. 文章阅读

- [hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
- [HTML 篇](https://jasonandjay.github.io/study/zh/book/HTML%E7%AF%87.html)

2. 源码阅读

- react

3. leetCode 刷题

- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4. 项目进度

- [x] 详情页加载图片浏览
- [x] 进度条的实现缓冲页面
- [x] 拆分推荐阅读组件
- [x] 详情页排版
- [x] 导航详情页跳转详情
- [x] 轮播图跳详情
- [x] 点击分享页面跳转

## 2021.8.22

1. 文章阅读

- [Umijs](https://umijs.org/zh-CN/plugins/preset-react)
- [HTML 篇](https://jasonandjay.github.io/study/zh/book/HTML%E7%AF%87.html)

2. 源码阅读

- react

3. leetCode 刷题

- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4. 项目进度

- [x] 详情页加载图片浏览
- [x] 进度条的实现缓冲页面
- [x] 拆分推荐阅读组件
- [x] 详情页排版
- [x] 导航详情页跳转详情
- [x] 轮播图跳详情

## 2021.8.20

1. 文章阅读

- [Umijs](https://umijs.org/zh-CN/plugins/preset-react)
- [HTML 篇](https://jasonandjay.github.io/study/zh/book/HTML%E7%AF%87.html)

2. 源码阅读

- react

3. leetCode 刷题

- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4. 项目进度

- [x] 详情页加载图片浏览
- [x] 进度条的实现缓冲页面
- [x] 拆分推荐阅读组件
- [x] 详情页排版
- [x] 导航详情页跳转详情

## 2021.8.19

1. 文章阅读

- [掘金小册 —— chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [HTML 篇](https://jasonandjay.github.io/study/zh/book/HTML%E7%AF%87.html)

2. 源码阅读

- vue

3. leetCode 刷题

- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4. 项目进度

- [x] 详情页加载图片浏览
- [x] 进度条的实现缓冲页面
- [x] 拆分推荐阅读组件
- [x] 详情页排版

1. 文章阅读

- [掘金小册 —— chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [Hooks 篇](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

2. 源码阅读

- vue

3. leetCode 刷题

- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4. 项目进度

- [x] 详情页加载图片浏览
- [x] 进度条的实现缓冲页面

## 2021.8.17

1. 文章阅读

- [Umijs](https://umijs.org/zh-CN/plugins/preset-react)
- [跨域](https://jasonandjay.github.io/study/zh/interview/#%E7%BD%91%E7%BB%9C)

2. 源码阅读
   无
3. leetcode 刷题

- [搜索旋转排序数组](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1017/)

4. 项目进度

- [x] 文章页面左右排版
- [x] 实现轮播图效果
- [x] 列表数据跳详情
- [x] 右侧数据滑动高亮效果

## 2021.8.16

1. 文章阅读

- [Umijs](https://umijs.org/zh-CN/plugins/preset-react)
- [跨域](https://jasonandjay.github.io/study/zh/interview/#%E7%BD%91%E7%BB%9C)

2. 源码阅读
   无
3. leetcode 刷题

- [搜索旋转排序数组](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1017/)

4. 项目进度

- [x] 文章页面左右排版
- [x] 实现轮播图效果

## 2021.8.15

1. 文章阅读

- [Umijs](https://umijs.org/zh-CN/plugins/preset-react)

2. 源码阅读

- 无

3. leetCode

- 无

4. 项目进度

- [x] 项目路由

## 2021.8.13

1. 文章阅读

- [掘金小册 —— chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [HTML 篇](https://jasonandjay.github.io/study/zh/book/HTML%E7%AF%87.html)

2. 源码阅读

- 无

3. leetcode 刷题

- 两数之和
- 删除排序数组中的重复项

4. 项目进度

- [x] 文章首页排版

## 2021.8.12

1. 文章阅读

- [掘金小册 —— chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [HTML 篇](https://jasonandjay.github.io/study/zh/book/HTML%E7%AF%87.html)

2. 源码阅读

- 无

3. leetcode 刷题

- 两数之和

4. 项目进度
   无

## 2021.8.11

1. 文章阅读

- [掘金小册 —— chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [javaScript 篇](https://jasonandjay.github.io/study/zh/book/JavaScript%E7%AF%87.html)

2. 源码阅读

3. leetcode 刷题
4. 项目进度

- [x] 安装 git，学习命令，配置 git 环境
- [x] 创建分支
- [x] 合并小组代码

---

# 梁杉杉

## 2021.9.15

1. 文章阅读

- [微信小程序文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)
2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [除自身以外数组的乘积](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

4. 项目进度

- [x] 获取首页tab切换的内容


---
---



## 2021.9.14

1. 文章阅读

- [vuex](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [几道 JS 代码手写题以及一些代码实现](https://juejin.cn/post/6844903575559077895)

---


## 2021.9.13

1. 文章阅读

- [uniapp 官网](https://uniapp.dcloud.io/quickstart-cli)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

---

## 2021.9.12

1. 文章阅读

- [chrome](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [反转链表](https://leetcode-cn.com/leetbook/read/tencent/x5xg2m/)

---

## 2021.9.10

1. 文章阅读

- [阿里云](https://help.aliyun.com/document_detail/32070.html?spm=5176.87240.400427.55.781c4614ZXiCBz)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [大写字母](https://leetcode-cn.com/problems/detect-capital/submissions/)

---

## 2021.9.9

1. 文章阅读

- [掘金](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [只出现一次数字](https://leetcode-cn.com/problems/add-two-numbers/)

4. 项目进度

- [x] 知识小册排版
- [x] 知识小册上传草稿切换

---

## 2021.9.8

1. 文章阅读

- [nodemailer](https://www.npmjs.com/package/nodemailer)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- - [洗牌算法](https://www.jianshu.com/p/ba055b157de9)

4. 项目进度

- [x] 知识小册删除
- [x] 文件页图片放大
- [x] 发送邮件 后端

---

## 2021.9.3

1. 文章阅读

- [mobx](https://cn.mobx.js.org/)
- [umijs](https://umijs.org/zh-CN/docs)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- 无

4. 项目进度

- [x] 文件页的的搜索和重置
- [x] 知识小册的的搜索和重置

---

## 2021.9.2

1. 文章阅读

- [mobx](https://cn.mobx.js.org/)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [有效的括号](https://leetcode-cn.com/leetbook/read/tencent/xx1d8v/)

4. 项目进度

- [x] 获取知识小册的数据
- [x] 知识小册的重置
- [x] 文件页图片放大

---

## 2021.9.1

1. 文章阅读

- [antd 官网](https://ant.design/components/upload/)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [计算一个整数的阶乘](https://www.jianshu.com/p/669cf5c289a0)

4. 项目进度

- [x] 点击文件管理页的数据出现抽屉
- [x] 获取抽屉里的数据

---

## 2021.8.31

1. 文章阅读

- [jquery 官网](https://jquery.cuishifeng.cn/)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [只出现一次的数字](https://blog.csdn.net/cw1254332663/article/details/104962294)

4. 项目进度

- [x] 获取文件管理页的数据
- [x] 排版 文件管理页

---

## 2021.8.30

1. 文章阅读

- [mobx 官网](https://www.npmjs.com/package/mobx)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [随机生成指定长度的字符串](https://www.jianshu.com/p/669cf5c289a0)

4. 项目进度

- [x] 文件管理页面的上传
- [x] 封装上传组件

---

## 2021.8.29

1. 文章阅读

- [路由](https://umijs.org/zh-CN/docs/convention-routing)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [数组的最大差值](https://www.jianshu.com/p/669cf5c289a0)

4. 项目进度

- [x] 配置路由
- [x] 重定向路由

---

## 2021.8.27

1. 文章阅读

- [防抖节流](https://jasonandjay.github.io/study/zh/interview/threeYear.html#%E9%98%B2%E6%8A%96%E5%92%8C%E8%8A%82%E6%B5%81)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [字符串的排列](https://leetcode-cn.com/explore/interview/card/bytedance/242/string/1016/)

4. 项目进度

- [x] 无

---

## 2021.8.26

1. 文章阅读

- [页面接口调用](https://www.yuque.com/chenqiu/alipay-node-sdk/page_api)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- 删除排序数组中的重复项

4. 项目进度

- [x] 支付宝支付

---

## 2021.8.25

1. 文章阅读

- [百度](https://umijs.org/zh-CN/plugins/plugin-analytics)
- [浏览器的缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

4. 项目进度

- [x] 归档详情页左侧滚动，目录高亮

---

## 2021.8.24

1. 文章阅读

- [base](https://umijs.org/zh-CN/config#base)
- [publicPath](https://umijs.org/zh-CN/config#publicpath)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

4. 项目进度

- [x] 归档详情右侧吸顶
- [x] 归档详情左侧滚动到相应位置

---

## 2021.8.23

1. 文章阅读

- [答辩流程](https://www.yuque.com/heinan/luckbody/rqu3b1)

2. 源码阅读

- [react](https://mp.weixin.qq.com/s/aqszbRAFXK5KA9x374Todg)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

4. 项目进度

- [x] 封装组件，精细排版
- [x] 回到顶部

---

## 2021.8.22

1. 文章阅读

- [plugin-locale](https://umijs.org/zh-CN/plugins/plugin-locale)

2. 源码阅读

- 无

3. leecode 刷题

- [三数之和](https://leetcode-cn.com/explore/interview/card/bytedance/243/array-and-sorting/1020/)

4. 项目进度

- [x] 中英文切换

---

## 2021.8.20

1. 文章阅读

- [react-color](https://www.npmjs.com/package/react-color)
- [viewerjs](https://www.npmjs.com/package/viewerjs)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- [字符串反序](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)

4. 项目进度

- [x] 封装组件
- [x] 详细排版
- [x] 获取归档详情目录数据
- [x] 渲染归档详情目录
- [x] 归档详情目录加高亮

---

## 2021.8.18

1. 文章阅读

- [hooks 官网](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

2. leecode 刷题

- [统计字符串中出现最多的字母](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)

4. 项目进度

- [x] 小楼又清风底部排版
- [x] 底部高亮跳转
- [x] 归档页面右侧排版
- [x] 归档页面右侧高亮,跳详情

---

## 2021.8.17

1. 文章阅读

- [跨域](https://jasonandjay.github.io/study/zh/book/Ajax%E7%AF%87.html#%E5%A6%82%E4%BD%95%E8%A7%A3%E5%86%B3%E8%B7%A8%E5%9F%9F%E9%97%AE%E9%A2%98)
- [hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leecode 刷题

- 无

4. 项目进度

- [x] 使用 post 传参
- [x] 获取归档详情页的数据
- [x] 归档页面跳转详情

---

## 2021.8.16

1. 文章阅读

- [umijs 官网](https://umijs.org/zh-CN/docs/load-on-demand)
- [hooks 官网](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

2. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leecode 刷题

- 无

4. 项目进度

- [x] 获取小楼又清风归档页面的数据
- [x] 小楼又清风的归档页面排版
- [x] 小楼又清风的归档页面的高亮

---

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leecode 刷题

- 无

4. 项目进度

- [x] 写小楼又清风路由
- [x] 小楼又清风的首页排版

---

## 2021.8.15

1. 文章阅读

- [整合 antd 组件库](https://umijs.org/zh-CN/plugins/plugin-antd)
- [整合 dva 数据流](https://umijs.org/zh-CN/plugins/plugin-dva)
- [request](https://umijs.org/zh-CN/plugins/plugin-request)

2. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leecode 刷题

- 无

4. 项目进度

- [x] 写小楼又清风路由
- [x] 小楼又清风的首页排版

---

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leecode 刷题

- 无

4. 项目进度

- [x] 写小楼又清风路由
- [x] 小楼又清风的首页排版

---

## 2021.8.13

1. 文章阅读

- [整合 antd 组件库](https://umijs.org/zh-CN/plugins/plugin-antd)
- [整合 dva 数据流](https://umijs.org/zh-CN/plugins/plugin-dva)
- [request](https://umijs.org/zh-CN/plugins/plugin-request)

2. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leecode 刷题

- 无

4. 项目进度

- [x] 下载 yarn
- [x] 使用 hooks 搭建结构目录
- [x] 获取小楼又清风主页数据

---

## 2021.8.12

1. 文章阅读

- [hooks 简介](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
- [umijs 官网](https://umijs.org/zh-CN/docs/load-on-demand)

2. 源码阅读

- 无

3. leecode 刷题

- 无

4. 项目进度

- [x] umijs 应用
- [x] 创建 tapd 平台
- [x] hooks 加减应用

---

## 2021.8.11

1. 文章阅读

- [知识点总结](https://jasonandjay.github.io/study/)
- [前端面试题](https://jasonandjay.github.io/study/zh/book/)

2. 源码阅读

- [react](https://reactjs.bootcss.com/docs/hooks-intro.html)

3. leecode 刷题

4. 项目进度

- [x] git 指令
- [x] 连接仓库
- [x] 创建公钥

---

# 仝健华
## 2021.9.15

1. 文章阅读

- [axios为什么要进行二次封装](https://blog.csdn.net/qq_29236119/article/details/115328112)

2. 源码阅读

3. leecode 刷题

- [如何实现顺序数组的随机排序？](http://www.manongjc.com/article/80487.html)

4. 项目进度

- [x] 特惠头部搜索布局
## 2021.9.14

1. 文章阅读

- [vue  插件](https://blog.csdn.net/qq_43363884/article/details/103783669)
- [vue 方法](https://www.cnblogs.com/qjuly/p/8521778.html)

2. 源码阅读

3. leecode 刷题

- [如何实现顺序数组的随机排序？](http://www.manongjc.com/article/80487.html)


## 2021.9.14

1. 文章阅读

- [ vuex  插件](https://blog.csdn.net/qq_43363884/article/details/103783669)
- [vue 方法有什么](https://www.cnblogs.com/qjuly/p/8521778.html)

2. 源码阅读

3. leecode 刷题

- [数组排序](http://www.manongjc.com/article/80487.html)

- [数组按照中间高的方法](http://www.manongjc.com/article/80487.html)
## 2021.9.13

1. 文章阅读

- [vue 中 vuex 的 Logger 插件](https://blog.csdn.net/qq_43363884/article/details/103783669)
- [vue 的变异方法是什么](https://www.cnblogs.com/qjuly/p/8521778.html)

2. 源码阅读

3. leecode 刷题

- [如何实现顺序数组的随机排序？](http://www.manongjc.com/article/80487.html)

- [实现数组按照中间高两边低进行排序的方法？](http://www.manongjc.com/article/80487.html)

## 2021.9.12

1. 文章阅读

- [vue 中 data 为什么放在函数里](https://blog.csdn.net/weixin_30423977/article/details/97082129)
- [uni](https://uniapp.dcloud.io/quickstart-cli)

2. 源码阅读

3. leecode 刷题

- [实现斐波那契数列](http://www.manongjc.com/article/80487.html)

- [翻转一个给定字符串](http://www.manongjc.com/article/80487.html)

## 2021.9.10

1. 文章阅读

- [回顾 hooks](https://www.jianshu.com/p/89f2cf94a7c2)
- [uni](https://uniapp.dcloud.io/quickstart-cli)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

## 2021.9.9

1. 文章阅读

- [谈谈对 html5 的理解](https://zhidao.baidu.com/question/1047308391217551739.html)

- [typeof null 为什么是 object](https://juejin.cn/post/7005402640746020877)

- [重绘和回流](https://www.jianshu.com/p/e081f9aa03fb)

2. 源码阅读

3. leecode 刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)

## 2021.9.8

1. 文章阅读

- [在浏览器输入 url 后发生了什么](https://www.jianshu.com/p/7eea6fbc5fcd)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

- [多维数组的扁平化 flat](https://www.jb51.net/article/194713.htm)

## 2021.9.7

1. 文章阅读

- [响应式网站用什么做(响应式网站并不适合市面上所有类型公司！)](https://www.xiezuoshe.com/seo/15839.html)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

## 2021.9.6

1. 文章阅读

- [详解三次握手和四次挥手](https://blog.csdn.net/su_bao/article/details/80845246)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

## 2021.9.4

1. 文章阅读

- [什么是前端工程化？](https://zhuanlan.zhihu.com/p/344571908)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

## 2021.9.3

1. 文章阅读

- [next 预习](https://nextjs.frontendx.cn/docs/#%E9%97%AE%E7%AD%94)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

## 2021.9.2

1. 文章阅读

- [MYSQL 相比于其他数据库有哪些特点？](https://blog.csdn.net/qq_34107571/article/details/77196327)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
- [验证回文字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)

## 2021.9.1

1. 文章阅读

- [TCP 三次握手、四次挥手的理解及面试题（图解过程）](https://blog.csdn.net/sinat_41144773/article/details/88314735)

2. 源码阅读

- [useeffect](https://reactjs.bootcss.com/docs/hooks-effect.html)

3. leecode 刷题

- [最长公共前缀](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnmav1/)

4. 项目进度

- [x] 邮箱页面
- [x] PPT 介绍

---

## 2021.8.31

1. 文章阅读

- [理解 mobx](https://zhuanlan.zhihu.com/p/22406271)

2. 源码阅读

3. leecode 刷题

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
- [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)

4. 项目进度

- [x] 邮箱页面 警告框 跳路由
- [x] 访问统计
- [x] 用户管理

---

## 2021.8.30

1. 文章阅读

- [修改属性中的颜色和字体大小](https://www.cnblogs.com/art-poet/p/13570409.html)

2. 源码阅读

3. leecode 刷题

- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

4. 项目进度

- [x] 邮箱页面表格

---

## 2021.8.29

1. 文章阅读

- [React 进行优化的生命周期 shouldComponentupdata](https://segmentfault.com/a/1190000006254212)

2. 源码阅读

3. leecode 刷题

- [Mother's Milk](https://www.papamelon.com/problem/38)

4. 项目进度

- [x] 新增后台管理系统任务

---

## 2021.8.27

1. 文章阅读

- [伪类元素伪类选择器](https://www.jianshu.com/p/8b610fdf0d48)
- [react 进阶](https://www.cnblogs.com/idiv/p/8442060.html)

2. 源码阅读

3. leecode 刷题

- [二维数组递增](https://blog.csdn.net/qq_40707033/article/details/82748198)

4. 项目进度

---

## 2021.8.26

1. 文章阅读

- [webpack 热加载原理](https://blog.csdn.net/sinat_17775997/article/details/83662133)
- [webpack 按需加载原理](https://www.jianshu.com/p/e7f5a26bfc1e)

2. 源码阅读

3. leecode 刷题

- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)

4. 项目进度

---

## 2021.8.25

1. 文章阅读

- [复习浏览器缓存总结语雀笔记](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E6%A6%82%E5%BF%B5)
- [Es6 新增的字符串方法](https://blog.csdn.net/weixin_30847865/article/details/101166659)

2. 源码阅读

- [Hash](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/Hash.md)
- [堆](https://github.com/jasonandjay/js-code/blob/master/algorithm/heap.js)

3. leecode 刷题

- [统计字符串中出现最多的字母](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)

4. 项目进度

- [x]路由按需加载

---

## 2021.8.24

1. 文章阅读

- [sticky 实现吸顶](https://www.cnblogs.com/s1nker/p/4835079.html)
- [使用富文本编辑器](https://www.wangeditor.com/)

2. 源码阅读

- [Hash](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/Hash.md)
- [堆](https://github.com/jasonandjay/js-code/blob/master/algorithm/heap.js)

3. leecode 刷题

- [移除元素](https://leetcode-cn.com/problems/remove-element/)

4. 项目进度

- [x]邮箱验证
- [x]邮箱登录后本地记录

---

## 2021.8.23

1. 文章阅读

- [hook 实现](https://www.jianshu.com/p/d41e6b09ef41)
- [hooks 优点](https://www.sohu.com/a/408993470_120054459)

2. 源码阅读

- [Hash](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/Hash.md)
- [堆](https://github.com/jasonandjay/js-code/blob/master/algorithm/heap.js)

3. leecode 刷题

- [移除元素](https://leetcode-cn.com/problems/remove-element/)

4. 项目进度

- [x]表情包实现
- [x]表情包添加

---

## 2021.8.22

1. 文章阅读

- [双飞燕布局和圣杯布局](https://www.jianshu.com/p/81ef7e7094e8)
- [严格模式](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Strict_mode)

---

## 2021.8.20

1. 文章阅读

- [初次面试之旅--良好体验+学习成长+战胜自我](https://juejin.cn/post/6962807197680009247)
- [如何应用 SOLID 原则整理 React 代码之单- -原则](https://juejin.cn/post/6963480203637030926)

2. 源码阅读

- [Hash](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/Hash.md)
- [堆](https://github.com/jasonandjay/js-code/blob/master/algorithm/heap.js)

3. leecode 刷题

- [两数之和](https://leetcode-cn.com/problems/two-sum/)
- [两数相加](https://leetcode-cn.com/problems/add-two-numbers/)

4. 项目进度

- [x]动态获取分页数据

---

## 2021.8.19

1. 文章阅读

- [为什么 web app 要使用 rem](http://caibaojian.com/web-app-rem.html)
- [媒体查询](https://www.runoob.com/cssref/css3-pr-mediaquery.html)

2. 源码阅读

- [React 面试必知必会 Day13](https://juejin.cn/post/6975120676982095886)

3. leecode 刷题

- [找到所有数组中消失的数字](https://leetcode-cn.com/problems/find-all-numbers-disappeared-in-an-array/)
- [最小操作次数使数组元素相等](https://leetcode-cn.com/problems/minimum-moves-to-equal-array-elements/)

4. 项目进度

- [x]分页
- [x]跳转详情

---

## 2021.8.18

1. 文章阅读

- [Promise 不会？？看这里！！！史上最通俗易懂的 Promise！！！](https://juejin.cn/post/6844903607968481287)
- [react+flexible 适配移动端项目的配置](https://juejin.cn/post/6951366350572879903)

2. 源码阅读

- [React 面试必知必会 Day12](https://juejin.cn/post/6975120676982095886)

3. leecode 刷题

- [Fizz Buzz](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xngt85/)
- [计数质数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnzlu6/)

4. 项目进度

- [x] 表情包弹窗

---

## 2021.8.17

1. 文章阅读

- [cors 跨域资源共享](http://www.ruanyifeng.com/blog/2016/04/cors.html)
- [hook](https://www.ruanyifeng.com/blog/2020/09/react-hooks-useeffect-tutorial.html)

2. 源码阅读

- [React 的数据结构](https://react.jokcy.me/book/api/react-structure.html)

3. leecode 刷题

- [.两数中位数](https://juejin.cn/post/6994959998283907102)

4. 项目进度

- [x] 留言板页面布局
- [x] 获取留言板数据
- [x] 渲染列表数据

---

## 2021.8.16

1. 文章阅读

- [BFC 块级格式化上下文](https://blog.csdn.net/weixin_42677762/article/details/108738331)
- [ZOOM:1 的原理和作用](https://blog.csdn.net/u010313768/article/details/47067593)

2. 源码阅读

- [React 的数据结构](https://react.jokcy.me/book/api/react-structure.html)

3. leecode 刷题

- [.两数中位数](https://juejin.cn/post/6994959998283907102)

4. 项目进度

- [x] 表情包 npm 下载使用
- [x] 评论 dom 结构
- [x] 评论样式

---

## 2021.8.15

1. 文章阅读

- [diff 算法图解](https://juejin.cn/post/6994959998283907102)

2. 源码阅读

- [React 的数据结构](https://react.jokcy.me/book/api/react-structure.html)

3. leecode 刷题

- [.两数中位数](https://juejin.cn/post/6994959998283907102)

4. 项目进度

- [x] 未更新

---

## 2021.8.13

1. 文章阅读

- [Dvn 官网](https://dvajs.com/guide/#%E5%91%BD%E5%90%8D%E7%94%B1%E6%9D%A5%EF%BC%9F)

2. 源码阅读

- [React 的数据结构](https://react.jokcy.me/book/api/react-structure.html)
- [React.render](https://react.jokcy.me/book/update/react-dom-render.html)

3. leecode 刷题

- [统计字符串中出现最多的字母](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)
- [字符串反序](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)
- [.深拷贝](https://www.cnblogs.com/dmcl/archive/2018/06/20/9204168.html)

4. 项目进度

- [x] 写路由层级
- [x] 主页布局

---

## 2021.8.12

1. 文章阅读

- [Umi 官网](https://umijs.org/zh-CN/docs/directory-structure)

2. 源码阅读

- [ReactElement](https://react.jokcy.me/book/api/react-element.html)
- [React.Children](https://react.jokcy.me/book/api/react-children.html)

3. leecode 刷题

- [不含有重复字符的最长子串的长度](https://www.cnblogs.com/mfrank/p/10472651.html)
- [leetcode 三数之和](https://blog.csdn.net/youyou_LIN/article/details/83217055)
- [字符串相乘](https://blog.csdn.net/weixin_40920953/article/details/103362401)

4. 项目进度

- [x] 分析项目路由层级
- [x] 分析项目布局
- [x] 分析项目需求
- [x] 分配到留言板页面代办

---

## 2021.8.11

1. 文章阅读

2. 源码阅读

3. leetcode 刷题

4. 项目进度

---

# 张唱唱

## 2021.9.15

1. 文章阅读

- [Vue3.0 新特性以及使用经验总结](https://juejin.cn/post/6940454764421316644)

2. 源码阅读

- [Vue 3 源码导读](https://juejin.cn/post/6844903957421096967)

3. leetcode 刷题

- [字符串相乘](https://blog.csdn.net/weixin_40920953/article/details/103362401)

4. 项目进度

- 美味不用等tab切换
## 2021.9.14

1. 文章阅读

- [Vuex框架原理与源码分析](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

2. 源码阅读

- [redux 源码小结](https://juejin.cn/post/6994393869957726215)

3. leetcode 刷题

- [寻找两个有序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/solution/xun-zhao-liang-ge-you-xu-shu-zu-de-zhong-wei-s-114/)

4. 项目进度

- 宏烨找房获取数据
- 渲染三个接口

## 2021.9.13

1. 文章阅读

- [Vuex 源码深度解析](https://juejin.cn/post/6844903676721496071)

2. 源码阅读

- [Vue 3 源码导读](https://juejin.cn/post/6844903957421096967)

3. leetcode 刷题

- [反转链表](https://leetcode-cn.com/problems/reverse-linked-list/solution/fan-zhuan-lian-biao-by-leetcode-solution-d1k2/)

4. 项目进度

- 无
## 2021.9.12

1. 文章阅读

- [为什么 vue 组件中 data 必须用函数表达？](https://zhuanlan.zhihu.com/p/159452050)

2. 源码阅读

- [Vue 3 源码导读](https://juejin.cn/post/6844903957421096967)

3. leetcode 刷题

- [搜索旋转排序数组](https://leetcode-cn.com/leetbook/read/tencent/x5ueu1/)

4. 项目进度

- 微信小程序初识

## 2021.9.10

1. 文章阅读

- [FormData 使用方法详解](https://www.jianshu.com/p/e984c3619019)

2. 源码阅读

- [websocket]()

3. leetcode 刷题

- [数组中的第 K 个最大元素](https://leetcode-cn.com/leetbook/read/tencent/x5txi7/)

4. 项目进度

- 无

## 2021.9.9

1. 文章阅读

- [(中篇)中高级前端大厂面试秘籍，寒冬中为您保驾护航，直通大厂](https://juejin.cn/post/6844903801153945608#heading-0)

2. 源码阅读

- [Vue 3 源码导读](https://juejin.cn/post/6844903957421096967)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

4. 项目进度

- 阿里云 oss 上传文件

## 2021.9.8

1. 文章阅读

- [SMTP 协议介绍](https://blog.csdn.net/qq_35644234/article/details/68961603)

2. 源码阅读

- [websocket]()

3. leetcode 刷题

- [反转链表](https://leetcode-cn.com/leetbook/read/tencent/x5xg2m/)

4. 项目进度

- 电子邮件传输协议 SMTP 的后端

## 2021.9.7

1. 文章阅读

- [每个前端都需要知道这些面向未来的 CSS 技术](https://juejin.cn/post/6989513390636924936)

2. 源码阅读

- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/utils/mockHooks.js)

3. leetcode 刷题

- [两数相加](https://leetcode-cn.com/leetbook/read/tencent/x55qm1/)

4. 项目进度

- 无

## 2021.9.6

1. 文章阅读

- [2021 年我的前端面试准备](https://juejin.cn/post/6989422484722286600)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [合并两个有序链表](https://leetcode-cn.com/leetbook/read/tencent/x59tp7/)

4. 项目进度

- 无

## 2021.9.5

1. 文章阅读

- [中高级前端大厂面试秘籍，为你保驾护航金三银四，直通大厂(上)](https://juejin.cn/post/6844903776512393224#heading-25)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [合并 K 个排序链表](https://leetcode-cn.com/leetbook/read/tencent/x5ode6/)

4. 项目进度

- 无

## 2021.9.3

1. 文章阅读

- [WebSocket：5 分钟从入门到精通](https://juejin.cn/post/6844903544978407431)

2. 源码阅读

- [websocket]()

3. leetcode 刷题

- [寻找两个正序数组的中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)

4. 项目进度

- 新建文章页面功能
- 协同编辑器页面功能

## 2021.9.2

1. 文章阅读

- [websocket 和 socketio 的区别](https://blog.csdn.net/qq_14998435/article/details/80030258)

2. 源码阅读

- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/utils/mockHooks.js)

3. leetcode 刷题

- [除自身以外数组的乘积](https://leetcode-cn.com/leetbook/read/tencent/xxfcwh/)

4. 项目进度

- 新建文章页面功能
- 协同编辑器页面功能

## 2021.9.1

1. 文章阅读

- [你需要 Mobx 还是 Redux？](https://juejin.cn/post/6844903562095362056)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [有效的括号](https://leetcode-cn.com/leetbook/read/tencent/xx1d8v/)

4. 项目进度

- 做页面功能点
- 排版协同编辑器页面
- 获取页面数据

## 2021.8.31

1. 文章阅读

- [TypeScript 结合 React 全家桶(antd、axios、Nextjs)的一些类型总结](https://blog.csdn.net/weixin_43902189/article/details/99706223)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/tencent/x5mohi/)

4. 项目进度

- 排版文章管理页面下的标签管理页面
- 排版协同编辑器页面
- 获取页面数据

## 2021.8.30

1. 文章阅读

- [vscode: Visual Studio Code 常用快捷键](https://www.cnblogs.com/bindong/p/6045957.html)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/tencent/xxj50s/)

4. 项目进度

- 排版文章管理页面下的标签管理页面
- 排版系统设置页面 tab 切换

## 2021.8.29

1. 文章阅读

- [BFC 详解](https://www.jianshu.com/p/55b4d64bd268)

2. 源码阅读

- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/utils/mockHooks.js)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/tencent/x5h4n3/)

4. 项目进度

- 无

## 2021.8.27

1. 文章阅读

- [面试官: 你了解前端路由吗?](https://juejin.cn/post/6844903589123457031)

2. 源码阅读

- [umi]()

3. leetcode 刷题

- [字符串相乘](https://leetcode-cn.com/leetbook/read/tencent/xxbolr/)

4. 项目进度

- 无

## 2021.8.26

1. 文章阅读

- [你需要知道的单页面路由实现原理](https://juejin.cn/post/6844903600913645582)

2. 源码阅读

- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/utils/mockHooks.js)

3. leetcode 刷题

- [最长公共前缀](https://leetcode-cn.com/leetbook/read/tencent/xxzqki/)

4. 项目进度

- 今日无项目进度
- 路由实现原理
- 支付宝支付

## 2021.8.25

1. 文章阅读

- [彻底理解浏览器的缓存机制（http 缓存机制）](https://www.cnblogs.com/chengxs/p/10396066.html)

2. 源码阅读

- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/utils/mockHooks.js)

3. leetcode 刷题

- [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)

4. 项目进度

- [x] 黑暗光明主题切换样式
- [x] 配置上线准备

## 2021.8.24

1. 文章阅读

- [浅谈 Hooks](https://blog.csdn.net/weixin_45820444/article/details/108988336)

2. 源码阅读

- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/utils/mockHooks.js)

3. leetcode 刷题

- [三数之和](https://leetcode-cn.com/leetbook/read/tencent/xxst6e/)

4. 项目进度

- [x] 拆分输入框组件
- [x] 组件排版

## 2021.8.23

1. 文章阅读

- [React hooks memo](https://blog.csdn.net/aminwangaa/article/details/108369633)

2. 源码阅读

- [hooks](https://github.com/jasonandjay/tranining/blob/master/React%20Hooks/hooks/src/utils/mockHooks.js)

3. leetcode 刷题

- [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)

4. 项目进度

- [x] 拆分组件
- [x] 子组件排版

## 2021.8.22

1. 文章阅读

- [Vue 3.2 发布了，那尤雨溪是怎么发布 Vue.js 的？](https://juejin.cn/post/6997943192851054606)

2. 源码阅读

- [Vue 3 源码导读](https://juejin.cn/post/6844903957421096967)

3. leetcode 刷题

- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

4. 项目进度

- [x] 拆分组件
- [x] 子组件排版

## 2021.8.20

1. 文章阅读

- [漫谈 promise 使用场景](https://www.jianshu.com/p/c613c0198430)

2. 源码阅读

- [umi]()

3. leetcode 刷题

- [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)

4. 项目进度

- [x] 拆分组件
- [x] 子组件排版

## 2021.8.19

1. 文章阅读

- [你知道我们平时在 CSS 中写的%都是相对于谁吗？](https://blog.csdn.net/zw52yany/article/details/85324855)

2. 源码阅读

- [umi]()

3. leetcode 刷题

- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

4. 项目进度

- [x] 关于页面排版中高度还原
- [x] 点击发布评论
- [x] 点击分页功能
- [x] 引入图片放大组件功能

## 2021.8.18

1. 文章阅读

- [JavaScript 工作原理之十－使用 MutationObserver 监测 DOM 变化](https://juejin.cn/post/6844903617833631752)

2. 源码阅读

- 今日无

3. leetcode 刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

4. 项目进度

- [x] 补充关于页面的样式
- [x] 点击图文列表跳详情

## 2021.8.17

1. 文章阅读

- [HTTP 请求的完全过程](https://blog.csdn.net/ailunlee/article/details/90600174)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

4. 项目进度

- [x] 渲染排版完毕关于页面组件内容
- [x] 精准排版

## 2021.8.16

1. 文章阅读

- [什么是跨域？跨域解决方法](https://blog.csdn.net/qq_38128179/article/details/84956552)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)

4. 项目进度

- [x] 渲染排版关于页面组件内容

## 2021.8.15

1. 文章阅读

- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

2. 源码阅读

- [react-hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

3. leetcode 刷题

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

4. 项目进度

- [x] 渲染排版文章组件内容
- [x] 渲染关于页面组件内容

## 2021.8.13

1. 文章阅读

- [你所不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)

2. 源码阅读

- [react-hooks]()

3. leetcode 刷题

- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4. 项目进度

## 2021.8.12

1. 文章阅读

- [你所不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)

2. 源码阅读

- [umi]()

3. leetcode 刷题

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

4. 项目进度

- 今日无

-

## 2021.8.11

1. 文章阅读

- [彻底搞懂 JS 原型、原型链和继承](https://juejin.cn/post/6994632915648774152)
- [看了就会，手写 Promise 原理，最通俗易懂的版本！！！](https://juejin.cn/post/6994594642280857630)

2. 源码阅读

- 今日无

3. leetcode 刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

4. 项目进度

- 今日无
