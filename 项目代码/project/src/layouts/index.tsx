// 这是全局layout 需创建黑白名单 这里要判断是否在登录或注册页面，并且判断路径然后重定向至需要的页面
import React, { useEffect, useState } from 'react';
import BlackList from '@/utils/routes/black';
import Routes from '@/utils/routes/route';
import { NavLink, useHistory, useLocation, useRouteMatch } from 'umi';
import { Layout, Menu, Breadcrumb } from 'antd';
import { createFromIconfontCN, MenuFoldOutlined } from '@ant-design/icons';
import './index.less';
import CardSelection from '@/components/card-selection';

//引入后侧
import Selection from '@/components/selection';

// icon图标链接
const IconFont = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_2775506_4913l990bpe.js',
});
// antd的layout布局
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const Layouts: React.FC = (props) => {
  // 左侧列表开关
  const [collapsed, setCollaosed] = useState(false);
  const onCollapse = () => {
    setCollaosed(!collapsed);
  };
  //   获取路由信息
  const localtion = useLocation();
  const history = useHistory();
  // 白名单 制止login页面的无限跳动
  const whiteList = ['/login', '/register'];
  //记录路由的key
  const [key, setKey] = useState(sessionStorage.keys + '' || '0');
  //记录二级面包屑
  const [crumbs, setCrumbs] = useState(sessionStorage.crumbs || '');
  //记录二级的路由
  const [route, setRoute] = useState(sessionStorage.route || '');

  // 做key值 用于列表替换
  let keys = 0;
  //登陆拦截
  useEffect(() => {
    // 切换key
    let ind = Routes.findIndex((item) => item.path === location.pathname);
    if (ind >= 0) {
      let n = Routes[ind].key;
      setKey(n + '');
      setCrumbs(Routes[ind].name);
      setRoute(Routes[ind].path);
      sessionStorage.keys = n + '';
      sessionStorage.crumbs = Routes[ind].name;
      sessionStorage.route = Routes[ind].path;
    } else {
      Routes.forEach((item) => {
        if (item.children) {
          let i = item.children.findIndex(
            (ite) => ite.path === location.pathname,
          );
          if (i >= 0) {
            let n = item.children[i].key;
            setKey(n + '');
            setCrumbs(item.children[i].name);
            setRoute(item.children[i].path);
            sessionStorage.keys = n + '';
            sessionStorage.crumbs = item.children[i].name;
            sessionStorage.route = item.children[i].path;
            return;
          }
        }
      });
      if (location.pathname === '/ownspace') {
        sessionStorage.keys = -1 + '';
        sessionStorage.crumbs = '用户管理';
        sessionStorage.route = '/ownspace';
        setKey(-1 + '');
        setCrumbs('用户管理');
        setRoute('/ownspace');
      }
    }

    //   判断是否在白名单  注意这里少了一个token值
    if (!whiteList.includes(localtion.pathname)) {
      // 不在的时候判断页面uid是否存在
      const { uid, user, token } = localStorage;
      if (uid && user && token) {
        // 存在啥也不做
      } else {
        // 不存在则去登录
        history.replace('/login');
      }
    }
  }, [localtion.pathname]);

  //   此处判断是否有左边列表布局
  if (BlackList.includes(localtion.pathname)) {
    // 此处是没有该布局的
    return <>{props.children}</>;
  } else {
    //   此处有左侧布局
    return (
      <Layout style={{ minHeight: '100vh', overflow: 'hidden' }}>
        <Sider collapsed={collapsed}>
          <div className="logo" />
          <div className="rv_9xUBx9vsH6qbhqqmOc">
            <img src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" />
            {collapsed ? null : <span>管理后台</span>}
          </div>
          {collapsed ? null : <CardSelection></CardSelection>}

          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            {Routes?.map((item, index) => {
              keys++;
              if (item.parent) {
                return (
                  <SubMenu
                    key={'sub' + keys}
                    icon={<IconFont type={item.icon} />}
                    title={item.parent}
                  // style={{width:"100px",height:"20px",background:"red"}}
                  >
                    {item.children?.map((ite) => {
                      keys++;
                      return (
                        <Menu.Item
                          key={keys}
                          icon={<IconFont type={ite.icon} />}
                        >
                          <NavLink to={ite.path}>{ite.name}</NavLink>
                        </Menu.Item>
                      );
                    })}
                  </SubMenu>
                );
              } else {
                return (
                  <Menu.Item key={keys} icon={<IconFont type={item.icon} />}>
                    <NavLink to={item.path!}>{item.name}</NavLink>
                  </Menu.Item>
                );
              }
            })}
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            <div className="abtRow">
              <div className="leftHeader">
                <span onClick={onCollapse}>
                  <MenuFoldOutlined />
                </span>
              </div>
              {/* 右侧图片 */}
              <div className="rightHeader">
                <div className="O8g9pCnM_qIcqSuaJm5uP">
                  <a
                    className="_283jLEQ1DhWAe2fw-CeVAf"
                    href="https://github.com/fantasticit/wipi"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <span
                      role="img"
                      aria-label="github"
                      className="anticon anticon-github"
                    >
                      <svg
                        viewBox="64 64 896 896"
                        focusable="false"
                        data-icon="github"
                        width="1em"
                        height="1em"
                        fill="currentColor"
                        aria-hidden="true"
                      >
                        <path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9a127.5 127.5 0 0138.1 91v112.5c.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z"></path>
                      </svg>
                    </span>
                  </a>
                  <div className="ant-dropdown-trigger">
                    <span className="ant-avatar ant-avatar-sm ant-avatar-circle ant-avatar-icon">
                      <span
                        role="img"
                        aria-label="user"
                        className="anticon anticon-user"
                      >
                        <svg
                          viewBox="64 64 896 896"
                          focusable="false"
                          data-icon="user"
                          width="1em"
                          height="1em"
                          fill="currentColor"
                          aria-hidden="true"
                        >
                          <path d="M858.5 763.6a374 374 0 00-80.6-119.5 375.63 375.63 0 00-119.5-80.6c-.4-.2-.8-.3-1.2-.5C719.5 518 760 444.7 760 362c0-137-111-248-248-248S264 225 264 362c0 82.7 40.5 156 102.8 201.1-.4.2-.8.3-1.2.5-44.8 18.9-85 46-119.5 80.6a375.63 375.63 0 00-80.6 119.5A371.7 371.7 0 00136 901.8a8 8 0 008 8.2h60c4.4 0 7.9-3.5 8-7.8 2-77.2 33-149.5 87.8-204.3 56.7-56.7 132-87.9 212.2-87.9s155.5 31.2 212.2 87.9C779 752.7 810 825 812 902.2c.1 4.4 3.6 7.8 8 7.8h60a8 8 0 008-8.2c-1-47.8-10.9-94.3-29.5-138.2zM512 534c-45.9 0-89.1-17.9-121.6-50.4S340 407.9 340 362c0-45.9 17.9-89.1 50.4-121.6S466.1 190 512 190s89.1 17.9 121.6 50.4S684 316.1 684 362c0 45.9-17.9 89.1-50.4 121.6S557.9 534 512 534z"></path>
                        </svg>
                      </span>
                    </span>
                    <span style={{ marginLeft: '8px' }}>
                      <Selection></Selection>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </Header>

          <Content>
            <div className="breads ">
              <Breadcrumb>
                <Breadcrumb.Item>
                  <NavLink to="/">工作台</NavLink>
                </Breadcrumb.Item>
                {['/article/category', '/article/tags'].includes(
                  location.pathname,
                ) && (
                    <Breadcrumb>
                      <NavLink to="/article">
                        所有文章<span style={{ marginLeft: '7px' }}>/</span>
                      </NavLink>
                    </Breadcrumb>
                  )}
                {location.pathname != '/' && (
                  <Breadcrumb.Item>
                    <NavLink to={route}>{crumbs}</NavLink>
                  </Breadcrumb.Item>
                )}
              </Breadcrumb>
            </div>

            {props.children}
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Ant Design ©2018 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    );
  }
};

export default Layouts;
