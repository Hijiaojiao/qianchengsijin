export interface ILoginForm {
  name: string;
  password: string;
}

//注册
export interface IRegistryForm {
  name: string;
  confirm: string;
  password: string;
}
