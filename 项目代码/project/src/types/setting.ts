// // 系统设置中的右侧滑出内容数据类型
// export interface IGettingList {
//   id: string;
//   originalname: string;
//   filename: string;
//   type: string;
//   size: number;
//   url: string;
//   createAt: string;
// }

// 系统设置页面数据
export interface ISettingList {
  adminSystemUrl: string;
  baiduAnalyticsId: string;
  createAt: string;
  googleAnalyticsId: boolean;
  i18n: string;
  id: string;
  oss: string;
  seoDesc: string;
  seoKeyword: string;
  smtpFromUser: string;
  smtpHost: string;
  smtpPass: string;
  smtpPort: string;
  smtpUser: string;
  systemFavicon: string;
  systemFooterInfo: string;
  systemLogo: string;
  systemTitle: string;
  systemUrl: string;
  updateAt: string;
}