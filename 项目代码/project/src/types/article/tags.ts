// 文章管理中的标签管理页面数据类型
export interface IArticleTags {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}