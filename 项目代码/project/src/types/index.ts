export * from './article/tags'; // 文章管理中的标签管理页面数据类型
export * from './setting';
export * from './user';
export * from './file'; //文件数据类型
export * from './comment';

//所有文章
export * from './article/article';
//页面管理
export * from './page';

export * from './category';
export * from './setting';

export * from './mail';
export * from './knowledge';
