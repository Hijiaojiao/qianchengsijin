// import { FileItem } from "@/types";
import { IKnowledgeSearch } from '@/types';
import { request } from 'umi';
//文件管理数据
export const getFiles = (page = 1, pageSize = 12, parame: Partial<IKnowledgeSearch> = {}) => {
    return request(`/api/file?page=${page}&pageSize=${pageSize}`, { parame })
}
