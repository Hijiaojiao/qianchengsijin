//登录接口
import { IRegistryForm } from '@/types';
import { request } from 'umi';

// 登陆接口
export function goRegistry(data: IRegistryForm) {
  return request('/api/user/register', {
    method: 'POST',
    data,
  });
}
