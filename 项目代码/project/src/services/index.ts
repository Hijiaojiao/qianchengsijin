export * from './article/tags'; // 获取文章管理中的标签管理页面数据
export * from './setting'; // 获取系统设置中的右侧滑出内容
export * from './user';
export * from './file';
export * from './registry';

export * from './category';
export * from './article/article'; // 发布文章

export * from './mail';
export * from './article';
//页面管理
export * from './page';
export * from './knowledge';
export * from './workbench';

export * from './comment';
