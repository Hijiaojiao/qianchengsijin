import {request} from "umi"

//最新文章
export function getWorkbench(page= 1, pageSize = 6){
     return request(`/api/article?page=${page}&pageSize=${pageSize}`)
}

export function getCommit(page = 1,pageSize = 6){
     return request(`/api/comment?page=${page}&pageSize=${pageSize}`)
}