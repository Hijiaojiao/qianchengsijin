import { request } from 'umi';

export function getPage(page: number, pageSize: number) {
  return request(`/api/page?page=${page}&pageSize=${pageSize}`);
}
