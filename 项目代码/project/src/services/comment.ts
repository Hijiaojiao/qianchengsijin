import { request } from 'umi';

// 分页获取评论
export function getComment(page=1, pageSize=12){
    return request(`/api/comment?page=${page}&pageSize=${pageSize}`)
}
