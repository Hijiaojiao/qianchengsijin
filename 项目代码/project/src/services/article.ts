import { request } from 'umi';

export function getArticle(page = 1, pageSize = 12) {
  return request(`/api/article?page=${page}&pageSize=${pageSize}`);
}
