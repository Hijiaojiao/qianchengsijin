import { IKnowledgeSearch } from "@/types";
import { request } from 'umi';
//知识小册的数据
export const knowledge = (page = 1, pageSize = 12, params: Partial<IKnowledgeSearch> = {}) => {
    console.log(params);

    return request(`/api/knowledge?page=${page}&pageSize=${pageSize}`, { params })
}


//知识小册删除
export function deleteItem(id: string) {
    return request(`/api/knowledge/${id}`, {
        method: 'DELETE'
    })
}


//知识小册发布或草稿
export function switChover(id: string, status: string) {
    return request(`/api/knowledge/${id}`, {
        method: "PATCH",
        data: {
            status
        }
    })
}
