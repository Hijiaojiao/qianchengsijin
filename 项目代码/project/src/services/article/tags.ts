import { request } from 'umi'
import { IArticleTags } from '@/types/article/tags'

// 获取文章管理中的标签管理页面数据
export function getArticleTags() {
    return request(`/api/tag`);
}
// 添加新标签
export function addArticleTags(label:string,value:string) {
    return request(`/api/tag`, {
        method: 'POST',
        params:{
            label,value
        }
    });
}
// 删除标签
export function delArticleTags(id: string) {
    return request(`/api/tag/${id}`, {
        method: 'DELETE'
    });
}
// 更新标签
export function updateArticleTags(label:string,value:string,id:string) {
    return request(`/api/tag/${id}`, {
        method: 'PATCH',
        params:{
            label,value
        }
    });
}
