import { IArticleItem } from '@/types/article/article';
import { request } from 'umi';

// 发布文章
export function publishArticle(data: IArticleItem) {
    return request('/api/article', {
        method: 'POST',
        data
    });
}