import { request } from 'umi';

// // 系统设置中的右侧滑出内容接口
// export function getSetting(page = 1, pageSize = 12) {
//     return request(`/api/file?page=${page}&pageSize=${pageSize}`)
// }

// 点击保存更改提交数据
export function setSetting() {
    return request(`/api/setting`, {
        method: 'POST'
    })
}
