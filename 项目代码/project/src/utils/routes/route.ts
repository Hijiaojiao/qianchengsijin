const routes = [
  {
    path: '/',
    name: '工作台',
    icon: 'icon-gongzuotai',
    key: 0,
  },
  {
    parent: '文章管理',
    icon: 'icon-wenzhangguanli',
    key: 1,
    children: [
      {
        path: '/article',
        name: '所有文章',
        icon: 'icon-wenzhangguanli',
        key: 2,
      },
      {
        path: '/article/category',
        name: '分类管理',
        icon: 'icon-fenleiguanli',
        key: 3,
      },
      {
        path: '/article/tags',
        name: '标签管理',
        icon: 'icon-biaoqianguanli',
        key: 4,
      },
    ],
  },
  {
    path: '/page',
    name: '页面管理',
    icon: 'icon-page-management',
    key: 5,
  },
  {
    path: '/knowledge',
    name: '知识小册',
    icon: 'icon-knowledge',
    key: 6,
  },
  {
    path: '/poster',
    name: '海报管理',
    icon: 'icon-haibao',
    key: 7,
  },
  {
    path: '/comment',
    name: '评论管理',
    icon: 'icon-pinglun',
    key: 8,
  },
  {
    path: '/mail',
    name: '邮件管理',
    icon: 'icon-youjian',
    key: 9,
  },
  {
    path: '/file',
    name: '文件管理',
    icon: 'icon-wenjian',
    key: 10,
  },
  {
    path: '/search',
    name: '搜索记录',
    icon: 'icon-sousuo',
    key: 11,
  },
  {
    path: '/view',
    name: '访问统计',
    icon: 'icon-tongji',
    key: 12,
  },
  {
    path: '/user',
    name: '用户管理',
    icon: 'icon-yonghu',
    key: 13,
  },
  {
    path: '/setting',
    name: '系统设置',
    icon: 'icon-shezhi',
    key: 14,
  },
];
export default routes;
