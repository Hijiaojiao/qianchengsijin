import Cookie from 'js-cookie';

const key = 'authorization';
// 存储登陆态
export function setToken(value: string) {
  Cookie.set(key, value, { expires: 1 });
}

// 获取登陆态
export function getToken() {
  return Cookie.get(key);
}

// 清除登陆态
export function removeToken() {
  Cookie.remove(key);
}
