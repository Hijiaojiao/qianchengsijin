import { RequestConfig } from 'umi';
import { message } from 'antd';
import React from 'react';
import StateContext from '@/context/stateContext';// 引入mobx的Provider
import store from '@/models/index';
import { getToken } from './utils';

// 关闭线上console
process.env.NODE_ENV === 'production' ? (console.log = () => { }) : null;

// 覆盖根组件
export function rootContainer(container: React.ReactNode) {
  return React.createElement(
    StateContext.Provider,
    { value: store },
    container,
  );
}

// 全局路由切换配置   这里做判断本地用户是否存在，不存在则重定向到登录页面
export function onRouteChange({ matchedRoutes }: any) { }

// 网络请求配置
// 设置显示错误信息的开关
let showError = false;
const baseUrl = 'https://creationapi.shbwyz.com'; //删除协议https:达到浏览器自判断协议
export const request: RequestConfig = {
  timeout: 10000,
  errorHandler: (error) => {
    const { response, data } = error;
    if (response && response.status) {
      let { url } = response;
      switch (url) {
        case baseUrl + '/api/user/register':
          //注册失败的时候的提示信息
          if (response.status === 400) {
            message.warning('该用户已被注册');
          }
          break;
        case baseUrl + '/api/auth/login':
          //登录失败的时候的提示信息
          if (response.status === 400) {
            message.error('用户名或密码错误');
          }
          break;
      }
    }
    return response;
  },
  errorConfig: { },
  middlewares: [],
  // 请求拦截器
  requestInterceptors: [(url, options) => {
    let authorization = getToken();
    if (authorization) {
      options = { ...options, headers: { authorization: 'Bearer ' + authorization } };
    }
    return {
      url: `${baseUrl}${url}`,
      options,
    };
  }],
  // 响应拦截器
  responseInterceptors: [
    (response) => {
      const codeMaps: { [key: number]: string } = {
        400: '错误的请求',
        403: '禁止访问',
        404: '找不到资源',
        500: '服务器内部错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      // 处理网络请求错误
      if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
        message.error(codeMaps[response.status]);
      }
      // 处理业务逻辑请求错误
      // if (response.data.success !== true){
      // message.error(response.data.msg)
      // }
      // console.log('response...', response);
      return response;
    },
  ],
};
