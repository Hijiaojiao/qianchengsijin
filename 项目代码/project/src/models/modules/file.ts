import { FileItem } from '@/types';

import { makeAutoObservable, runInAction } from 'mobx';
import { getFiles } from '@/services';
class File {
  //文件管理数据
  filelist: FileItem[] = [];
  fileCount: number = 0;
  constructor() {
    makeAutoObservable(this);
  }
  async getfile(page = 1, pageSize = 12, params: {}) {
    let result = await getFiles(page, pageSize, params);
    console.log(result);
    if (result.data) {
      runInAction(() => {
        this.fileCount = result.data[1];
        this.filelist = result.data[0];
      });
    }
  }
}
export default File;
