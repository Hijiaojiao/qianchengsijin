import {makeAutoObservable,runInAction} from "mobx"
import {getWorkbench} from "@/services/workbench"
import {workObject} from "@/types/workbench"

class workbench{
    newArticle: workObject[] = [];
    constructor(){
        makeAutoObservable(this)
    }
    async getWorkbenchs(page = 1){
      let result = await getWorkbench(page);
      console.log(result); 
      
      if(result.data){
          runInAction(()=>{
              this.newArticle = result.data[0]
          })
      }
    }
}
export default workbench