import {makeAutoObservable,runInAction} from "mobx"
import {getCommit} from "@/services/workbench"
import {CommitObject} from "@/types/workbench"

class Commits{
   newCommit:CommitObject[]=[];
   constructor(){
       makeAutoObservable(this)
   }
   async getCommits(page = 1){
       let result = await getCommit(page)
       console.log(result);
       if(result.data){
           runInAction(()=>{
               this.newCommit = result.data[0]
           })
       }
   }
}
export default Commits