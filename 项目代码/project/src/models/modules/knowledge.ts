import { knowledgeItem } from '@/types';

import { makeAutoObservable, runInAction } from 'mobx';
import { knowledge, deleteItem, switChover } from '@/services';
import { message } from 'antd';
class Knowledge {
    //文件管理数据
    knowledgelist: knowledgeItem[] = [];
    knowledgeCount: number = 0;
    constructor() {
        makeAutoObservable(this);
    }
    // 获取知识小册数据
    async getknowledge(page = 1, pageSize = 12, params: {}) {
        let result = await knowledge(page, pageSize, params);
        console.log(result);
        if (result.data) {
            runInAction(() => {
                this.knowledgeCount = result.data[1];
                this.knowledgelist = result.data[0];
            });
        }
    }

    //删除
    async deleteItem(ids: string[]) {
        message.loading('操作中', 0)
        Promise.all(ids.map(id => deleteItem(id)))
            .then(res => {
                message.destroy()
                message.success('删除成功')
                this.getknowledge()
            })
            .catch(err => {
                message.destroy()
                message.error('操作失败')
            })
    }

    // 切换发布/草稿
    async switChover(id: string, status: string) {
        let result = await switChover(id, status)
        console.log(status);
        if (result.statusCode === 200) {
            this.getknowledge()
        }
    }

}
export default Knowledge;
