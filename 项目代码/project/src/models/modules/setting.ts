import { setSetting } from "@/services";
import { makeAutoObservable, runInAction } from 'mobx'

class Setting {
    // gettingList:IGettingList[] = [];
    settingList!: {};
    constructor() {
        makeAutoObservable(this);
    }
    // 系统设置中的右侧滑出内容
    // async getSetting(page = 1) {
    //     let result = await getSetting(page);
    //     if (result.data) {
    //         runInAction(() => {
    //             this.gettingList = result.data[0];
    //         })
    //     }
    // }

    // 点击保存提交更改数据
    async setSetting() {
        let result = await setSetting();
        if (result.data) {
            runInAction(() => {
                this.settingList = result.data;
            })
        }
    }
}

export default Setting;
