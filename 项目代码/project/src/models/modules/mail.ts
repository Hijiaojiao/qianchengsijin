import { getmail } from "@/services"
import { MailItem } from "@/types"
import { makeAutoObservable, runInAction } from "mobx"
class Mail {
    //邮件数据
    Maillist: MailItem[] = [];
    mailCount: number = 0
    constructor() {
        makeAutoObservable(this);
    }
    async getmail(page=1) {
        let result = await getmail(page)
        console.log(result);

        if (result.data) {
            runInAction(() => {
                this.mailCount = result.data[1]
                this.Maillist = result.data[0]
            })
        }
    }
}
export default Mail