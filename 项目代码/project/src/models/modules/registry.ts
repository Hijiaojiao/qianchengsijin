import { goRegistry } from '@/services';
import { IRegistryForm } from '@/types';
import { makeAutoObservable } from 'mobx';

class Registry {
  constructor() {
    makeAutoObservable(this);
  }
  async registrys(data: IRegistryForm) {
    let res = await goRegistry(data);
    console.log('注册.....', res);
  }
}

export default Registry;
