import { pageItem } from '@/types';
import { getPage } from '@/services';
import { makeAutoObservable } from 'mobx';

class Page {
  //初始值
  pageList: pageItem[] = [];
  pageCount: number = 0;

  constructor() {
    makeAutoObservable(this);
  }
  async getPageList(page = 1, pageSize = 12) {
    let result = await getPage(page, pageSize);
    console.log('文章页面....', result);
    //赋值
    this.pageList = result.data[0];
    this.pageCount = result.data[1];
  }
}
export default Page
