import { login } from '@/services';
import { ILoginForm } from '@/types';
import { makeAutoObservable } from 'mobx';
import { removeToken, setToken } from '@/utils';

class User {
  isLogin = false;
  userInfo = {};
  constructor() {
    makeAutoObservable(this);
  }
  async login(data: ILoginForm) {
    let result = await login(data);
    console.log('result...', result);
    if (result.data) {
      this.isLogin = true;
      this.userInfo = result.data;
      // 存储登陆态
      setToken(result.data.token);
    }

    return result;
  }
  logout() {
    this.isLogin = false;
    this.userInfo = {};
    removeToken();
  }
}

export default User;


