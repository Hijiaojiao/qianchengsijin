import { getComment } from "@/services";
import { ICommentItem } from "@/types";
import { makeAutoObservable, runInAction } from "mobx"

class Comment{
    commentList: ICommentItem[] = [];
    commentCount: number = 0;
    constructor(){
        makeAutoObservable(this);
    }

    async getComment(page=1){
        let result = await getComment(page);
        if (result.data){
            runInAction(()=>{
                this.commentCount = result.data[1];
                this.commentList = result.data[0];
            })
        }
    }
}

export default Comment;
