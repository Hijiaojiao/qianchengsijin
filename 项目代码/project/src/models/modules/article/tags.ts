import { getArticleTags, addArticleTags, delArticleTags, updateArticleTags } from "@/services";
import { makeAutoObservable, runInAction } from 'mobx'
import { IArticleTags } from '@/types'

class Tags {
  tagsList: IArticleTags[] = [];
  constructor() {
    makeAutoObservable(this);
  }

    // 获取所有标签
    async getArticleTags() {
        let result = await getArticleTags();
        if (result.data) {
            runInAction(() => {
                this.tagsList = result.data;
            })
        }
    }
    // 添加新标签
    async addArticleTags(label: string, value: string) {
        let result = await addArticleTags(label, value);
        if (result.data) {
            runInAction(() => {
                this.tagsList = result.data;
            })
        }
    }
    // 删除标签
    async delArticleTags(id: string) {
        let result = await delArticleTags(id);
        if (result.data) {
            runInAction(() => {
                this.tagsList = result.data;
            })
        }
    }

    // 更新标签
    async updateArticleTags(id: string, label: string, value: string) {
        let result = await updateArticleTags(id, label, value);
        if (result.data) {
            runInAction(() => {
                this.tagsList = result.data;
            })
        }
    }
}

export default Tags;
