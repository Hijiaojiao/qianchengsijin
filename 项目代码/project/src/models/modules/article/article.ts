import { publishArticle } from "@/services";
import { IArticleItem } from "@/types/article/article";
import { makeAutoObservable } from "mobx"

class Articles {
    constructor() {
        makeAutoObservable(this);
    }
    async publishArticle(data: IArticleItem) {
        let result = await publishArticle(data);
        if (result.data) {

        }
        return result;
    }
}

export default Articles;
