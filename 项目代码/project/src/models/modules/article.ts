import { makeAutoObservable, runInAction } from 'mobx';
import { getArticle } from '@/services';
import { articleItem } from '@/types';

class Article {
  //初始值
  articleList: articleItem[] = [];
  articleCount: number = 0;
  constructor() {
    //自动绑定监听
    makeAutoObservable(this);
  }
  async getArticelItem(page = 1) {
    let result = await getArticle(page);
    console.log(result, 'result++++');
    if (result.data) {
      runInAction(() => {
        this.articleList = result.data[0];
        this.articleCount = result.data[1];
      });
    }
  }
}
export default Article;
