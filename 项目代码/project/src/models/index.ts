// 引入mobx模块
import Tags from './modules/article/tags';
import Setting from './modules/setting';
import User from './modules/user';
import Registry from './modules/registry';
import File from './modules/file';
import Comment from './modules/comment';
import Category from './modules/category';
import Articles from './modules/article/article';
import Workbench from './modules/workbench';
import Commit from './modules/commit';
import Mail from './modules/mail';
import Article from './modules/article';
import Page from './modules/page';
import Knowledge from './modules/knowledge';

export default {
  tags: new Tags(),
  setting: new Setting(),
  user: new User(),
  registry: new Registry(),
  file: new File(),
  comment: new Comment(),
  category: new Category(),
  articles: new Articles(),
  workbench: new Workbench(),
  commit: new Commit(),
  mail: new Mail(),
  article: new Article(),
  page: new Page(),
  knowledge: new Knowledge(),
};
