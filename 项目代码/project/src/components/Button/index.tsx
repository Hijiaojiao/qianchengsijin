import React, { useEffect, useState } from 'react'
import { Form, Input, Button, Select } from 'antd'
//知识小册搜索和重置
import styles from '@/styles/file/file.less'
import userStore from '@/context/userStore';
interface IForm {
    [key: string]: string | boolean
}
const { useForm } = Form;
export default function index() {
    const store = userStore()
    const [page, setPage] = useState(1)
    const [pageSize, setSize] = useState(12)
    const [params, setParams] = useState({})
    useEffect(() => {
        store.knowledge.getknowledge(page, pageSize, params)
    }, [page, pageSize, params])

    const [form] = useForm();
    function submit() {
        let values = form.getFieldsValue();
        console.log(values);

        let params: IForm = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params)
    }
    return (
        <div>
            <Form form={form}
                onFinish={submit}
                className={styles.outermost}>
                <div className={styles.middle}>
                    <Form.Item
                        name="title"
                        label="文件名称"
                    >
                        <Input type="text" placeholder=" 请输入文件名称" />
                    </Form.Item>
                    <Form.Item
                        name="status"
                        label="状态"
                    >
                        <Select>
                            <Select.Option value="publish">已发布</Select.Option>
                            <Select.Option value="draft">草稿</Select.Option>
                        </Select>
                    </Form.Item>

                </div>

                <div className={styles.buttons}>
                    <div>
                        <span>
                            <Button type="primary" htmlType="submit">搜索</Button>
                            <Button style={{ marginLeft: '8px', marginRight: '8px' }} htmlType='reset'>重置</Button>
                        </span>

                    </div>
                </div>
            </Form>
        </div>
    )
}
