import React from 'react';
import { Menu, Dropdown, Button, Space } from 'antd';
import { NavLink } from 'umi';
const rightList = [
  {
    path: '/ownspace',
    titel: '个人中心',
  },
  {
    path: '/user',
    titel: '用户管理',
  },
  {
    path: '/setting',
    titel: '系统设置',
  },
  {
    path: '/login',
    titel: '退出登录',
  },
];
const Selection: React.FC = () => {
  const menu = (
    <Menu>
      {rightList.map((item, index) => {
        return (
          <Menu.Item key={index}>
            <NavLink to={item.path}>{item.titel}</NavLink>
          </Menu.Item>
        );
      })}
    </Menu>
  );
  return (
    <Space direction="vertical">
      <Space wrap>
        <Dropdown overlay={menu} placement="bottomLeft">
          <Button style={{ border: 'none' }}>Hi Heinan</Button>
        </Dropdown>
      </Space>
    </Space>
  );
};

export default Selection;
