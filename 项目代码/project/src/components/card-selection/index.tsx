import React, { useState } from 'react';
import { Menu, Dropdown, Button, Space } from 'antd';
import { NavLink } from 'umi';
import './index.less';
const rightList = [
  {
    path: '/article/amEditor',
    titel: '新建文章-协同编辑器',
  },
  {
    path: '/article/editor',
    titel: '新建文章',
  },
  {
    path: '/page/editor',
    titel: '新建页面',
  },
];
const Selection: React.FC = () => {
  const [collapsed, setCollaosed] = useState(false);
  const menu = (
    <Menu>
      {rightList.map((item, index) => {
        return (
          <Menu.Item key={index}>
            <NavLink to={item.path}>{item.titel}</NavLink>
          </Menu.Item>
        );
      })}
    </Menu>
  );
  return (
    <Space direction="vertical">
      <Space wrap>
        <Dropdown overlay={menu} placement="bottomLeft">
          <Button
            style={{
              border: 'none',
              width: '150px',
              height: '40px',
              lineHeight: '40px',
              textAlign: 'center',
              marginLeft: '20px',
              marginTop: '7px',
              backgroundColor: '#0188fb',
              color: '#fff',
              marginBottom: '8px',
            }}
          >
            <span style={{ fontSize: '20px' }}>+</span>
            <span style={{ fontSize: '16px', marginLeft: '8px' }}>新建</span>
          </Button>
        </Dropdown>
      </Space>
    </Space>
  );
};

export default Selection;
