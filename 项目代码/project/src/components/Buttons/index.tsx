import React, { useEffect, useState } from 'react'
import { Form, Input, Button } from 'antd'

import styles from '@/styles/file/file.less'
import userStore from '@/context/userStore';
interface IForm {
  [key: string]: string | boolean
}
const { useForm } = Form;
export default function index() {
  const [form] = useForm();

  const store = userStore()
  const [page, setPage] = useState(1)
  const [pageSize, setSize] = useState(12)
  const [params, setParams] = useState({})
  useEffect(() => {
    store.file.getfile(page, pageSize, params)
  }, [page, pageSize, params])


  // function submit() {
  //   let values = form.getFieldsValue();
  //   let params: IForm = {};
  //   for (let key in values) {
  //     values[key] && (params[key] = values[key]);
  //   }
  //   setParams(params)
  // }

  function submit() {
    let values = form.getFieldsValue();
    console.log(values);

    let params: IForm = {};
    for (let key in values) {
      values[key] && (params[key] = values[key]);
    }
    console.log(params);

    setParams(params)
  }
  return (
    <div>
      <Form form={form}
        onFinish={submit}
        className={styles.outermost}>
        <div className={styles.middle}>
          <Form.Item
            name="originalname"
            label="文件名称"
          >
            <Input type="text" placeholder=" 请输入文件名称" />
          </Form.Item>
          <Form.Item
            name="type"
            label="文件类型"
          >
            <Input type="text" placeholder=" 请输入文件类型" />
          </Form.Item>
        </div>

        <div className={styles.buttons}>
          <div>
            <span>
              <Button type="primary" htmlType="submit">搜索</Button>
              <Button style={{ marginLeft: '8px', marginRight: '8px' }} htmlType='reset'>重置</Button>
            </span>

          </div>
        </div>
      </Form>
    </div>
  )
}
