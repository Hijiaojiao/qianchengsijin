import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import userStore from '@/context/userStore';
import styles from '@/styles/drawerContent.less';
import Buttons from '@/components/Buttons';
import { Pagination } from 'antd';
import ImageView from '../Imageview';

const DrawerContent: React.FC = () => {
  const store = userStore();
  console.log(store);
  const fileList = store.file.filelist;
  const [page, setPage] = useState(1);

  useEffect(() => {
    store.file.getfile(page);
  }, [page]);
  return (
    <div>
      <Buttons></Buttons>
      <div className={styles.ownspaceList}>
        {fileList.map((item, index) => {
          return (
            <div key={index} className={styles.chunk}>
              <ImageView>
                <div className={styles.item}>
                  <div className={styles.picture}>
                    <img src={item.url} alt="" />
                  </div>
                  <div>
                    <span className={styles.word}>{item.originalname}</span>
                  </div>
                </div>
              </ImageView>
            </div>
          );
        })}
        <div>
          <Pagination
            total={24}
            showTotal={() => `共 ${store.file.fileCount} 页`}
            defaultPageSize={12}
            defaultCurrent={1}
            onChange={(e) => {
              setPage(e);
            }}
          />
        </div>
      </div>
    </div>
  );
};
export default observer(DrawerContent);
