import React, { useEffect, useState } from "react"
import { NavLink } from "umi"
import "./comment.less"
import userStore from "@/context/userStore"
import { observer } from "mobx-react-lite"
import {Button} from "antd"

const article: React.FC = () => {
    const store = userStore()
    const commitList = store.commit.newCommit
    const [page, setPage] = useState(false)
    useEffect(() => {
        store.commit.getCommits()
    }, [page])
    return <div className="comment">
        <div className="comment_one">
            <div className="ant-card-head-wrapper">
                <div className="ant-card-head-title">最新评论</div>
                <div className="ant-card-extra">
                    <NavLink to={`/comment`}>
                        <span>全部评论</span>
                    </NavLink>

                </div>
            </div>
        </div>
        <div className="comment_two">
            <ul className="comment_ul" style={{margin:"0"}}>
                {
                    commitList && commitList.map((item, index) => {
                        return <li key={index} className="comment_li" style={{display:"flex"}}>
                            <div className="comment_li_div">
                                <span>{item.name}</span>&nbsp;在&nbsp;&nbsp;<a>文章</a>&nbsp;&nbsp;评论
                                <span>
                                    <Button type="link">查看内容</Button>
                                </span>
                                &nbsp;
                                <span> 
                                    <span className="ant-badge-status-dot ant-badge-status-success"></span>&nbsp;&nbsp;
                                    <span>通过</span>
                                </span>
                            </div>
                            <ul className="comment_li_ul">
                                <li className="comment_li_ul_li">
                                    <a>通过</a>
                                    &nbsp;&nbsp;&nbsp;
                                    <a>拒绝</a>
                                    &nbsp;&nbsp;&nbsp;
                                    <a>回复</a>
                                    &nbsp;&nbsp;&nbsp;
                                    <a>删除</a>
                                </li>
                            </ul>
                        </li>
                    })
                }
            </ul>
        </div>
    </div>
}

export default observer(article)