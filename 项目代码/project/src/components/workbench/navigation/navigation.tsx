import React from "react"
import { NavLink } from "umi"
import "./navigation.less"

const  navigation:React.FC=()=>{
    const navList=["文章管理","评论管理","文件管理","用户管理","访问管理","系统设置"]
    const navlists=["/article","/comment","/file","/user","/view","/setting",]
    return <div className="navigation">
         <div className="navigation_one">
             <div className="ant-card-head-wrapper">
                 <div className="ant-card-head-title">快速导航</div>
             </div>
         </div>
         <div className="navigation_two">
              <div className="ant-row">
                  {
                     
                      navList&&navList.map((item,index)=>{
                          return <div className="nav" key={index}>
                            <NavLink key={index} to={navlists[index]}>
                               <span>{item}</span>
                            </NavLink>
                          </div>
                      })
                  }
              </div>
         </div>

    </div>
}

export default  navigation