import React, { useEffect, useState } from "react"
import { NavLink } from "umi"
import "./article.less"
import { observer } from "mobx-react-lite"
import userStore from "@/context/userStore";
// import { getWorkbench } from "@/services"
 

const article: React.FC = () => {
    const store = userStore()
    const articleList = store.workbench.newArticle;
    const [page, setPage] = useState(false)
    useEffect(() => {
        store.workbench.getWorkbenchs()
    }, [page])

    return <div className="article">
        <div className="article_one">
            <div className="ant-card-head-wrapper">
                <div className="ant-card-head-title">最新文章</div>
                <div className="ant-card-extra">
                    <NavLink to={`/article`}>
                        <span>全部文章</span>
                    </NavLink>

                </div>
            </div>
        </div>
        <div className="article_two">
            {
                articleList.map((item, index) => {
                    return <div key={index} className='small_box'>
                       <div className="box_content">
                            <img src={item.cover} alt="" className="small_img"/>
                            <p className="small_p">{item.title}</p>
                       </div>
                    </div>
                })
            }
        </div>
    </div>
}

export default observer(article)