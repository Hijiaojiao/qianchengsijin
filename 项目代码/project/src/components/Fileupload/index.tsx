//文件上传组件
import React from 'react'
import { Upload, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import '../../styles/file/file.less'
const index: React.FC = () => {
    const { Dragger } = Upload;
    const props = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info: any) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        onDrop(e: any) {
            console.log('Dropped files', e.dataTransfer.files);
        },
    };
    return (
        <div>
            <Dragger {...props} style={{ marginTop: "20px", backgroundColor: "#fff" }}>
                <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                </p>
                <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                <p className="ant-upload-hint">
                    文件将上传到OSS,如未配置请先配置
                </p>
            </Dragger>
        </div >
    )
}

export default index
