// 进去后的默认页面 工作台页面   这里做一个路由重定向判断是否登录
import React from "react";
import styles from "@/styles/workbench.less"
import Navigation from "@/components/workbench/navigation/navigation"
import Article from "@/components/workbench/article/article";
import Commit from "@/components/workbench/comment/comment"

const Index:React.FC=()=>{
    return <div>
        <header className={styles.header}>
                <div>
                    <h1>您好，admin</h1>
                    <div>您的角色：管理员</div>
                </div>
        </header>
        <main className={styles.main}>
            <div className={styles.box_echarts}>
            </div>
            <Navigation/>
            <Article/>
            <Commit/>
        </main>
       
    </div>
}
export default Index;