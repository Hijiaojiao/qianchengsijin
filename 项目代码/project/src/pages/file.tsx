// 文件管理页面
import Fileupload from '@/components/Fileupload';
import Buttons from '@/components/Buttons';
import styles from '../styles/file/fileContent.less';
import userStore from '@/context/userStore';
import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import ImageView from '@/components/Imageview';

import {
   Pagination,
   Drawer,
   Form,
   Button,
   Col,
   Row,
   Input,
   Select,
   DatePicker,
} from 'antd';

const file: React.FC = () => {
   const { Option } = Select;
   //获取数据
   const store = userStore();
   // 定义个状态
   let [id, setId] = useState('');
   // console.log(store);
   const fileList = store.file.filelist;
   console.log(fileList);

   const [page, setPage] = useState(1);
   const [pageSize, setSize] = useState(12);
   const [params, setParams] = useState({});

   useEffect(() => {
      store.file.getfile(page, pageSize, params);
      console.log(fileList, 'file');
   }, [page, pageSize, params]);

   function onShowSizeChange(current: number, pageSize: number) {
      console.log(current, pageSize);
   }

   const [visible, setVisible] = useState(false);

   const showDrawer = (id: string) => {
      setVisible(!visible);
      console.log(id);
      setId(id);
   };

   const onClose = () => {
      setVisible(false);
   };

   return (
      <div className={styles.files}>
         <Fileupload></Fileupload>
         <Buttons></Buttons>
         <div className={styles.content}>
            {fileList.map((item, index) => {
               return (
                  <div className={styles.item} onClick={() => showDrawer(item.id)}>
                     <div className={styles.items}>
                        <div className={styles.picture}>
                           <img src={item.url} alt="" />
                        </div>
                        <div className={styles.text}>
                           <div className={styles.title}>{item.originalname}</div>
                           <div className={styles.description}>
                              上传于{item.createAt}
                           </div>
                        </div>
                     </div>
                  </div>
               );
            })}
            <>
               <Pagination
                  showSizeChanger
                  onShowSizeChange={onShowSizeChange}
                  defaultCurrent={3}
                  total={500}
               />
            </>
            <>
               <Drawer
                  title="文件信息"
                  width={720}
                  onClose={onClose}
                  visible={visible}
                  bodyStyle={{ paddingBottom: 80 }}
                  footer={
                     <div
                        style={{
                           textAlign: 'right',
                        }}
                     >
                        <Button onClick={onClose} style={{ marginRight: 8 }}>
                           关闭
                        </Button>
                        <Button onClick={onClose} type="primary">
                           删除
                        </Button>
                     </div>
                  }
               >
                  <div className={styles.drawer}>
                     {fileList.map((item, index) => {
                        return (
                           item.id === id && (
                              <div className={styles.drawerItem}>
                                 <ImageView>
                                    {' '}
                                    <div className={styles.drawerItems}>
                                       <div className={styles.drawerImg}>
                                          <img src={item.url} alt="" />
                                       </div>
                                       <div className={styles.drawerText}>
                                          <div className={styles.drawerTitle}>
                                             文件名称：{item.originalname}
                                          </div>
                                          <div className={styles.drawerFilename}>
                                             {' '}
                                             存储路径：{item.filename}
                                          </div>
                                          <div className={styles.drawerType}>
                                             {' '}
                                             文件类型：{item.type} 文件大小：{item.size}
                                          </div>
                                          <div className={styles.drawerUrl}>
                                             <div>访问链接：{item.url}</div>
                                          </div>
                                          <div className={styles.button}>
                                             <span>复制</span>
                                          </div>
                                       </div>
                                    </div>
                                 </ImageView>
                              </div>
                           )
                        );
                     })}
                  </div>
               </Drawer>
            </>
         </div>
      </div>
   );
};

export default observer(file);
