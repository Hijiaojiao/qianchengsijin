// 新建文章
import React, { useEffect, useState } from 'react';
import { useHistory } from 'umi';
import ForEditor from 'for-editor'
import useStore from '@/context/userStore';
import { makeHtml, makeToc } from '@/utils/markdown';
import styles from '@/styles/article/editor.less';
import { message, Dropdown, Menu, Drawer, Form, Input, Button, Select, Popconfirm, Switch } from 'antd';
import {
  CloseOutlined,
  EllipsisOutlined,
} from '@ant-design/icons';

const Editor: React.FC = () => {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [settingDrawer, setSettingDrawer] = useState(false);
  const [cover, setCover] = useState('');
  const [form] = Form.useForm();

  const history = useHistory();
  const store = useStore();

  useEffect(() => {
    store.category.getCategoryList();
    store.tags.getArticleTags();
  }, [])

  function onChange(checked) {
    console.log(`switch to ${checked}`);
  }
  function confirm(e: MouseEvent) {
    // 点击返回首页
    history.replace('/');
  }
  function cancel(e: MouseEvent) {
    console.log(e);
    message.error('Click on No');
  }
  async function submit() {
    if (!title) {
      message.warn('请输入文章标题');
      return;
    }
    const values = form.getFieldsValue();
    // 添加文章md内容
    values.content = content;
    // 添加文章html内容
    values.html = makeHtml(content);
    // 添加文章大纲
    values.toc = makeToc(values.html);
    // 添加文章标题
    values.title = title;
    // 添加文章发布状态
    values.status = 'publish';
    let result = await store.articles.publishArticle(values);
    console.log('result...', result);
    if (result.data) {
      message.success('文章发布成功');
    }
  }
  const toolbar = {
    h1: true, // h1
    h2: true, // h2
    h3: true, // h3
    h4: true, // h4
    img: true, // 图片
    link: true, // 链接
    code: true, // 代码块
    preview: true, // 预览
    expand: true, // 全屏
    /* v0.0.9 */
    undo: true, // 撤销
    redo: true, // 重做
    save: true, // 保存
    /* v0.2.3 */
    subfield: true, // 单双栏模式
  }
  const editorMenu = <Menu>
    <Menu.Item disabled>查看</Menu.Item>
    <Menu.Item onClick={() => setSettingDrawer(true)}>设置</Menu.Item>
    <Menu.Item>保存草稿</Menu.Item>
    <Menu.Item disabled>删除</Menu.Item>
  </Menu>
  console.log('form..', form.getFieldsValue());
  return <div className={styles.editor}>
    <header>
      <div className={styles.top}>
        <div className={styles.tleft}>
          <Popconfirm
            title="确认关闭？如果有内容变更，请先保存。"
            onConfirm={confirm}
            onCancel={cancel}
            placement="rightTop"
            okText="确认"
            cancelText="取消"
          >
            <Button><CloseOutlined /></Button>
          </Popconfirm>
          <Input placeholder="请输入文章标题" value={title} onChange={e => setTitle(e.target.value)} />
        </div>
        <div className={styles.tright}>
          <Button type="primary" onClick={submit}>发布</Button>
          <Dropdown overlay={editorMenu}>
            <Button onClick={e => e.preventDefault()} style={{ marginLeft: '12px', border: 'none', color: '#0188fb' }}><EllipsisOutlined /></Button>
          </Dropdown>
        </div>
      </div>
    </header>
    <div className={styles.main}>
      <ForEditor
        value={content}
        toolbar={toolbar}
        onChange={value => setContent(value)}
      />
    </div>
    <Drawer
      footer={<Button type="primary" onClick={() => setSettingDrawer(false)}>确认</Button>}
      width="35%" title="文章设置" placement="right" onClose={() => setSettingDrawer(false)} visible={settingDrawer}>
      <Form
        form={form}
      >
        <Form.Item name="summary" label="文章摘要">
          <Input.TextArea></Input.TextArea>
        </Form.Item>
        <Form.Item name="password" label="访问密码">
          <Input.Password placeholder="" />
        </Form.Item>
        <Form.Item name="totalAmount" label="付费查看">
          <Input.Password placeholder="" />
        </Form.Item>
        <Form.Item name="isCommentable" label="开启评论">
          <Switch defaultChecked onChange={onChange} checked={false} />
        </Form.Item>
        <Form.Item name="isRecommended" label="首页推荐">
          <Switch defaultChecked onChange={onChange} checked={false} />
        </Form.Item>
        <Form.Item name="category" label="选择分类">
          <Select>{
            store.category.categoryList.map(item => {
              return <Select.Option value={item.id}>{item.label}</Select.Option>
            })
          }</Select>
        </Form.Item>
        <Form.Item name="tags" label="选择标签">
          <Select>{
            store.tags.tagsList.map(item => {
              return <Select.Option value={item.id}>{item.label}</Select.Option>
            })
          }</Select>
        </Form.Item>
        <img src={cover} alt="" />
        <Form.Item name="cover" label="文章封面">
          <Input type="text" placeholder="" onChange={e => setCover(e.target.value)} />
        </Form.Item>
        <Button onClick={() => {
          let values = form.getFieldsValue();
          form.setFieldsValue({ ...values, cover: '' })
          setCover('');
        }}>移除</Button>
      </Form>
    </Drawer>
  </div>;
};
export default Editor;
