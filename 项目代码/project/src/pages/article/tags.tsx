// 文章管理下的标签管理
import React, { useEffect, useState } from "react"
import { IArticleTags } from '@/types'
import { observer } from 'mobx-react-lite';
import useStore from '@/context/userStore';
import { Input, Button } from 'antd';
import styles from '@/styles/article/tags.less'

const Tags: React.FC = () => {
    const store = useStore();
    // 展示添加还是更新
    const [flag, setFlag] = useState(false);
    const [label, setLabel] = useState('');
    const [value, setValue] = useState('');
    const [id, setId] = useState('');
    const [isModalVisible, setIsModalVisible] = useState(false);
    useEffect(() => {
        store.tags.getArticleTags();
    }, [])
    // 点击确认删除
    const handleOk = () => {
        store.tags.delArticleTags(id);
        setFlag(false);
        setLabel('');
        setValue('');
        setId('');
        setIsModalVisible(false);
    }
    // 点击取消
    const handleCancel = () => {
        setIsModalVisible(false)
    }
    // 点击按钮
    const finish = (values: string) => {
        // 这里判断点击的value是哪个
        if (values === '保存') { // 添加
            store.tags.addArticleTags(label, value);
        } else if (values === '返回添加') {
            setFlag(false);
            setLabel('');
            setValue('');
            setId('');
        } else if (values === '删除') {
            setIsModalVisible(true);
        } else if (values === '更改') {
            store.tags.updateArticleTags(id, value, label);
        }
    }
    // 点击标签
    const click = (el: HTMLElement) => {
        // 点击span时
        if (el.nodeName === 'SPAN') {
            // 获取id值改变label和value
            let result = store.tags.getArticleTags(el.dataset.id!);
            setLabel(result.label);
            setValue(result.value);
            setId(result.id);
            setFlag(true);
        }
    }
    return <div className={styles.main}>
        <div className={styles.tags}>
            <div className={styles.left}>
                <div className={styles.ltitle}>
                    添加标签
                </div>
                <div className={styles.linput}>
                    <Input placeholder="输入标签名称" />
                    <Input placeholder="输入标签值（请输入英文，作为路由使用）" />
                    <Button type="primary" onClick={() => { store.tags.addArticleTags }}>保存</Button>
                </div>
            </div>
            <div className={styles.right}>
                <div className={styles.ltitle}>
                    所有标签
                </div>
                <ul className={styles.span}>
                    {
                        store.tags.tagsList.map(item => {
                            return <li key={item.id}>
                                <span>{item.label}</span>
                            </li>
                        })
                    }
                </ul>
            </div>
        </div>
    </div>
}

export default observer(Tags)