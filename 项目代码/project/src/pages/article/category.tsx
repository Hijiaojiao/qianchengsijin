// 文章管理下的分类管理
import React from "react"
import { Input } from 'antd';
import { Button } from 'antd';
import styles from "@/styles/category.less"

const Category:React.FC=()=>{
     return <main className={styles.cat_main}>
         <div className={styles.main_box}>
             <div className={styles.main_box_o} style={{padding:"0 8px"}}>
                 <div className={styles.main_box_one} >
                    <div className={styles.one_header}>
                        <div className={styles.ant_card_head_wrapper}>
                        <div className={styles.ant_card_head_title}>添加分类</div>
                        </div>
                    </div>
                    <div className={styles.one_main}>
                            <Input placeholder="输入分类名称" style={{margin:"0 0 20px 0"}}/>
                            <Input placeholder="输入分类值（请输入英文，作为路由使用）" style={{margin:"0 0 20px 0"}}/>
                            <Button type="primary">保存</Button>
                    </div>
                </div>
             </div>
             <div className={styles.main_box_t} style={{padding:"0 8px"}}>
                <div className={styles.main_box_two}>
                    <div className={styles.two_header}>
                        <div className={styles.cat_box}>
                            <div className={styles.cats}>所有分类</div>
                        </div>
                    </div>
                    <div className={styles.two_main}>
                        <ul className={styles.ZN93PXmnhjDJVgLo_o2fi}>
                            <li className={styles._7skkIE_ATciDEFbHHmO9T}></li>
                        </ul>
                    </div>
                </div>
            </div>
         </div> 
     </main>
}
export default Category