// 文章管理的所有文章
import React, { useEffect, useState } from 'react';
import styles from '@/styles/article.less';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/userStore';
import { Form, Input, Select, Button, Table, Badge, Tag } from 'antd';
import { articleItem } from '@/types';
import moment from 'moment';
import { useHistory } from 'umi';

interface TableItem {
  title: string;
  width: number;
  dataIndex: string;
  key: string;
  fixed: string;
}

const { Option } = Select;
const Article: React.FC<TableItem> = () => {
  const [page, usePage] = useState(1);
  const store = useStore();
  const article = store.article.articleList;
  const [params, setParams] = useState({});
  const history = useHistory();
  useEffect(() => {
    store.article.getArticelItem(page);
  }, [page]);

  // console.log(article);
  //处理首焦
  const handlecook = (id: string, isRecommended: boolean) => {
    store.article.articleList(id, !isRecommended);
  };
  //点击编辑跳转页面
  const handleAdd = () => {
    history.push('/article/editor');
  };
  //点击搜索
  function submit() {
    let values = form.getFieldsValue();
    console.log(values);
    let params: { [key: string]: string } = {};
    for (let key in values) {
      values[key] && (params[key] = values[key]);
    }
    setParams(params);
  }

  const [form] = Form.useForm();
  //点击搜索
  // function submit() {
  //   let values = form.getFieldValue();
  // }

  const columns = [
    {
      title: '标题',
      width: 200,
      dataIndex: 'title',
      fixed: 'left',
    },
    {
      title: '状态',
      dataIndex: 'status',
      width: 100,
      render: (status: string) => {
        if (status === 'publish') {
          return (
            <>
              <Badge status="success" />
              已发布
            </>
          );
        } else {
          return (
            <>
              <Badge status="warning" />
              草稿
            </>
          );
        }
      },
    },
    {
      title: '分类',
      dataIndex: 'category',
      width: 100,
      render: (row: any) => {
        if (row !== null) {
          return (
            <Tag color="magenta" key={row.id}>
              {row.label}
            </Tag>
          );
        } else {
          return <></>;
        }
      },
    },
    {
      title: '标签',
      key: 'tags',
      dataIndex: 'tags',
      render: (tags: any[]) => {
        return (
          <span>
            {tags.map((tag) => {
              let color = tag.length > 5 ? 'geekblue' : 'green';
              if (tag === 'loser') {
                color = 'volcano';
              }
              return (
                <Tag color={color} key={tag}>
                  {tag.label}
                </Tag>
              );
            })}
          </span>
        );
      },
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      width: 100,
      render: (views: number) => {
        if (views <= 9) {
          return (
            <div
              style={{
                width: '20px',
                height: '20px',
                background: 'rgb(82, 196, 26)',
                borderRadius: '50%',
                textAlign: 'center',
                color: '#fff',
              }}
            >
              {views}
            </div>
          );
        }
        return (
          <div
            style={{
              width: '30px',
              height: '20px',
              backgroundColor: 'rgb(82,196,26)',
              borderRadius: '10px',
              lineHeight: '20px',
              textAlign: 'center',
              color: '#fff',
            }}
          >
            {views}
          </div>
        );
      },
    },
    {
      title: '喜欢数',
      width: 100,
      dataIndex: 'likes',
      render: (likes: number) => {
        if (likes <= 9) {
          return (
            <div
              style={{
                width: '20px',
                height: '20px',
                background: 'rgb(235,47,150)',
                borderRadius: '50%',
                textAlign: 'center',
                color: '#fff',
              }}
            >
              {likes}
            </div>
          );
        }
        return (
          <div
            style={{
              width: '30px',
              height: '20px',
              background: 'rgb(235,47,150)',
              borderRadius: '10px',
              lineHeight: '20px',
              textAlign: 'center',
              color: '#fff',
            }}
          >
            {likes}
          </div>
        );
      },
    },

    {
      title: '发布时间',
      dataIndex: 'updateAt',
      width: 200,
      render: (updateAt: string) => {
        return <div>{moment(updateAt).format('YYY-MM--DD HH:mm:ss')}</div>;
      },
    },
    {
      title: '操作',
      fixed: 'right',
      width: 300,
      render: (record: articleItem) => (
        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
          <a href="" onClick={() => handleAdd()}>
            编辑
          </a>
          |
          <a
            href=""
            onClick={() => {
              handlecook(record.id, record.isRecommended);
            }}
          >
            {record.isRecommended ? '首焦推荐' : '撤销推荐'}
          </a>
          |<a href="">查看访问</a>|<a href="">删除</a>
        </div>
      ),
    },
  ];

  return (
    <div className={styles.ArticleMain}>
      <div className={styles.articleLeft}>
        <div>
          <Form className={styles.input} form={form} onFinish={submit}>
            <Form.Item style={{ float: 'left', marginLeft: '50px' }}>
              <span> 标题:</span>
              <Input
                placeholder="请输入与标题"
                style={{ width: '222.45px', height: '31.6px' }}
              />
            </Form.Item>
            <Form.Item style={{ marginLeft: '60px' }}>
              状态:
              <Select style={{ width: '180px', height: '32px' }}>
                <Option value="rmb">已发布</Option>
                <Option value="dollar">草稿</Option>
              </Select>
            </Form.Item>
            <Form.Item style={{ marginLeft: '60px' }}>
              分类:
              <Select style={{ width: '180px', height: '32px' }}>
                {store.article.articleList.map((item, index) => {
                  return item.tags.map((it, ind) => {
                    return (
                      <Option value={it.id} key={it.id}>
                        {it.label}
                      </Option>
                    );
                  });
                })}
              </Select>
            </Form.Item>
          </Form>
        </div>

        <div className={styles.btn}>
          <Button
            style={{ float: 'right' }}
            className={styles.but}
            onClick={() => {
              setParams({ ...params });
            }}
          >
            重置
          </Button>
          <Button
            htmlType="submit"
            type="primary"
            style={{ float: 'right', marginRight: '20px' }}
            className={styles.but}
            onClick={() => submit()}
          >
            搜索
          </Button>
        </div>
      </div>
      <div className={styles.articleRight}>
        <Table
          style={{ height: '567px' }}
          columns={columns}
          dataSource={store.article.articleList}
          scroll={{ x: 1500, y: 300 }}
        />
        ,
      </div>
    </div>
  );
};

export default observer(Article);
// function handlecook(id: string, isRecommended: boolean) {
//   throw new Error('Function not implemented.');
// }
