// 知识小册页面
import { observer } from "mobx-react-lite"
import Button from "@/components/Button"
import userStore from "@/context/userStore"
import { useEffect, useState } from "react"
import styles from "@/styles/knowledge.less"
import EditOutlined from "@ant-design/icons/lib/icons/EditOutlined"
import CloudUploadOutlined from "@ant-design/icons/lib/icons/CloudUploadOutlined"
import DeleteOutlined from "@ant-design/icons/lib/icons/DeleteOutlined"
import SettingOutlined from "@ant-design/icons/lib/icons/SettingOutlined"
import { Popconfirm, Tooltip } from "antd"
const knowledge: React.FC = () => {
    const store = userStore()
    console.log(store);
    const knowledgelist = store.knowledge.knowledgelist
    console.log(knowledgelist);
    const [page, setPage] = useState(1)
    const [pageSize, setSize] = useState(12)
    const [params, setParams] = useState({})
    useEffect(() => {
        store.knowledge.getknowledge(page, pageSize, params)
    }, [page, pageSize, params])

    function handleSwitch(id: string, status: string) {
        status === "publish" ?
            store.knowledge.switChover(id, status = "draft")
            : store.knowledge.switChover(id, status = "publish")
    }


    return (
        <div className={styles.knowledges}>
            <Button></Button>

            <div className={styles.content}>
                {
                    knowledgelist.map((item, index) => {
                        return <div key={index} className={styles.item}>
                            <div className={styles.items}>
                                <div className={styles.picture}>
                                    <img src={item.cover} alt="" />
                                </div>
                                <div className={styles.text}>
                                    <span>{item.title}</span>
                                </div>
                                <ul className={styles.uls}>
                                    <li>
                                        <span>
                                            <EditOutlined />
                                        </span>
                                    </li>
                                    <li>
                                        <span onClick={() => {
                                            handleSwitch(item.id, item.status)
                                        }}>
                                            <Tooltip title={item.status == "publish" ? "设为草稿" : "发布线上"}>
                                                {
                                                    item.status == "publish" ? <CloudUploadOutlined /> : <CloudUploadOutlined />
                                                }
                                            </Tooltip>
                                        </span>

                                    </li>
                                    <li>
                                        <span>
                                            <SettingOutlined />
                                        </span>
                                    </li>
                                    <li>
                                        <Popconfirm title="确认删除吗？" okText="确定" cancelText="取消" onConfirm={() => store.knowledge.deleteItem([item.id])}>
                                            <a href="#"> <span>
                                                <DeleteOutlined />
                                            </span></a>
                                        </Popconfirm>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    })
                }
            </div>
        </div >
    )
}


export default observer(knowledge)




