// 海报管理页面
import Fileupload from "@/components/Fileupload"
import Buttons from "@/components/Buttons"
import { observer } from "mobx-react-lite"


const poster: React.FC = () => {


    return (
        <div>
            <Fileupload></Fileupload>
            <Buttons></Buttons>
        </div >
    )
}


export default observer(poster)
