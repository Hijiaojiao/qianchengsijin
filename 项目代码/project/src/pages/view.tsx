// 访问统计页面
import styles from '@/styles/view.less';
import React from 'react';
import { NavLink } from 'react-router-dom';
import {
  Alert,
  Table,
  Tag,
  Space,
  Radio,
  Divider,
  Empty,
  Row,
  Col,
  Form,
  Input,
  Button,
} from 'antd';
import useStore from '@/context/userStore';
import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';

const columns = [
  {
    title: '发件人',
    dataIndex: 'name',
    render: (text: string) => <a>{text}</a>,
  },
  {
    title: '收件人',
    dataIndex: 'age',
  },
  {
    title: '主题',
    dataIndex: 'address',
  },
  {
    title: '发送时间',
    dataIndex: 'addresa',
  },
];

function mail() {
  // 获取数据
  const store = useStore();
  console.log(store);
  const fileList = store.file.filelist;
  const [page, setPage] = useState(1);

  useEffect(() => {
    store.file.getfile(page);
  }, [page]);

  interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
  }

  console.log(fileList);

  const data: DataType[] = [
    // {
    //   key: '1',
    //   name: 'John Brown',
    //   age: 32,
    //   address: 'New York No. 1 Lake Park',
    // },
  ];
  const message = (
    <div className={styles.msg}>
      系统检查到 <b>SMTP 未完善</b>，当收到评论时，无法进行邮件通知。
      <NavLink to="/setting">点击进行完善</NavLink>
    </div>
  );

  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
    getCheckboxProps: (record: DataType) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };

  const Demo = () => {
    const [selectionType, setSelectionType] = useState<'checkbox' | 'radio'>(
      'checkbox',
    );

    return (
      <div>
        <Radio.Group
          onChange={({ target: { value } }) => {
            setSelectionType(value);
          }}
          value={selectionType}
        >
          {/* <Radio value="checkbox">Checkbox</Radio>
          <Radio value="radio">radio</Radio> */}
        </Radio.Group>

        <Divider />

        <Table
          rowSelection={{
            type: selectionType,
            ...rowSelection,
          }}
          columns={columns}
          dataSource={data}
        />
      </div>
    );
  };
  return (
    <div className={styles.mail}>
      <div className={styles.from}>
        <Form>
          <Row>
            <Col span={6}>
              <b>IP： </b>
              <Form.Item>
                <Input placeholder="请输入IP地址"></Input>
              </Form.Item>
            </Col>
            <Col span={6}>
              <b>UA：</b>
              <Form.Item>
                <Input placeholder="请输入 User Agent"></Input>
              </Form.Item>
            </Col>
            <Col span={6}>
              <b>Url：</b>
              <Form.Item>
                <Input placeholder="请输入URL"></Input>
              </Form.Item>
            </Col>
            <Col span={6}>
              <b>请输入地址：</b>
              <Form.Item>
                <Input placeholder="请输入地址"></Input>
              </Form.Item>
            </Col>
            <Col span={6}>
              <b>浏览器：</b>
              <Form.Item>
                <Input placeholder="请输入浏览器"></Input>
              </Form.Item>
            </Col>
            <Col span={6}>
              <b>内核：</b>
              <Form.Item>
                <Input placeholder="请输入内核"></Input>
              </Form.Item>
            </Col>
            <Col span={6}>
              <b>OS：</b>
              <Form.Item>
                <Input placeholder="请输入操作系统"></Input>
              </Form.Item>
            </Col>
            <Col span={6}>
              <b>设备：</b>
              <Form.Item>
                <Input placeholder="请输入设备"></Input>
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col span={20}></Col>
            <Col span={3}>
              <Button type="primary">搜索</Button>
              <Button htmlType="reset">重置</Button>
            </Col>
          </Row>
        </Form>
      </div>
      <div className={styles.tab}>
        {/* <Table></Table> */}

        <Demo />
      </div>
    </div>
  );
}
export default observer(mail);
