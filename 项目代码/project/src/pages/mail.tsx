// 邮件管理页面
import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from '../styles/mail.less';
import {
  Alert,
  Table,
  Tag,
  Space,
  Radio,
  Divider,
  Empty,
  Row,
  Col,
  Form,
  Input,
  Button,
} from 'antd';

import useStore from '@/context/userStore';
import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';

const columns = [
  {
    title: '发件人',
    dataIndex: 'name',
    render: (text: string) => <a>{text}</a>,
  },
  {
    title: '收件人',
    dataIndex: 'age',
  },
  {
    title: '主题',
    dataIndex: 'address',
  },
  {
    title: '发送时间',
    dataIndex: 'addresa',
  },
];

const mail = () => {
  // 获取数据
  const store = useStore();
  console.log(store);
  // const mailList = store.mail.list
  // const [page, setPage] = useState(1)

  // useEffect(() => {
  //   store.mail.getmail(page);
  // }, [page])
  interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
  }

  // console.log(mailList);

  const data: DataType[] = [
    // {
    //   key: '1',
    //   name: 'John Brown',
    //   age: 32,
    //   address: 'New York No. 1 Lake Park',
    // },
  ];
  const message = (
    <div className={styles.msg}>
      系统检查到 <b>SMTP 未完善</b>，当收到评论时，无法进行邮件通知。
      <NavLink to="/setting">点击进行完善</NavLink>
    </div>
  );

  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
    getCheckboxProps: (record: DataType) => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  };

  const Demo = () => {
    const [selectionType, setSelectionType] = useState<'checkbox' | 'radio'>(
      'checkbox',
    );

    return (
      <div>
        <Radio.Group
          onChange={({ target: { value } }) => {
            setSelectionType(value);
          }}
          value={selectionType}
        >
          {/* <Radio value="checkbox">Checkbox</Radio>
          <Radio value="radio">radio</Radio> */}
        </Radio.Group>

        <Divider />

        <Table
          rowSelection={{
            type: selectionType,
            ...rowSelection,
          }}
          columns={columns}
          dataSource={data}
        />
      </div>
    );
  };
  return (
    <div className={styles.mail}>
      <Alert message={message} type="warning" />
      <div className={styles.from}>
        <Form>
          <Row>
            <Col span={8}>
              <b>发件人：</b>
              <Form.Item>
                <Input placeholder="请输入发件人"></Input>
              </Form.Item>
            </Col>
            <Col span={8}>
              <b>收件人：</b>
              <Form.Item>
                <Input placeholder="请输入收件人"></Input>
              </Form.Item>
            </Col>
            <Col span={8}>
              <b>主题：</b>
              <Form.Item>
                <Input placeholder="请输入主题"></Input>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={20}></Col>
            <Col span={3}>
              <Button type="primary">搜索</Button>
              <Button htmlType="reset">重置</Button>
            </Col>
          </Row>
        </Form>
      </div>
      <div className={styles.tab}>
        {/* <Table></Table> */}

        <Demo />
      </div>
    </div>
  );
};
export default observer(mail);
