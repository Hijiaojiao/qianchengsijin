// 评论管理页面
import useStore from '@/context/userStore';
import { ICommentItem } from '@/types';
import { Form, Input, Select, Button, Table, Badge } from 'antd';
import { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import styles from '@/styles/comment.less';

const { useForm } = Form;
const Comment: React.FC = () => {
  const [form] = useForm();
  const store = useStore();
  const [page, setPage] = useState(1);

  useEffect(() => {
    store.comment.getComment(page);
  }, [page]);
  function submit() {
    let values = form.getFieldsValue();
    console.log(values);
  }

  const columns = [
    {
      title: '状态',
      render: (row: ICommentItem) => {
        return row.pass ? (
          <Badge status="success" text="通过" />
        ) : (
          <Badge status="warning" text="未通过" />
        );
      },
    },
    {
      title: '称呼',
      dataIndex: 'replyUserName',
    },
    {
      title: '联系方式',
      dataIndex: 'replyUserEmail',
    },
    {
      title: '原始内容',
      dataIndex: 'content',
    },
    {
      title: 'HTML内容',
      dataIndex: 'html',
    },
    {
      title: '管理文章',
      dataIndex: 'url',
    },
    {
      title: '创建时间',
      dataIndex: 'createAt',
    },
    {
      title: '父级评论',
      dataIndex: 'parentCommentId',
    },
    {
      title: '操作',
      render: (row: ICommentItem) => {
        return (
          <p>
            <Button>通过</Button>
            <Button>拒绝</Button>
            <Button>回复</Button>
            <Button>删除</Button>
          </p>
        );
      },
    },
  ];

  return (
    <div style={{ margin: '20px' }}>
      <Form form={form} onFinish={submit} className={styles.table}>
        <div className={styles.tableTop}>
          <Form.Item
            name="replyUserName"
            label="称呼"
            style={{ float: 'left', marginLeft: '50px' }}
          >
            <Input
              type="text"
              placeholder=" 请输入称呼"
              style={{ width: '222.45px', height: '31.6px' }}
            />
          </Form.Item>
          <Form.Item
            name="replyUserEmail"
            label="邮箱"
            style={{ marginLeft: '60px' }}
          >
            <Input
              type="text"
              placeholder=" 请输入称呼"
              style={{ width: '222.45px', height: '31.6px' }}
            />
          </Form.Item>
          <Form.Item name="pass" label="状态" style={{ marginLeft: '60px' }}>
            <Select style={{ width: '180px', height: '32px' }}>
              <Select.Option value="true">已通过</Select.Option>
              <Select.Option value="false">未通过</Select.Option>
            </Select>
          </Form.Item>
        </div>
        <div className={styles.btn}>
          <Button htmlType="submit" className={styles.but}>
            搜索
          </Button>
          <Button htmlType="reset" className={styles.but}>
            重置
          </Button>
        </div>
      </Form>
      <Table
        columns={columns}
        dataSource={store.comment.commentList}
        style={{ marginTop: '30px' }}
      ></Table>
    </div>
  );
};

export default observer(Comment);
