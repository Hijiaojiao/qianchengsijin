// 系统设置页面
import React, { useEffect, useState } from "react"
import useStore from '@/context/userStore';
import { Tabs, Form, Input, Button, Drawer, Alert } from 'antd';
import styles from '@/styles/setting.less'
import {
    FileImageOutlined,
} from '@ant-design/icons';
import DrawerContent from '@/components/DrawerContent';

const Setting: React.FC = () => {
    const store = useStore();
    useEffect(() => {
        store.setting.setSetting();
    }, [])
    const [visible, setVisible] = useState(false);
    const showDrawer = () => {
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
    };
    const { TabPane } = Tabs;
    const [tabPosition, setTabPosition] = useState('left');
    const obj = store.setting.settingList;

    return <div className={styles.setting}>
        <Tabs tabPosition={tabPosition}>
            <TabPane tab="系统设置" key="1">
                <Form>
                    <Form.Item label="系统地址" className={styles.item} colon={false}>
                        <Input placeholder="请输入系统地址" value="creationad.shbwyz.com" />
                    </Form.Item>
                    <Form.Item label="后台地址" className={styles.item} colon={false}>
                        <Input placeholder="请输入后台地址" value="creationadmin.shbwyz.com" />
                    </Form.Item>
                    <Form.Item label="系统标题" className={styles.item} colon={false}>
                        <Input placeholder="请输入系统标题，将作为head.title显示" value="八维创作平台" />
                    </Form.Item>
                    <Form.Item label="Logo" className={styles.item} colon={false}>
                        <div style={{ display: 'flex' }}>
                            <Input placeholder="请输入Logo链接或选择文件，也可输入html" value="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" />
                            <div className={styles.icon}>
                                <FileImageOutlined onClick={showDrawer} />
                            </div>
                        </div>
                    </Form.Item>
                    <Form.Item label="Favicon" className={styles.item} colon={false}>
                        <div style={{ display: 'flex' }}>
                            <Input placeholder="请输入favicon链接或选择文件" value="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" />
                            <div className={styles.icon}>
                                <FileImageOutlined onClick={showDrawer} />
                            </div>
                        </div>
                    </Form.Item>
                    <Form.Item label="页脚信息" className={styles.item} colon={false}>
                        <Input.TextArea />
                    </Form.Item>
                    <Button type="primary">保存</Button>
                </Form>
                <Drawer
                    placement="right"
                    width={786}
                    closable={false}
                    onClose={onClose}
                    visible={visible}
                >
                    <DrawerContent></DrawerContent>
                </Drawer>
            </TabPane>
            <TabPane tab="国际化设置" key="2">
                国际化设置
            </TabPane>
            <TabPane tab="SEO设置" key="3">
                <Form>
                    <Form.Item label="关键词" className={styles.item} colon={false}>
                        <Input placeholder="请输入关键词，空格分割" value="八维创作平台" />
                    </Form.Item>
                    <Form.Item label="描述信息" className={styles.item} colon={false}>
                        <Input.TextArea />
                    </Form.Item>
                    <Button type="primary">保存</Button>
                </Form>
            </TabPane>
            <TabPane tab="数据统计" key="4">
                <Form.Item label="百度统计" className={styles.item} colon={false}>
                    <Input placeholder="请输入百度统计Id" value="8bd099eec64421b1831043373289e" />
                </Form.Item>
                <Form.Item label="谷歌分析" className={styles.item} colon={false}>
                    <Input placeholder="请输入谷歌分析Id" />
                </Form.Item>
                <Button type="primary">保存</Button>
            </TabPane>
            <TabPane tab="OSS设置" key="5">
                <Alert
                    message="说明"
                    description='请在编辑器中输入您的 oss 配置，并添加 type 字段区分 {"type":"aliyun","accessKeyId":"","accessKeySecret":"","bucket":"","https":true,"region":""}'
                    type="info"
                    showIcon
                />
            </TabPane>
            <TabPane tab="SMTP服务" key="6">
                <Form>
                    <Form.Item label="SMTP地址" className={styles.item} colon={false}>
                        <Input placeholder="请输入SMTP地址" />
                    </Form.Item>
                    <Form.Item label="SMTP端口（强制使用SSL连接）" className={styles.item} colon={false}>
                        <Input placeholder="请输入SMTP端口" />
                    </Form.Item>
                    <Form.Item label="SMTP用户" className={styles.item} colon={false}>
                        <Input placeholder="请输入SMTP用户" />
                    </Form.Item>
                    <Form.Item label="SMTP密码" className={styles.item} colon={false}>
                        <Input placeholder="也可能是授权码" />
                    </Form.Item>
                    <Form.Item label="发件人" className={styles.item} colon={false}>
                        <Input placeholder="请输入正确的邮箱地址" />
                    </Form.Item>
                    <Button type="primary">保存</Button>
                    <Button style={{ marginLeft: '16px' }}>测试</Button>
                </Form>
            </TabPane>
        </Tabs>
    </div>
}

export default Setting