// 个人中心页面
import React, { useEffect } from 'react';
import styles from '@/styles/ownspace.less';
import { Tabs, Avatar, Form, Input, Button, Drawer } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useState } from 'react';
import DrawerContent from '@/components/DrawerContent';

const { TabPane } = Tabs;

function callback(key: string) {
  console.log(key);
}
const onFinish = (values: string) => {
  console.log('Success:', values);
};

const onFinishFailed = (errorInfo: any) => {
  console.log('Failed:', errorInfo);
};

const Ownspace: React.FC = () => {
  const [visible, setVisible] = useState(false);

  //点击显示抽屉
  const onshowDrawer = () => {
    setVisible(!visible);
  };
  //点击抽屉隐藏
  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className={styles.ownspace}>
      <div className={styles.antList}>
        <div className={styles.antListheader}>系统概览</div>
        <ul>
          <li>累计发表了 10篇文章</li>
          <li>累计创建了 8个分类</li>
          <li>累计创建 5个标签</li>
          <li>累计上传 24个文件</li>
          <li>累计获取 36个评论</li>
        </ul>
      </div>

      <div className={styles.antCard}>
        <div className={styles.antListheader}>个人资料</div>
        <div className={styles.antCardList}>
          <div className={styles.tabList}>
            <Tabs defaultActiveKey="1" onChange={callback}>
              <TabPane tab="基本设置" key="1">
                <div
                  className={styles.antdAvatar}
                  style={{ position: 'relative', left: '50%' }}
                  onClick={() => onshowDrawer()}
                >
                  <Avatar
                    size={64}
                    icon={<UserOutlined />}
                    className={styles.antdVar}
                  />
                </div>
                <div
                  className={styles.table}
                  style={{
                    position: 'relative',
                    left: '-10%',
                    marginTop: '20px',
                  }}
                >
                  <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                  >
                    <Form.Item
                      label="用户名"
                      name="username"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your username!',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>

                    <Form.Item
                      label="密码"
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your password!',
                        },
                      ]}
                    >
                      <Input.Password />
                    </Form.Item>
                    <Form.Item
                      name="remember"
                      valuePropName="checked"
                      wrapperCol={{ offset: 8, span: 16 }}
                    ></Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                      <Button
                        type="primary"
                        htmlType="submit"
                        style={{ marginTop: '-100px', marginLeft: '-100px' }}
                      >
                        保存
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </TabPane>
              <TabPane tab="更新密码" key="2">
                <div style={{ position: 'relative', left: '-10%' }}>
                  <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                  >
                    <Form.Item
                      label="原密码"
                      name="username"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your username!',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>

                    <Form.Item
                      label="新密码"
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your password!',
                        },
                      ]}
                    >
                      <Input.Password />
                    </Form.Item>
                    <Form.Item
                      label="确认密码"
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: 'Please input your password!',
                        },
                      ]}
                    >
                      <Input.Password />
                    </Form.Item>
                    <Form.Item
                      name="remember"
                      valuePropName="checked"
                      wrapperCol={{ offset: 8, span: 16 }}
                    ></Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                      <Button
                        type="primary"
                        htmlType="submit"
                        style={{ marginTop: '-100px', marginLeft: '-100px' }}
                      >
                        更新
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </TabPane>
            </Tabs>
          </div>
        </div>
      </div>
      <Drawer
        title="文件选择"
        width={720}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <DrawerContent></DrawerContent>
      </Drawer>
    </div>
  );
};
export default Ownspace;
