import React from 'react';

import { Form, Input, Select, Button, Table } from 'antd';

const Page: React.FC = () => {
  return (
    <Form>
      <Form.Item name="name" label="称呼">
        <Input type="text" placeholder=" 请输入称呼" />
      </Form.Item>
      <Form.Item name="email" label="邮箱">
        <Input type="text" placeholder=" 请输入称呼" />
      </Form.Item>
      <Form.Item name="pass" label="状态">
        <Select>
          <Select.Option value="true">已通过</Select.Option>
          <Select.Option value="false">未通过</Select.Option>
        </Select>
      </Form.Item>
      <Button htmlType="submit">搜索</Button>
      <Button htmlType="reset">重置</Button>
    </Form>
  );
};
export default Page;
