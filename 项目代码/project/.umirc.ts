import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  antd: {},
  //开启dva 仓库管理
  mfsu: {},
  webpack5: {},
  links: [
    {
      rel: 'icon',
      href: 'https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png',
    },
  ],
  title: "一组后台",
  //路由懒加载
  dynamicImport: {
    // loading: "@/component/Spin"
  },   //按需加载 CMD规范
  // hash: true,//打包时生成hash
  // publicPath: process.env.NODE_ENV === 'production' ? '/1812A/houjinhong/' : '/',//资源线上路径
  // base: process.env.NODE_ENV === 'production' ? '/1812A/houjinhong' : '/',//路由前缀
  // // hash:true,//目的完成增量更新，使线上强缓存文件不会影响更新文件

  // analytics: {
  //   baidu: '928d7b1756e840f7ffe4072ae794850e',
  // },//添加埋点
  //国际化处理
  // locale: {
  //   default: 'zh-CN',
  //   antd: false,
  //   title: false,
  //   baseNavigator: true,
  //   baseSeparator: '-',
  // },
});
