import {
  getBanner,
  homeWidget,
  homeOperation,
  homeRecTabs,
  homeTabContent,
  getShopsByTab,
} from "@/services";

const state = {
  banners: [],
  widgets: [],
  operation: [],
  recTabs: [],
  tabContent: [],
  shops: [],
  shopCount: 0,
  shopQuery: {
    tabId: 41,
    showSort: 1,
    showSortMode: 1,
    pageNo: 1,
    pageSize: 10,
  },
};

const mutations = {
  update(state, payload) {
    for (let key in payload) {
      state[key] = payload[key];
    }
  },
};

const actions = {
  // 获取轮播图
  async getBanner({ commit }, payload) {
    let result = await getBanner();
    if (result.errNo === 0) {
      commit("update", {
        banners: result.data,
      });
    }
  },
  // 获取首页组件
  async homeWidget({ commit }) {
    let result = await homeWidget();
    if (result.errNo === 0) {
      commit("update", {
        widgets: result.data.items,
      });
    }
  },
  // 获取操作指南
  async homeOperation({ commit }) {
    let result = await homeOperation();
    if (result.errNo === 0) {
      commit("update", {
        operation: result.data,
      });
    }
    console.log("result+++", result.data);
  },
  // 获取推荐、附近美食 tab切换列表
  async homeRecTabs({ commit }) {
    let result = await homeRecTabs();
    console.log("recTabs...", result);
    if (result.errNo === 0) {
      commit("update", {
        recTabs: result.data,
      });
    }
  },
  // 通过tab获取店铺列表
  async getShopsByTab({ commit, state }) {
    let result = await getShopsByTab(state.shopQuery);
    if (result.errNo === 0) {
      let shops = result.data.data;
      console.log("shops...", shops);
      if (state.shopQuery.pageNo > 1) {
        shops = [...state.shops, ...shops];
      }
      commit("update", {
        shops,
        shopCount: result.data.totalCount,
      });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
