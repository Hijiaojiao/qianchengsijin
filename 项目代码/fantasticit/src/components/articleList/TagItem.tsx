import React from 'react';
import styles from './TagItem.less';
import { IRootState, tagListItem } from '@/types';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'umi';
import { useState, useEffect } from 'react';

const TagItem: React.FC = (props) => {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);

  useEffect(() => {
    dispatch({
      type: 'article/getTagList',
    });
  }, []);
  return (
    <div className={styles.tags}>
      <span>所有</span>
      {article.tagList.map((item, index) => {
        return (
          <NavLink to={`/category/${item.value}`} key={index}>
            <span>{item.label}</span>
          </NavLink>
        );
      })}
    </div>
  );
};
export default TagItem;
