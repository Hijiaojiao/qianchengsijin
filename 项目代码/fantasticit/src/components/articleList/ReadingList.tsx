import { IRootState } from '@/types';
import React from 'react';
import { useEffect, useState } from 'react';
import { NavLink, useDispatch, useSelector } from 'umi';
import styles from './readingList.less';
import moment from 'moment';

const ReadingList: React.FC = (props) => {
  const dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
    });
    dispatch({
      type: 'article/getLable',
    });
  }, []);
  console.log(article.lableList);

  return (
    <div className={styles.right}>
        <div className={styles.top_title}>推荐阅读</div>
        <ul className={styles.tagItem}>
          {article.recommend.map((item, index) => {
            return (
              <li key={index}>
                <span>
                  <NavLink to={`/archives/${item.id}`}>
                    <time dateTime={item.createAt}>{item.title}·一个月前</time>
                  </NavLink>
                </span>
              </li>
            );
          })}
        </ul>
      </div>
  
  );
};

export default ReadingList;
