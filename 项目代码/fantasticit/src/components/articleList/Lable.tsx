import React from 'react';
import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'umi';
import { IRootState } from '@/types';
import styles from './Lable.less';

const Lable: React.FC = () => {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);

  useEffect(() => {
    // 标签
    dispatch({
      type: 'article/getLable',
    });
  });

  return (
    <div className={styles.lable}>
      <h3>文章标签</h3>
      {article.lableList.map((item, index) => {
        return (
          <p key={index}>
            {item.label} [<span>{item.articleCount}</span>]
          </p>
        );
      })}
    </div>
  );
};
export default Lable;
