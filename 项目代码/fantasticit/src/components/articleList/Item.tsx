import React from 'react';
import { IArticleItem } from '@/types';
import Moment from '@/utils/moment';
import { NavLink } from 'react-router-dom';
import {
  ShareAltOutlined,
  HeartOutlined,
  EyeOutlined,
} from '@ant-design/icons';

import styles from './Item.less'; // 等于启用了css-module
import TagItem from './TagItem';
import share from '../Share';
import { useHistory } from 'umi';

interface Props {
  item: Partial<IArticleItem>;
}
function shareArticle(e: React.MouseEvent, item: IArticleItem) {
  e.stopPropagation();
  share(item);
}

const ArticleItem: React.FC<Props> = (props) => {
  const history = useHistory();
  return (
    <div className={styles.list}>
      <div className={styles.title}>
        <h3 onClick={() => history.push(`/archives/${props.item.id}`)}>
          {props.item.title}
        </h3>
        <span style={{ margin: '0 8px' }}>|</span>
        <span>{Moment(props.item.createAt, 'YYYYMMDD').fromNow()}</span>
        {props.item.category?.label ? (
          <span style={{ margin: '0 8px' }}>|</span>
        ) : undefined}
        <span>{props.item.category?.label}</span>
      </div>
      <dl>
        <dt>
          <div className={styles.imgs}>
            {props.item.cover ? (
              <img src={props.item.cover} alt="" />
            ) : undefined}
          </div>
        </dt>
        <dd>
          <p>{props.item.summary}</p>
          <p>
            <span>
              <HeartOutlined />
              {props.item.likes}
            </span>
            <span style={{ margin: '0 8px' }}>·</span>
            <span>
              <EyeOutlined />
              {props.item.views}
            </span>
            <span style={{ margin: '0 8px' }}>·</span>
            <span onClick={(e) => shareArticle(e, props.item)}>
              <ShareAltOutlined />
              分享
            </span>
          </p>
        </dd>
      </dl>
    </div>
  );
};
export default ArticleItem;
