import React, { useState, ChangeEvent } from 'react'
import styles from './index.less'
import { emojis } from '@/utils/emojis';
import { message, Button, Form, Input, Popover } from 'antd';
import { SmileOutlined } from '@ant-design/icons'

const InputBox: React.FC = () => {
    const [user, setuser] = useState(false);
    const [popover, setpopover] = useState(false);
    const [text, settext] = useState('');
    const [about, setabout] = useState(false);

    // 输入框内容
    let edit = (e?: ChangeEvent<HTMLTextAreaElement>, emojis?: string) => {
        if (e && !emojis) {
            settext(e.target.value)
            e.target.value.length > 0 ? setabout(true) : setabout(false)
        }
        emojis && settext(text + emojis)
    }
    // 改变弹窗的状态
    let shade = () => {
        let users = localStorage.getItem('user')
        users ? setuser(false) : setuser(true)
    }
    // 点击关闭弹窗
    let close = () => {
        setuser(false)
    }
    // 改变弹窗的出现和隐藏状态
    let VisibleChange = () => {
        if (localStorage.getItem('user')) {
            setpopover(!popover)
        } else {
            setpopover(false)
        }
    }
    // 添加小表情
    let addemojis = (item: string) => {
        console.log(item);
        // console.log(dom.current);
        settext(text + item)
    }
    // 小表情列表
    let content = () => {
        return <ul className={styles.emojis}>
            {
                Object.values(emojis).map((item, index) => {
                    return <li key={index} onClick={(e) => {
                        edit((e as unknown as ChangeEvent<HTMLTextAreaElement>), item)
                        addemojis(item)
                    }}>
                        {item}
                    </li>
                })
            }
        </ul>
    }
    // 将user的状态储存到本地
    const onFinish = (values: any) => {
        localStorage.setItem('user', JSON.stringify(values))
        setuser(false)
    };
    // 全局提示
    const success = () => {
        message.success('评论成功，已提交审核');
        settext('')
    };

    return <div className={styles.con}>
        {/* 输入框部分 */}
        <div className={styles.inp}>
            <div className={styles.message}>
                <textarea onChange={edit} placeholder="请输入评论内容（支持 Markdown）" className={styles.texts}></textarea>
            </div>
            <p>
                <span onClick={shade} className={styles.reply}>
                    {/* placement：气泡框位置(放在左下角)，onVisibleChange：显示隐藏的回调，visible：用于手动控制浮层显隐，trigger：触发行为(点击) */}
                    <Popover placement="bottomLeft" content={content} visible={popover} onVisibleChange={VisibleChange} trigger="click">
                        <SmileOutlined />
                        <span> 表情</span>
                    </Popover>
                </span>
                <button className={about ? styles.disable : undefined} onClick={success}>发布</button>
            </p>
        </div>
        {/* 弹窗验证身份部分 */}
        {
            user ? <div className={styles.shade} onClick={close}>
                <div onClick={(e) => {
                    e.stopPropagation()
                }}>
                    <p>
                        <span>请设置你的信息</span>
                        <span onClick={close}>X</span>
                    </p>
                    <Form
                        onFinish={onFinish}
                    >
                        <Form.Item
                            label="名称："
                            name="username"
                            rules={[{ required: true, message: '请输入您的称呼' }]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="邮箱："
                            name="email"
                            rules={[{ required: true, pattern: /^\w+@\w+\.(com|cn|net)$/, message: '输入合法邮箱地址，以便在收到回复时邮件通知' }]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item wrapperCol={{ offset: 8, span: 16 }} style={{ textAlign: 'right' }}>
                            <Button style={{ margin: "0 10px 0 0" }} onClick={close}>
                                取消
                            </Button>
                            <Button type="primary" htmlType="submit" danger>
                                设置
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div> : null
        }
    </div>
}

export default InputBox