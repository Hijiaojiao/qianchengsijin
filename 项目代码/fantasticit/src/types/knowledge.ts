export interface knowObject {
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}

export interface RootObject {
  id: string;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: any;
  tags: any[];
}

export interface  articleObject {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}

export interface knowDtail { 
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  children: Child[];
}

interface Child {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

export interface detailObject {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

export interface DtailObject {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}