import { ReactNode } from "react";
//归档目录类型

export interface Catalogue {
    level: ReactNode;
    id: string;
    title: string;
    cover: string;
    summary: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    category: Category;
    tags: any[];
}

interface Category {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
}
