export interface ArchivesItem {
    id: string;
    title: string;
    cover?: any;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    password?: any;

}
