export interface IState {
    name: string;
    commentData: IComment[];
    recommentData: IRecomment[];
}

interface IComment {
    id: string;
    name: string;
    email: string;
    content: string;
    html?: string;
    pass: boolean;
    userAgent: string;
    hostId: string;
    url: string;
    parentCommentId?: any;
    replyUserName?: any;
    replyUserEmail?: any;
    createAt: string;
    updateAt: string;
    children: Child[];
}

interface Child {
    id: string;
    name: string;
    email: string;
    content: string;
    html: string;
    pass: boolean;
    userAgent: string;
    hostId: string;
    url: string;
    parentCommentId: string;
    replyUserName: string;
    replyUserEmail: string;
    createAt: string;
    updateAt: string;
}

interface IRecomment {
    id: string;
    title: string;
    cover?: string;
    summary?: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    category?: Category;
    tags: Category[];
}

interface Category {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
}

export interface plItem {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children: any[];
}