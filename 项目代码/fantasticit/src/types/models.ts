import { ArticleModelState } from '@/models/article';
import { AboutModelState } from '@/models/about';
import { knowModelState } from '@/models/knowledge';
import { ArchivesModelState } from '@/models/archives';
import { CategoryModelState } from '@/models/category';
import { LanguageModelState } from '@/models/language';

export interface IRootState {
  msgboard: any;
  article: ArticleModelState;
  archives: ArchivesModelState;
  about: AboutModelState;
  knowledge: knowModelState;
  archivesDetail: ArchivesModelState;
  category: CategoryModelState;
  catalogueList: ArchivesModelState;
  LanguageModelState: LanguageModelState,
  loading: {
    global: boolean;
  }
}
