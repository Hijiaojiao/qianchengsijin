// @ts-nocheck
import React from 'react';
import { ApplyPluginsType, dynamic } from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/node_modules/@umijs/runtime';
import * as umiExports from './umiExports';
import { plugin } from './plugin';
import LoadingComponent from '@/layouts/blogLayout';

export function getRoutes() {
  const routes = [
  {
    "path": "/",
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'layouts__index' */'@/layouts/index.tsx'), loading: LoadingComponent}),
    "routes": [
      {
        "path": "/archives/:id",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__archives__id' */'@/pages/archives/[id].tsx'), loading: LoadingComponent})
      },
      {
        "path": "/archives",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__archives' */'@/pages/archives.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/category/be",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__category__be' */'@/pages/category/be.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/category/fe",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__category__fe' */'@/pages/category/fe.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/category/LeetCode",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__category__LeetCode' */'@/pages/category/LeetCode.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/category/Linux",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__category__Linux' */'@/pages/category/Linux.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/category/news",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__category__news' */'@/pages/category/news.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/category/reading",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__category__reading' */'@/pages/category/reading.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__index' */'@/pages/index.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/knowDtail/:id",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__knowDtail__id__index' */'@/pages/knowDtail/[id]/index.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/knowDtail/:id/:id",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__knowDtail__id__id' */'@/pages/knowDtail/[id]/[id].tsx'), loading: LoadingComponent})
      },
      {
        "path": "/knowledge",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__knowledge' */'@/pages/knowledge.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/page/about",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__page__about' */'@/pages/page/about.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/page/Detail",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__page__Detail' */'@/pages/page/Detail.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/page/msgboard",
        "exact": true,
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__page__msgboard' */'@/pages/page/msgboard.tsx'), loading: LoadingComponent})
      },
      {
        "path": "/main",
        "routes": [
          {
            "path": "/main/home",
            "exact": true,
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__main__home' */'@/pages/main/home.tsx'), loading: LoadingComponent})
          }
        ],
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__main___layout' */'@/pages/main/_layout.tsx'), loading: LoadingComponent})
      }
    ]
  }
];

  // allow user to extend routes
  plugin.applyPlugins({
    key: 'patchRoutes',
    type: ApplyPluginsType.event,
    args: { routes },
  });

  return routes;
}
