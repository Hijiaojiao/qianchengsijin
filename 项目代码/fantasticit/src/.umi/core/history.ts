// @ts-nocheck
import { createBrowserHistory, History } from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/node_modules/@umijs/runtime';

let options = {
  "basename": "/1812B/wangjiaojiao/fantasticit"
};
if ((<any>window).routerBase) {
  options.basename = (<any>window).routerBase;
}

// remove initial history because of ssr
let history: History = process.env.__IS_SERVER ? null : createBrowserHistory(options);
export const createHistory = (hotReload = false) => {
  if (!hotReload) {
    history = createBrowserHistory(options);
  }

  return history;
};

export { history };
