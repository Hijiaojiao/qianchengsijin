// @ts-nocheck
import { Component } from 'react';
import { ApplyPluginsType } from 'umi';
import dva from 'dva';
// @ts-ignore
import createLoading from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/node_modules/dva-loading/dist/index.esm.js';
import { plugin, history } from '../core/umiExports';
import ModelAbout0 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/about.ts';
import ModelArchives1 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/archives.ts';
import ModelArticle2 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/article.ts';
import ModelCategory3 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/category.ts';
import ModelKnowledge4 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/knowledge.ts';
import ModelLanguage5 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/language.ts';
import ModelMsgboard6 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/msgboard.ts';
import ModelPoster7 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/poster.ts';
import ModelSearch8 from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/src/models/search.ts';
import dvaImmer, { enableES5, enableAllPlugins } from 'C:/Users/86150/Desktop/qianchengsijin/项目代码/fantasticit/node_modules/dva-immer/dist/index.js';

let app:any = null;

export function _onCreate(options = {}) {
  const runtimeDva = plugin.applyPlugins({
    key: 'dva',
    type: ApplyPluginsType.modify,
    initialValue: {},
  });
  app = dva({
    history,
    
    ...(runtimeDva.config || {}),
    // @ts-ignore
    ...(typeof window !== 'undefined' && window.g_useSSR ? { initialState: window.g_initialProps } : {}),
    ...(options || {}),
  });
  
  app.use(createLoading());
  app.use(dvaImmer());
  (runtimeDva.plugins || []).forEach((plugin:any) => {
    app.use(plugin);
  });
  app.model({ namespace: 'about', ...ModelAbout0 });
app.model({ namespace: 'archives', ...ModelArchives1 });
app.model({ namespace: 'article', ...ModelArticle2 });
app.model({ namespace: 'category', ...ModelCategory3 });
app.model({ namespace: 'knowledge', ...ModelKnowledge4 });
app.model({ namespace: 'language', ...ModelLanguage5 });
app.model({ namespace: 'msgboard', ...ModelMsgboard6 });
app.model({ namespace: 'poster', ...ModelPoster7 });
app.model({ namespace: 'search', ...ModelSearch8 });
  return app;
}

export function getApp() {
  return app;
}

/**
 * whether browser env
 * 
 * @returns boolean
 */
function isBrowser(): boolean {
  return typeof window !== 'undefined' &&
  typeof window.document !== 'undefined' &&
  typeof window.document.createElement !== 'undefined'
}

export class _DvaContainer extends Component {
  constructor(props: any) {
    super(props);
    // run only in client, avoid override server _onCreate()
    if (isBrowser()) {
      _onCreate()
    }
  }

  componentWillUnmount() {
    let app = getApp();
    app._models.forEach((model:any) => {
      app.unmodel(model.namespace);
    });
    app._models = [];
    try {
      // 释放 app，for gc
      // immer 场景 app 是 read-only 的，这里 try catch 一下
      app = null;
    } catch(e) {
      console.error(e);
    }
  }

  render() {
    let app = getApp();
    app.router(() => this.props.children);
    return app.start()();
  }
}
