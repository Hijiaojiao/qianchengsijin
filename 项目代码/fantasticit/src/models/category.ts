import { Effect, Reducer } from 'umi';
import {
  getBe,
  getFe,
  getReading,
  getLinux,
  getLeetCode,
  getNews,
} from '@/services';
import { TagDetail } from '@/types';

export interface CategoryModelState {
  //前端
  FeDetailList: TagDetail[];
  //后端
  BeDetailList: TagDetail[];
  //阅读
  ReadingList: TagDetail[];
  //Linux
  LiunxData: TagDetail[];
  //leetCode
  leetCodeList: TagDetail[];
  //NewsDta
  newList: TagDetail[];
}
export interface CategoryModelType {
  namespace: 'category';
  state: CategoryModelState;
  //异步请求
  effects: {
    //前端详情
    getBe: Effect;
    getFe: Effect;
    getReading: Effect;
    getLinux: Effect;
    getLeetCode: Effect;
    getNews: Effect;
  };
  //同步
  reducers: {
    save: Reducer<CategoryModelState>;
  };
}

const CategoryModel: CategoryModelType = {
  namespace: 'category',
  state: {
    //前端
    FeDetailList: [],
    //后端
    BeDetailList: [],
    //阅读
    ReadingList: [],
    //Liunx
    LiunxData: [],
    //leetCode
    leetCodeList: [],
    //要闻
    newList: [],
  },
  effects: {
    //要闻
    *getNews({ payload }, { call, put }) {
      let result = yield call(getNews);
      // console.log('getreding+++', result);
      //判断是否成功
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { newList: [...result.data[0]] },
        });
      }
    },

    *getLeetCode({ payload }, { call, put }) {
      let result = yield call(getLeetCode);
      // console.log('getreding+++', result);
      //判断是否成功
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { leetCodeList: [...result.data[0]] },
        });
      }
    },

    // linux
    *getLinux({ payload }, { call, put }) {
      let result = yield call(getLinux);
      console.log('getLinux+++', result);
      //判断是否成功
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { LiunxData: [...result.data[0]] },
        });
      }
    },

    //关于阅读的详情
    *getReading({ payload }, { call, put }) {
      let result = yield call(getReading);
      console.log('getreding+++', result);
      //判断是否成功
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { ReadingList: [...result.data[0]] },
        });
      }
    },
    //关于前端的详情
    *getFe({ payload }, { call, put }) {
      let result = yield call(getFe);
      //判断是否成功
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { FeDetailList: [...result.data[0]] },
        });
      }
    },
    //关于后端的详情
    *getBe({ payload }, { call, put }) {
      let result = yield call(getBe);
      //判断是否成功
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { BeDetailList: [...result.data[0]] },
        });
      }
    },
  },

  reducers: {
    save(state, action) {
      console.log(state, action);
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default CategoryModel;
