import { IArticleItem, tagListItem, lableItem, ArchivesDetail } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {
  getArticleList,
  getRecommend,
  getTagList,
  getSwiper,
  getLable,
} from '@/services';

export interface ArticleModelState {
  recommend: IArticleItem[];
  articleList: IArticleItem[];
  articleCount: number;
  swiperlist: IArticleItem[];
  tagList: tagListItem[];
  lableList: lableItem[];
 
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getRecommend: Effect;
    getArticleList: Effect;
    //轮播图的接口数据
    getSwiper: Effect;
    getTagList: Effect;
    getLable: Effect;
  
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const ArticleModel: ArticleModelType = {
  namespace: 'article',
  state: {
    //推荐阅读的接口
    recommend: [],
    articleList: [],
    articleCount: 0,
    swiperlist: [],
    //标签
    tagList: [],
    lableList: [],
     
  },
  effects: {
 
    //右侧标签
    *getLable({ payload }, { call, put }) {
      let res = yield call(getLable);
      console.log('lable+++', res);
      //判断是否获取成功
      if (res.success === true) {
        yield put({
          type: 'save',
          payload: {
            lableList: res.data,
          },
        });
      }
    },
    //标签
    *getTagList({ payload }, { call, put }) {
      let res = yield call(getTagList);
      console.log('result++++', res);
      //判断是否获取数据成功
      if (res.success === true) {
        yield put({
          type: 'save',
          payload: {
            tagList: res.data,
          },
        });
      }
    },
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { recommend: result.data },
        });
      }
    },

    *getArticleList({ payload }, { call, put }) {
      let result = yield call(getArticleList, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            articleList: result.data[0],
            articleCount: result.data[1],
          },
        });
      }
    },
    //轮播图
    *getSwiper({ payload }, { call, put }) {
      let result = yield call(getSwiper, payload);
      console.log(result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            swiperlist: result.data,
          },
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default ArticleModel;
