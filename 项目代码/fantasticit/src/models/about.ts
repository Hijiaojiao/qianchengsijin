// 这是状态管理

import { IRecommend, ICommentList } from '@/types';
import { Effect, Reducer } from 'umi';
import { getRecommend, getCommentList } from '@/services';

export interface AboutModelState {
  recommendList: IRecommend[];
  commentList: ICommentList[];
  commentCount: number;
}

export interface AboutModelType {
  namespace: 'about';
  state: AboutModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getRecommend: Effect;
    getCommentList: Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<AboutModelState>;
  };
}


const AboutModel: AboutModelType = {
  namespace: 'about',
  state: {
    recommendList: [],
    commentList: [],
    commentCount: 0,
  },
  effects: {
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      console.log('result1...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { recommendList: result.data }
        })
      }
    },
    *getCommentList({ payload }, { call, put }) {
      let result = yield call(getCommentList, { ...payload });
      console.log('result2...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { commentList: result.data[0], commentCount: result.data[1] }
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      console.log(state, action);

      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default AboutModel;