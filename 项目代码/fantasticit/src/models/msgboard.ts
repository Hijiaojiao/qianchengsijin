import { Effect, Reducer, Subscription } from 'umi';
import { getCommentData ,getRecommentData} from '../services/modules/msgboard'

import { IRootState } from '../types'

export interface IndexModelState {
    name: string,
    commentData: IRootState[],
    recommentData: IRootState[],
}
export interface IndexModelType {
    namespace: 'msgboard';
    state: IndexModelState;
    effects: {
        getCommentData: Effect;
        getRecommentData: Effect;
    };
    reducers: {
        save: Reducer<IndexModelState>;
        // 启用 immer 之后
        // save: ImmerReducer<IndexModelState>;
    };
    subscriptions?: { setup: Subscription };
}
const msgboard: IndexModelType = {
    // 命名空间
    namespace: 'msgboard',

    // redux存储状态
    state: {
        name: "lml",
        commentData: [],
        recommentData: []
    },
    // 异步action
    effects: {
        *getCommentData({ payload }, { call, put }) {
            let res = yield call(getCommentData,payload);
            console.log('result++++', res);
            //判断是否获取数据成功
            if (res.success === true) {
              yield put({
                type: 'save',
                payload: {
                    commentData: res.data
                },
              });
            }
          },
        *getRecommentData({ payload }, { call, put }) {

            let result = yield call(getRecommentData)
            console.log(result);
            
            yield put({
                type: 'save',
                payload: {
                    recommentData: result.data
                }
            })
        },     
    },

    // 同步action
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    },
};

export default msgboard;