// 这是状态管理
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface LanguageModelState {
  locale: string;
  locales: { lable: string; locale: string }[];
}

export interface LanguageModelType {
  namespace: 'language';
  state: LanguageModelState;
  // 异步action，相当于vuex中的action
  effects: {};
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<LanguageModelState>;
  };
}

const LanguageModel: LanguageModelType = {
  namespace: 'language',
  state: {
    locale: 'zh-CN',
    locales: [
      {
        lable: '汉语',
        locale: 'zh-CN',
      },
      {
        lable: '英文',
        locale: 'en-US',
      },
    ],
  },
  effects: {},
  reducers: {
    save(state, action) {
      console.log(state, action);
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default LanguageModel;
