import { knowObject,RootObject, articleObject,knowDtail,detailObject} from '@/types';
import { Effect, Reducer} from 'umi';
import { getArticle, getknowList, getRecommends, getKnowDetail, getKnowDetailtwo} from '@/services';



export interface knowModelState {
  title:"string"
  knowList: knowObject [];
  recommend:RootObject[];
  article:articleObject[];
  knowDtail: {[key:string]:any};
  knowDtailtwo:Partial<detailObject>;
  knowCount: number;
}

export interface knowModelType {
  namespace: 'knowledge';
  state:knowModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getknow: Effect;
    getRecommends: Effect;
    getArticle:Effect;
    getKnowDetail:Effect;
    getKnowDetailtwo:Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<knowModelState>;
  };
}

const knowModel: knowModelType = {
  namespace: 'knowledge',
  state: {
    title:"string",
    knowList: [],
    recommend:[],
    article:[],
    knowCount: 0,
    knowDtail:{},
    knowDtailtwo:{},
  },
  
  effects: {
    *getknow({payload}, { call, put }) {
      let result = yield call(getknowList);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {knowList: result.data[0],knowCount:result.data[1]}
        })
      }
    },
    *getRecommends({ payload }, { call, put }) {
      let result = yield call(getRecommends);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { recommend: result.data },
        });
      }
    },
    *getArticle({payload},{call, put }) {     
      let result = yield call(getArticle);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { article: result.data },
        });
      }
    },
    *getKnowDetail({payload},{call, put }){
      let result = yield call(getKnowDetail,payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { knowDtail: result.data },
        });
      }
    },
    *getKnowDetailtwo({payload},{call, put}){
      let result = yield call(getKnowDetailtwo, payload);
      console.log('result...', result);
      if(result.success === true){
          yield put({
            type: 'save',
            payload: { knowDtailtwo:result.data }
          })
      }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default knowModel;