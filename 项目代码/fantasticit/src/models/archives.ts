import { ArchivesItem, ArchivesDetail, Catalogue } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getArchives, getArchivesDetail, getcatalogue } from '@/services';

export interface ArchivesModelState {
    archives: {
        [key: string]: {
            [key: string]: ArchivesItem[];
        };
    };
    archivesDetail: Partial<ArchivesDetail>;
    catalogueList: Catalogue[];
}
export interface ArchivesModelType {
    namespace: 'archives';
    state: ArchivesModelState;
    // 异步action，相当于vuex中的action
    effects: {
        getArchives: Effect;
        getArchivesDetail: Effect;
        getCatalogue: Effect;
    };
    // 同步action，相当于vuex中的mutation
    reducers: {
        save: Reducer<ArchivesModelType>;
    };
}
const ArchivesModel: ArchivesModelType = {
    namespace: 'archives',
    state: {
        archives: {},
        archivesDetail: {},
        catalogueList: [],
    },
    effects: {
        //获取归档数据
        *getArchives({ payload }, { call, put }) {
            let result = yield call(getArchives);
            // console.log('result...', result);
            if (result.success === true) {
                yield put({
                    type: 'save',
                    payload: { archives: result.data },
                });
            }
        },

        // 获取归档详情数据
        *getArchivesDetail({ payload }, { call, put }) {
            let result = yield call(getArchivesDetail, payload);
            console.log('result...', result);
            if (result.success === true) {
                yield put({
                    type: 'save',
                    payload: { archivesDetail: result.data },
                });
            }
        },

        //获取归档详情右边目录
        *getCatalogue({ payload }, { call, put }) {
            let result = yield call(getcatalogue, payload);
            console.log(result, '目录');
            if (result.success == true) {
                yield put({
                    type: 'save',
                    payload: { catalogueList: result.data },
                });
            }
        },
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    },
};
export default ArchivesModel;
