function theTime(Time: string | number) {
  let startTime = +new Date(Time);
  let endTime = +new Date();
  let theTimes = Math.floor(endTime - startTime);
  if (theTimes < 60 * 60 * 1000) {
    return Math.floor(theTimes / (60 * 1000)) + '分钟前';
  } else if (theTimes < 24 * 60 * 60 * 1000) {
    return Math.floor(theTimes / (60 * 60 * 1000)) + '小时前';
  } else if (theTimes < 30 * 24 * 60 * 60 * 1000) {
    return Math.floor((theTimes / 24) * 60 * 60 * 1000) + '天前';
  } else if (theTimes < 12 * 30 * 24 * 60 * 60 * 1000) {
    return Math.floor(theTimes / (30 * 24 * 60 * 60 * 1000)) + '月前';
  } else {
    return Math.floor(theTimes / (12 * 30 * 24 * 60 * 60 * 1000)) + '年前';
  }
}
export default theTime;
