import { request } from 'umi';
// 获取小册列表
export function getknowList(page: number, pageSize = 6, status = 'publish'){
    return request('/api/knowledge', {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            status,      
        },
        // 表示请求体里面传递的数据
        data: {},
    })
}

// 获取推荐
export function getRecommends(){
    return request('/api/article/recommend');
}
//获取文章
export function getArticle(articleStatus = "pulish"){
    return request("/api/category")
}

//获取详情页
export function getKnowDetail(id:string){
    console.log(id);
     return request(`/api/knowledge/${id}`, {
         method:"GET"
     })
}

//获取详情
export function getKnowDetailtwo(id:string){
    console.log(id);
    
   return request(`/api/knowledge/${id}/views`, {
       method:"POST"
   })
}