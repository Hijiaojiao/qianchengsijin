// 这里写所有的网络请求模块
import { request } from 'umi';

// 获取关于页面评论列表的内容
export function getCommentList({ page, pageSize = 6, id }: { page: number, pageSize?: number, id: string }) {
    return request(`/api/comment/host/${id}`, {
        // 表示请求地址栏上面的查询参数
        params: {
            page, pageSize
        },
        // 表示请求体里面传递的数据
        data: {}
    })
}
// 获取关于页面下部图文列表的接口
export function getAboutRecommend() {
    return request('/api/article/recommend');
}