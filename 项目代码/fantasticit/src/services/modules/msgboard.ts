import {request} from 'umi'

export const getCommentData = (page:number,pagesize=6) => {
    return request('/api/comment/host/8d8b6492-32e5-44e5-b38b-9a479d1a94bd',{
        method:'GET',
        params:{
            page,
            pagesize
        }
        
    })
}

export const getRecommentData = () => {
    return request('/api.blog.wipi.tech/api/article/recommend')
}
