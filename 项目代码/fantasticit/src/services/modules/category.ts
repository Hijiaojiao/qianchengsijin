import { request } from 'umi';

//后端接口
export function getBe() {
  return request('/api/article/category/be');
}
//前端接口
export function getFe() {
  return request('/api/article/category/fe');
}
//阅读的接口
export function getReading() {
  return request('/api/article/category/reading');
}
//Liunx
export function getLinux() {
  return request('/api/article/category/linux');
}
//leetCode
export function getLeetCode(){
  return request('/api/article/category/leetcode')
}
//news
export function getNews(){
  return request('/api/article/category/news')
}