import { request } from 'umi';
// 获取文章列表
export function getArticleList(page: number,pageSize = 12,status = 'publish',) {
  return request('/api/article', {
    // 表示请求地址栏上面的查询参数
    params: {
      page,
      pageSize,
      status,
    },
    // 表示请求体里面传递的数据
    data: {},
  });
}

// 获取推荐文章
export function getRecommend() {
  return request('/api/article/recommend');
}

//请求轮播图的接口
export function getSwiper() {
  return request('/api/article/all/recommend');
}

//请求导航的接口
export function getTagList() {
  return request('/api/category');
}
//请求标签的接口
export function getLable() {
  return request('/api/tag');
}
//详情页
export function getArticleDetail(id: string) {
  request(`/api/article/${id}/views`, {
    method: 'POST',
  });
}
