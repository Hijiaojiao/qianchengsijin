import {request} from 'umi';

export function getkeyword(keyword=""){
    return request('/api/search/article',{
        params:{
            keyword
        },

        data:{},
    })
}