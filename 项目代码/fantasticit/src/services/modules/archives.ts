// 写所有的网络请求模块
import { request } from "umi"
//获取归档数据
export function getArchives() {
    return request('/api/article/archives');
}

// 获取归档详情页面数据
export function getArchivesDetail(id: string) {
    return request(`/api/article/${id}/views`, {
        method: "POST"
    })
}

// 获取归档详情目录
export function getcatalogue(id: string) {
    return request(`/api/article/${id}/views`, {
        method: "POST"
    })
}

