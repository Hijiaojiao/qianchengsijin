import React from 'react';
import { IRootState } from '@/types';
import styles from './classification.less';
import { NavLink, useDispatch, useSelector } from 'umi';
import { useState } from 'react';
import { useEffect } from 'react';

const classNames = require('classnames');

const Classification: React.FC = () => {
  const dispatch = useDispatch();
  const knows = useSelector((state: IRootState) => state.knowledge);

  useEffect(() => {
    dispatch({
      type: 'knowledge/getArticle',
    });
  }, []);

  return (
    <div className={classNames(styles.box_right_two)}>
      <div className={classNames(styles._2b0LQ_j7dN)}>
        <span>文章分类</span>
      </div>
      <ul className={classNames(styles.two_ul)}>
        {knows.article.map((item, index) => {
          return (
            <NavLink to={`/category/${item.value}`} key={index}>
              <li className={classNames(styles.two_li)}>
                <span>{item.label}</span>
                <span>共计{item.articleCount}篇文章</span>
              </li>
            </NavLink>
          );
        })}
      </ul>
    </div>
  );
};

export default Classification;
