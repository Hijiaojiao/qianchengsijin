import React from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { useEffect, useState } from 'react';

import styles from './blogLayout.less';
import { IRouteProps, useDispatch, useSelector, useIntl, setLocale } from 'umi';
import { keywordInof } from '../types/search';
import { Input, Select } from 'antd';
import Theme from '@/components/Theme';
import cls from 'classnames';

const classNames = require('classnames');

interface Props {
  keyword: keywordInof;
}

const { Search } = Input;

const blogLayout: React.FC = (props) => {
  const intl = useIntl();
  const [flag, setFlag] = useState(false);
  const [value, setValue] = useState('');
  //路由
  const [menuList, setMenuList] = useState([
    {
      title: 'ment.artilce',
      url: '/',
    },
    {
      title: 'ment.archives',
      url: '/archives',
    },
    {
      title: 'ment.knowledge',
      url: '/knowledge',
    },
    {
      title: 'ment.message',
      url: '/page/msgboard',
    },
    {
      title: 'ment.about',
      url: '/page/about',
    },
  ]);

  const [visible, showVisible] = useState(false);
  const [curindex, setcurindex] = useState(0);
  const history = useHistory();
  const dispatch = useDispatch();
  const keyword = useSelector((state: Props) => state.keyword);
  const { language } = useSelector((state: IRouteProps) => {
    return {
      language: state.language,
    };
  });
  useEffect(() => {
    if (value !== '') {
      dispatch({
        type: 'keyword/getkeyWord',
        payload: value,
      });
    }
    setValue('');
  }, [value]);

  function clickTitle(id: string) {
    setFlag(false);
    history.push('/category/article/' + id);
  }

  function changeLocale(locale: string) {
    dispatch({
      type: 'language/save',
      payload: { locale },
    });
    setLocale(locale, false);
  }
  return (
    <div className={styles.header}>
      <div className={styles.menu}>
        {/* <h1> 文章页面</h1> */}
        {/* 路由页面 */}
        <img
          src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png"
          alt=""
        />
      </div>
      <div className={styles.right}>
        <div className={styles.tab}>
          {menuList.map((item, index) => {
            return (
              <NavLink key={item.url} to={item.url}>
                {intl.formatMessage({
                  id: item.title,
                })}
              </NavLink>
            );
          })}
        </div>
        <div className={styles.navIcon}>
          {/* 中汉互译 */}
          <Select
            value={language.locale}
            onChange={(e) => changeLocale(e)}
            style={{ opacity: '0' }}
          >
            {language.locales.map((item) => {
              return (
                <Select.Option key={item.locale} value={item.locale}>
                  {item.lable}
                </Select.Option>
              );
            })}
          </Select>
          <svg
            viewBox="0 0 1024 1024"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            width="1.5em"
            height="1.5em"
            style={{
              position: 'relative',
              right: '30px',
              top: '4px',
              color: '#f00',
            }}
          >
            <path
              d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z"
              fill="currentColor"
            ></path>
          </svg>
          {/* 调色板 */}
          <Theme/>
          {/* 模糊搜素 */}
          <svg
            viewBox="64 64 896 896"
            focusable="false"
            data-icon="search"
            width="1.3em"
            height="1.3em"
            fill="currentColor"
            aria-hidden="true"
            style={{ marginTop: '8px'  ,marginLeft:"10px"}}
            onClick={() => {
              setFlag(true);
            }}
          >
            <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
          </svg>
        </div>
      </div>

      {flag ? (
        <div className={classNames(styles.layer)}>
          <div className={classNames(styles.layerr)}>
            <div className={classNames(styles.topp)}>
              <h1>文章搜索</h1>
              <b onClick={() => setFlag(false)}>X</b>
            </div>
            <br />
            <div className={classNames(styles.search)}>
              <Search
                placeholder="请输入需要查找的内容"
                onSearch={(value) => {
                  setValue(value);
                }}
                enterButton
              />
              {keyword.listt.map(
                (item: {
                  id: string;
                  title:
                    | boolean
                    | React.ReactChild
                    | React.ReactFragment
                    | React.ReactPortal
                    | null
                    | undefined;
                }) => {
                  return (
                    <li>
                      <span
                        onClick={() => {
                          clickTitle(item.id);
                        }}
                      >
                        {item.title}
                      </span>
                    </li>
                  );
                },
              )}
            </div>
          </div>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};
export default blogLayout;
