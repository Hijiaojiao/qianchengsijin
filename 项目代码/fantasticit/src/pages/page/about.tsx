import React from 'react';
import { IRootState } from '@/types'; // 引入类型
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'umi';
import ImageView from '@/components/ImageView'; // 引入图片放大组件
import InputBox from '@/components/InputBox';
import ArticleList from '@/components/articleList/Item';
import Moment from '@/utils/moment'
import { Pagination } from 'antd';
import { MessageOutlined } from '@ant-design/icons';
import styles from './about.less';

// 定义关于页面函数组件
const About: React.FC = () => {
    const dispatch = useDispatch();
    const state = useSelector((state: IRootState) => state.about);
    const article = useSelector((state: IRootState) => state.article);
    let [page, setPage] = useState(1);

    // 请求图文列表数据
    useEffect(() => {
        dispatch({
            type: 'article/getArticleList',
            payload: page,
        });
    }, []);
    // 请求评论数据 分页
    useEffect(() => {
        dispatch({
            type: 'about/getCommentList',
            payload: { page, id: "a5e81ffe-0ad0-4be9-acca-c0462b1b98a1" }
        })
    }, [page]);

    return <div className={styles.about}>
        {/* 上部图文部分 */}
        <div className={styles.container}>
            <ImageView>
                <div className={styles.img}><img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg" alt="" /></div>
                <div className={styles.title}><p>这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活</p></div>
            </ImageView>
        </div>
        <div className={styles.bottom}>
            <div className={styles.comment}>
                <p style={{ textAlign: "center" }}>评论</p>
                {/* 输入框部分 */}
                <InputBox />
                {/* 评论列表部分 */}
                <div>
                    {
                        state.commentList && state.commentList.map(item => {
                            return <div key={item.id} className={styles.comitem}>
                                <header>
                                    <span className={styles.origin} style={{ backgroundColor: '#' + Math.floor(Math.random() * 0xffffff).toString(16) }}>{item.name[0]}</span>
                                    <span style={{ fontWeight: 700 }}>{item.name}</span>
                                </header>
                                <main>
                                    <div dangerouslySetInnerHTML={{ __html: item.content! }}></div>
                                </main>
                                <footer>
                                    <div>
                                        <span>{item.userAgent} · </span>
                                        <span>{Moment(item.createAt, "YYYYMMDD").fromNow()}</span>
                                        <span className={styles.reply}> <MessageOutlined />回复</span>
                                    </div>
                                    <>
                                        {item.children ? item.children.map(item2 => {
                                            return <div key={item2.id} className={styles.footchild}>
                                                <header>
                                                    <span style={{
                                                        width: '24px',
                                                        height: '24px',
                                                        lineHeight: '24px',
                                                        fontSize: '18px',
                                                        display: 'inline-block',
                                                        marginRight: '10px',
                                                        borderRadius: '50%',
                                                        textAlign: 'center',
                                                        color: '#fff',
                                                        backgroundColor: '#' + Math.floor(Math.random() * 0xffffff).toString(16)
                                                    }}>{item2.name.charAt(0)}</span>
                                                    <span style={{ fontWeight: 700 }}>{item2.name}</span>
                                                    <span style={{ margin: "0 8px" }}>回复</span>
                                                    <span style={{ fontWeight: 700 }}>{item2.replyUserName}</span>
                                                </header>
                                                <main>
                                                    <div dangerouslySetInnerHTML={{ __html: item2.content! }}></div>
                                                </main>
                                                <footer>
                                                    <div>
                                                        <span>{item2.userAgent} ·</span>
                                                        <span> {Moment(item2.createAt, "YYYYMMDD").fromNow()}</span>
                                                        <span className={styles.reply}> <MessageOutlined /> 回复</span>
                                                    </div>
                                                </footer>
                                            </div>
                                        }) : ''}
                                    </>
                                </footer>
                            </div>
                        })
                    }
                </div>
                {/* 分页按钮部分 */}
                <div className={styles.pag}>
                    <Pagination defaultCurrent={1} size="small" total={state.commentCount} onChange={(page) => setPage(page)} />
                </div>
            </div>
            {/* 下部图文列表部分 */}
            <div className={styles.lists}>
                <p style={{ textAlign: "center" }}>推荐阅读</p>
                <div style={{ backgroundColor: "var(--bg-second)", maxWidth: "768px", margin: "0 auto", height: "auto", overflowY: "hidden", padding: "0 16px" }}>
                    {article.articleList.map((item) => {
                        return (
                            <ArticleList key={item.id} item={item}></ArticleList>
                        );
                    })}
                </div>
            </div>
        </div>
    </div>
}
export default About;
