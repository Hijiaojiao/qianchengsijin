import React, { useEffect, useState,useRef } from 'react'
import { Link, localeInfo, useDispatch, useSelector } from 'umi'
import styles from './msgboard.less'
import '../../static/font_article/iconfont.css'
import { Avatar, Button, Form, Input, Popover, Pagination } from 'antd';
import { SmileOutlined, MessageOutlined, HeartOutlined, EyeOutlined, ShareAltOutlined, CommentOutlined } from '@ant-design/icons';
import { NavLink } from 'react-router-dom';
import Item from "../../components/articleList/Item"
import { IRootState, plItem } from '../../types'
import { UserOutlined } from '@ant-design/icons';
import { emojis } from '@/types/emojis'
import theTime from '../../services/modules/msg'

const msgboard: React.FC = () => {
    const state = useSelector((state: IRootState) => state.msgboard)
    const dispatch = useDispatch()
    const [popover, setpopover] = useState(false)
    const [user, setuser] = useState(false)
    const about = useSelector((state: IRootState) => state.about);
    let [page, setPage] = useState(1);
    let [text, settext] = useState("");
    let [pagesize, setPagesize] = useState(6);
    const dom = useRef(null)
    //请求列表数据
    useEffect(() => {
        dispatch({
            type: 'about/getAboutRecommend'
        })
    }, []);
    useEffect(() => {
        
        dispatch({
            type: "msgboard/getCommentData",
            payload: page
        })
    }, [page])
    
    // 改变弹窗的出现和隐藏状态
    let VisibleChange = () => {
        console.log(111);
        
        if (localStorage.getItem('user')) {
        
            setpopover(!popover)
        } else {
            setpopover(false)
        }
    }
    // 关闭弹窗
    let close = () => {
        setuser(false)
    }
    //发布按钮的事件
    let submit = () => {
       
        if(text){
            if( localStorage.getItem('user')){

            }else{
                setuser(true)
            }
          
        }else{
            alert("内容不能为空")
        }
        
        
    }
    //设置表情的事件
    let addemojis = (item: string) => {
        console.log(item);
        // console.log(dom.current);
         settext(text+item)
    }
    //将user的状态储存
    const onFinish = (values: any) => {
        localStorage.setItem('user', JSON.stringify(values))
        setuser(false)
    };
    //改变弹窗的状态
    let shade = () => {
        let users = localStorage.getItem('user')
        users ? setuser(false) : setuser(true)
    }
    //用于设置表情选择器的内容
    let content = () => {
        return <ul className={styles.emojis}>
            {
                Object.values(emojis).map((item, index) => {
                    return <li key={index} onClick={() => addemojis(item)}>
                        {item}
                    </li>
                })
            }
        </ul>
    }
    // 分页设置当前页
    const onChange = (page: number) => {
        console.log(page);
        setPage(page)
    }
    return (
        <div className={styles.msgboard}>
            {
                console.log(state.commentData[0])
            }
            <header className={styles.head}>
                <div>
                    <h2>留言板</h2>
                    <p><strong>「请勿灌水 🤖」</strong></p>
                </div>
            </header>
            {
                user ? <div className={styles.shade} onClick={close}>
                    <div onClick={(e) => {
                        e.stopPropagation()
                    }}>
                        <p>
                            <span>请设置你的信息</span>
                            <span onClick={close}>X</span>
                        </p>
                        <Form
                            onFinish={onFinish}
                        >
                            <Form.Item
                                label="名称"
                                name="username"
                                rules={[{ required: true, message: '请输入您的称呼' }]}
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                label="邮箱"
                                name="email"
                                rules={[{ required: true, pattern: /^\w+@\w+\.(com|cn|net)$/, message: '输入合法邮箱地址，以便在收到回复时邮件通知' }]}
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item wrapperCol={{ offset: 8, span: 16 }} style={{ textAlign: 'right' }}>
                                <Button style={{ margin: "0 10px 0 0" }} onClick={close}>
                                    取消
                                </Button>
                                <Button type="primary" htmlType="submit" danger>
                                    设置
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div> : null
            }
            <main>
                <div className={styles.container_up}>
                    <p className={styles.review}>评论</p>
                    <div className={styles.up_content}>
                        {/* 文本域 */}
                        <div className={styles.up_content_text}>
                            <div className={styles.up_content_text_filed}>
                                <div className={styles.filed}>
                                    <textarea className={styles.ant_input} placeholder="请输入评论内容（支持 Markdown）"
                                     value={text} ref={dom}  onChange={(e)=>{settext(e.target.value)}}  ></textarea>
                                </div>
                                <div className={styles.filed_release}>
                                    <span onClick={shade}>
                                        <Popover className={styles.popover} placement="bottomLeft" content={content} visible={popover} onVisibleChange={VisibleChange} trigger="click">
                                            <span className="iconfont icon-xiaolian"></span>
                                            <SmileOutlined />
                                        </Popover>
                                    </span>
                                    <button className={styles.release} onClick={submit}>发布</button>
                                </div>
                            </div>
                        </div>
                        {/* comment数据渲染 */}
                        {
                            state.commentData[0] ? state.commentData[0].map((item: plItem, index: number) => {
                                return (
                                    <div className={styles.itempl} key={index}>

                                        <h4><Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />{item.name}</h4>
                                        <p>{item.content}</p>
                                        <p>{item.userAgent} {item.createAt}<span> <CommentOutlined />回复</span></p>
                                    </div>
                                )
                            }) : ""
                        }
                        <div className={styles.paging}>
                            <Pagination defaultCurrent={page} size="small" total={12} defaultPageSize={6} onChange={onChange} />
                        </div>
                    </div>
                </div>
                <div className={styles.list}>
                    <p>推荐阅读</p>
                    <div className={styles.article}>
                        {
                            about.recommendList && about.recommendList.map(item => {
                                return <NavLink to={`/archives/${item.id}`} key={item.id}>
                                    <Item item={item}></Item>
                                </NavLink>
                            })
                        }
                    </div>
                </div>
            </main>
        </div>
    )
}

export default msgboard


