import React from 'react';
import { TagDetail, IRootState } from '@/types';
import { useEffect, useState } from 'react';
//文章标签的数据
import Lable from '@/components/articleList/Lable';
import ReadingList from '@/components/articleList/ReadingList';
import { useDispatch, useSelector } from 'umi';
// import CategoryItem from '@/components/categoryItem';
import styles from './be.less';
import {
  ShareAltOutlined,
  HeartOutlined,
  EyeOutlined,
} from '@ant-design/icons';
import Moment from '@/utils/moment';
import { IArticleItem } from '@/types';
import share from '@/components/Share';

const Reading: React.FC = () => {
  const dispatch = useDispatch();
  const category = useSelector((state: IRootState) => state.category);
  useEffect(() => {
    dispatch({
      type: 'category/getReading',
    });
  }, []);
  console.log(category.ReadingList);

  function shareArticle(e: React.MouseEvent, item: IArticleItem) {
    e.stopPropagation();
    share(item);
  }

  return (
    <div className={styles.contents}>
      <div className={styles.categoryList}>
        {/* 左面 */}
        <div className={styles.listLeft}>
          <div className={styles.categoryTop}>
            <p>
              <span>阅读</span> 分类文章
            </p>
            <p>
              共搜索到 <span>5</span> 篇
            </p>
          </div>
          <div className={styles.categoryContainer}>
            {/* 导航 */}
            <div className={styles.containerLeft}>
              {category.ReadingList.map((item, index) => {
                return (
                  <div className={styles.leftContent} key={index}>
                    <div className={styles.content}>
                      <div className={styles.contentTop}>
                        <h3>{item.title}</h3>
                        <span>
                          | {Moment(item.createAt, 'YYYYMMDD').fromNow()} |
                        </span>
                        <span>{item.category.label}</span>
                      </div>
                      <div className={styles.content_be}>
                        <div className={styles.img}>
                          {item.cover ? <img src={item.cover} alt="" /> : null}
                        </div>
                        <div className={styles.spans}>
                          <p>{item.summary}</p>
                          <p>
                            <span>
                              <HeartOutlined />
                              {item.likes}
                            </span>
                            <span style={{ margin: '0 8px' }}>·</span>
                            <span>
                              <EyeOutlined />
                              {item.views}
                            </span>
                            <span style={{ margin: '0 8px' }}>·</span>
                            <span onClick={(e) => shareArticle(e, item)}>
                              <ShareAltOutlined />
                              分享
                            </span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className={styles.listRight}>
          {/* 右侧推荐阅读 */}
          <ReadingList></ReadingList>
          <Lable></Lable>
        </div>
      </div>
    </div>
  );
};
export default Reading;
