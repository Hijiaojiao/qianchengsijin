import React from "react";
import styles from "./sid.less"
import { IRootState, DtailObject } from "@/types"
import { IRouteComponentProps, NavLink, useDispatch, useSelector } from "umi"
import { useEffect } from "react"

const knowDtailss: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
    let id = props.match.params.id
    const dispatch = useDispatch();
    const reading = useSelector((state: IRootState) => state.knowledge);
    console.log(reading);
    useEffect(() => {
        dispatch({
            type: "knowledge/getKnowDetailtwo",
            payload: id
        })
    }, [id])
    let arr = reading.knowDtailtwo.toc && JSON.parse(reading.knowDtailtwo.toc?.split("/")[0])
    console.log(arr);
    
    return <div className={styles.container}>
        <div className={styles.container_one}>
            <div className={styles.container_one_box}>
                <div className={styles.container_one_box_two}>
                    <span>
                        <span className="ant-breadcrumb-link"><a href="/knowledge">知识小册</a></span>
                        <span className={styles.ant_breadcrumb_separator}>/</span>
                    </span>
                    <span>
                        <span className="ant-breadcrumb-link">{reading.knowDtail.title}</span>
                        <span className={styles.ant_breadcrumb_separator}>/</span>
                    </span>
                    <span>
                        <span className="ant-breadcrumb-link">{reading.knowDtailtwo.title}</span>
                        {/* <span className={styles.ant_breadcrumb_separator}>/</span> */}
                    </span>
                </div>
            </div>
        </div>
        <div className={styles.section}>
            <div className={styles.article}>
                <div className={styles.title}>
                    <h1>{reading.knowDtailtwo.title}</h1>
                    <p><i>发布于{reading.knowDtailtwo.createAt}<b>.</b>阅读量{reading.knowDtailtwo.views}</i></p>
                </div>
                <div className={styles.markdown} dangerouslySetInnerHTML={{ __html: reading.knowDtailtwo.html! }}>
                </div>
                <p className={styles.publish}>发布于{reading.knowDtailtwo.publishAt}| 版权信息：非商用-署名-自由转载</p>
            </div>

            <div className={styles.aside_big}>
                <div className={styles.aside}>
                    <div className={styles.fingerpost}>
                        <div className={styles.header}>{reading.knowDtailtwo.title}</div>
                        <div className={styles.main}>
                            <ul>
                                {
                                    reading.knowDtail.children && reading.knowDtail.children.map((item: DtailObject,index:number) => {
                                        return <NavLink  key={index} to={`/knowDtail/${id}/${item.id}`}>
                                        <li key={index} className={item.id===id?styles.active:""}>{item.title}</li></NavLink>
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                    <div className={styles.catalogue}>
                            <h5 className={styles.mulu}>目录</h5>
                            <ul>
                                {
                                    arr && arr.map((item: { id: number; text: string; }) => {
                                        return <li key={item.id}>{item.text}</li>
                                    })
                                }
                            </ul>
                        </div>
                </div>
            </div>
            
        
        </div>
    </div>
}

export default knowDtailss