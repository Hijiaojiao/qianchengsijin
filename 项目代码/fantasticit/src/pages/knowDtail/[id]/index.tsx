import React from "react";
import Moment from "@/utils/moment";
import { IRouteComponentProps, useDispatch, useSelector, NavLink } from "umi"
import { useEffect, useState } from "react"
import { IRootState, knowDtail } from "@/types"
import styles from "./knowDtail.less"
// import knowledge from "@/models/knowledge";

const knowDtails: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
    const dispatch = useDispatch();
    const knowledge = useSelector((state: IRootState) => state.knowledge)
    console.log(knowledge, "1111");
    const id = props.match.params.id;
    useEffect(() => {
        dispatch({
            type: "knowledge/getKnowDetail",
            payload: id,
        })
    }, [id])
    let { knowDtail } = knowledge
    let list = knowDtail.children
    // console.log(knowDtail);

    return <div className={styles.detail} >
        <div className={styles.container_one}>
            <div className={styles.container_one_box}>
                <div className={styles.container_one_box_two}>
                    <span>
                        <span className="ant-breadcrumb-link"><a href="/knowledge">知识小册</a></span>
                        <span className={styles.ant_breadcrumb_separator}>/</span>
                    </span>
                    <span>
                        <span className="ant-breadcrumb-link">{knowledge.knowDtail.title}</span>
                        {/* <span className={styles.ant_breadcrumb_separator}>/</span> */}
                    </span>
                </div>
            </div>
        </div>
        <div className={styles.container_two}>
            <div className={styles.container_two_box_one}>
                <div className={styles.box_one}>
                    <header className={styles.box_one_header}>{knowledge.knowDtail.title}</header>
                    <main className={styles.box_one_main}>
                        <div className={styles.main_box}>
                            <div className={styles.main_img}>
                                <img src={knowledge.knowDtail.cover} alt="" />
                            </div>
                            <div className={styles.main_read}>
                                <div className={styles.main_read_box}>
                                    <p className={styles.main_read_box_title}>{knowledge.knowDtail.title}</p>
                                    <p className={styles.main_read_box_title_two}>{knowledge.knowDtail.summary}</p>
                                    <p className={styles.main_read_box_date}>
                                        <span>{knowledge.knowDtail.views}次阅读</span>&nbsp;&nbsp;
                                        <span>·</span>&nbsp;&nbsp;
                                        <span><time dateTime="2021-07-24 13:11:21">{knowledge.knowDtail.publishAt}</time></span>
                                    </p>
                                    {/* <NavLink to={``}> */}
                                        <div className={styles.main_read_box_button} >
                                            <button type="button"><span>开始阅读</span></button>
                                        </div>
                                    {/* </NavLink> */}
                                </div>
                            </div>
                        </div>
                        <ul className={styles.list_ul}>
                            {
                                list && list.map((item: knowDtail, index: number) => {
                                    return <NavLink  key={index} to={`/knowDtail/${id}/${item.id}`}>
                                    <li className={styles.list_li}>
                                        <div className={styles.list_li_title}>{item.title}</div>
                                        <div className={styles.list_li_publishAt}>{item.publishAt}</div>
                                    </li></NavLink>
                                })
                            }
                        </ul>
                    </main>
                </div>

            </div>
            <div className={styles.container_two_box_two}>
                <div className={styles.box_two}>
                    <header className={styles.box_two_header}>其他知识笔记</header>
                    <main className={styles.box_two_main}>
                        {
                            knowledge.knowList.filter(item=>id!=item.id).map((item,index)=>{
                                return <NavLink key={index}  to={`/knowDtail/${item.id}`}>
                                    <li className={styles.main_li}>
                                    <p className={styles.p}>
                                        <span className={styles.p_title}>{item.title}</span>
                                        <span className={styles.p_date}>{Moment(item.createAt).fromNow}</span>
                                    </p>
                                    <div className={styles.context_text}>
                                        <div className={styles.img}>
                                            <img src={item.cover} alt="" />
                                        </div>
                                        <div className={styles.text}>
                                            <p className={styles.text_title}>{item.summary}</p>
                                            <p style={{color: "#8590a6"}}>
                                            <span role="img" aria-label="eye"><svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg></span>
                                            &nbsp;<span>{item.views}</span>&nbsp;&nbsp;
                                                <span>·</span>&nbsp;&nbsp;
                                                <span role="img" aria-label="share-alt" ><svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg></span>
                                                &nbsp;<span>分享</span>
                                            </p>
                                        </div>
                                   </div></li>
                                </NavLink>
                            })
                        }
                    </main>
                </div>
            </div>
        </div>
    </div>
}
export default knowDtails