import React, { useState } from 'react';

interface IListItem{
    id: string;
    content: string;
    finished: boolean;
}
const Home: React.FC = ()=>{
    // 定义状态
    let [content, setContent] = useState('');
    let [list, setList] = useState<IListItem []>([]);

    // 响应回车事件添加todo
    function addList(e: React.KeyboardEvent<HTMLInputElement>){
        if (e.keyCode === 13 && content){
            setList(list=>{
                return [...list, {
                    id: String(+new Date()),
                    content,
                    finished: false
                }]
            })
        }
    }

    // 改变todo状态
    function changeFinished(e: React.ChangeEvent<HTMLInputElement>, id:string){
        let index = list.findIndex(item=>item.id===id);
        let newList = [...list];
        newList[index].finished = e.target.checked;
        setList(newList);
    }

    // 删除todo
    function deleteTodo(id: string){
        let index = list.findIndex(item=>item.id===id);
        let newList = [...list];
        newList.splice(index, 1);
        setList(newList);
    }

    return <div>
        <p>我是二级路由-Home页面</p>
        <header>
            <input type="text" value={content} onChange={e=>setContent(e.target.value)} onKeyDown={addList}/>
        </header>
        <section>{
            list.map(item=>{
                return <li key={item.id}>
                    <span>{item.id}</span>
                    <input type="checkbox" checked={item.finished} id="" onChange={e=>changeFinished(e, item.id)} />
                    <span>{item.content}</span>
                    <button onClick={()=>deleteTodo(item.id)}>删除</button>
                </li>
            })
        }</section>
    </div>
}

export default Home;