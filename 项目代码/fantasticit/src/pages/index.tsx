import { IRootState, tagListItem } from '@/types';
import { useEffect, useState } from 'react';
import {
  useDispatch,
  useSelector,
  IRouteComponentProps,
  useHistory,
} from 'umi';
//文章首页的列表数据
import ArticleList from '@/components/articleList/Item';
//标签导航的组件
import TagItem from '@/components/articleList/TagItem';
import { Carousel } from 'antd';
//文章标签的数据
import Lable from '@/components/articleList/Lable';
import ReadingList from '@/components/articleList/ReadingList';
import styles from './index.less'; // 等于启用了css-module
import './index.less';

const classNames = require('classnames');

export default function IndexPage() {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);
  const history = useHistory();

  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
    });
    //轮播图数据
    dispatch({
      type: 'article/getSwiper',
    });
    //标签数据
  }, []);
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page,
    });
  }, [page]);
  console.log(article.lableList);

  return (
    <div className={styles.index}>
      <div className={styles.container}>
        <div className={styles.swiper}>
          {/* 轮播图数据 */}
          <Carousel className={styles.Carousel} autoplay>
            {article.swiperlist.map((item, index) => {
              return (
                <div
                  className={styles.swiperItem}
                  key={index}
                  onClick={() => history.push(`/archives/${item.id}`)}
                >
                  <div>
                    <h2>{item.title}</h2>
                    <p>
                      <span>{item.views}次阅读</span>
                    </p>
                  </div>
                  <img src={item.cover} alt="" />
                </div>
              );
            })}
          </Carousel>
          {/* 标签数据 */}
          <div className={styles._1rLpyiQ0A_8hxxqRAIg_Ct}>
            <TagItem></TagItem>
            {/* 列表数据 */}
            <div className={styles.context}>
              {article.articleList.map((item) => {
                return <ArticleList key={item.id} item={item} ></ArticleList>;
              })}
            </div>
          </div>
        </div>
        {/* 右侧数据 */}
        <div className={styles.main}>
          {/* 右侧推荐阅读 */}
          <ReadingList></ReadingList>
          <Lable></Lable>
        </div>
      </div>
    </div>
  );
}
