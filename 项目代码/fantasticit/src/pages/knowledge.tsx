import React from 'react';
import { IRootState } from '@/types';
import { knowObject } from '@/types';
import styles from './knowledge.less';
import { NavLink, useDispatch, useSelector } from 'umi';
import { useState } from 'react';
import { useEffect } from 'react';
import moment from '@/components/moment/moment';


const classNames = require('classnames');

const knowledge: React.FC = () => {
  const dispatch = useDispatch();
  const [page, setpage] = useState(1);
  const knows = useSelector((state: IRootState) => state.knowledge);

  useEffect(() => {
    dispatch({
      type: 'knowledge/getknow',
      payload: page,
    });
  }, [page]);

  useEffect(() => {
    dispatch({
      type: 'knowledge/getRecommends',
    });
  }, []);

  useEffect(() => {
    dispatch({
      type: 'knowledge/getArticle',
    });
  }, []);

  return (
    <div className={classNames(styles.main)}>
      <div className={classNames(styles.content)}>
        <div className={classNames(styles.box_left)}>
          <div className={classNames(styles.box_list)}>
            {knows.knowList.map((item, index) => {
              return (
                <NavLink key={index} to={`/knowDtail/${item.id}`}>
                  <div className={classNames(styles.box_list_bor)}>
                    {/* <a href="/knowledge/c9769169-7265-4cb8-a5df-16e25b15dcad"> */}
                    <header className={classNames(styles.box_list_header)}>
                      <div className={classNames(styles.header_title)}>
                        <span className={classNames(styles.height_Color)}>
                          {item.title}
                        </span>
                      </div>
                      <div className={classNames(styles.header_time)}>
                        <span>
                          <time dateTime="2021-07-24 13:11:21">{moment(item.publishAt).fromNow()}</time>
                        </span>
                      </div>
                    </header>
                    <main className={classNames(styles.box_list_main)}>
                      <dl className={classNames(styles.list_dl)}>
                        <dt className={classNames(styles.dt_bj)}>
                          <img
                            src={item.cover}
                            alt="cover"
                            className={classNames(styles.dt_img)}
                          />
                        </dt>
                        <dd className={classNames(styles.dd_bj)}>
                          <div className={classNames(styles.dd_top)}>
                            <div>{item.summary}</div>
                          </div>
                          <div className={classNames(styles.dd_bottom)}>
                            <div className={classNames(styles.dd_bottom_left)}>
                              <span role="img" aria-label="eye">
                                <svg
                                  viewBox="64 64 896 896"
                                  focusable="false"
                                  data-icon="eye"
                                  width="1em"
                                  height="1em"
                                  fill="currentColor"
                                  aria-hidden="true"
                                >
                                  <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                                </svg>
                              </span>
                              <span>{item.views}</span>
                            </div>
                            <div className={classNames(styles.dd_bottom_right)}>
                              <span role="img" aria-label="share-alt">
                                <svg
                                  viewBox="64 64 896 896"
                                  focusable="false"
                                  data-icon="share-alt"
                                  width="1em"
                                  height="1em"
                                  fill="currentColor"
                                  aria-hidden="true"
                                >
                                  <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                                </svg>
                              </span>
                              <span>分享</span>
                            </div>
                          </div>
                        </dd>
                      </dl>
                    </main>
                    {/* </a> */}
                  </div>
                </NavLink>
              );
            })}
          </div>
        </div>



        <div className={classNames(styles.box_big_right)}>
          <div className={classNames(styles.box_right)}>
            <div className={classNames(styles.box_right_one)}>
              <div className={classNames(styles._1hh60i8CByuosza1JbgE0Q)}>
                <span>推荐阅读</span>
              </div>
              <div className={classNames(styles.one_box)}>
                <div className={classNames(styles.two_box)}>
                  <ul className={classNames(styles._1PytTgSelvgOTMZR4ckInB)}>
                    {knows.recommend.map((item, index) => {
                      return (
                        <li key={index}>
                          {/* <a href="/article/aa5104bd-f6dd-4e85-a1a0-de60de24b0ad"> */}
                          <span>{item.title}</span>&nbsp;&nbsp;·&nbsp;&nbsp;
                          <span>
                            <time dateTime={item.createAt}>{moment(item.publishAt).fromNow()}</time>
                          </span>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </div>
            </div>
            <div className={classNames(styles.box_right_two)}>
              <div className={classNames(styles._2b0LQ_j7dN)}>
                <span>文章分类</span>
              </div>
              <ul className={classNames(styles.two_ul)}>
                {knows.article.map((item, index) => {
                  return (
                    <li className={classNames(styles.two_li)} key={index}>
                      <span>{item.label}</span>
                      <span>共计{item.articleCount}篇文章</span>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default knowledge;
