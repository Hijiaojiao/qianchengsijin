import React, { useRef } from 'react';
// 引入类型
import { IRootState } from '@/types';

import { useEffect, useState } from 'react';
import { IRouteComponentProps, useDispatch, useSelector } from 'umi';
import ImageView from '@/components/ImageView';

import styles from './detail.less';
// 引入右侧推荐组件
import ReadingList from '@/components/articleList/ReadingList';
import "../../less/markdown.less"
// 日期处理类库
import moment from "moment"

interface IArr {
  id: string,
  level: string,
  text: string
}


const Detail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  const dispatch = useDispatch();
  //获取归档详情数据
  const { archivesDetail } = useSelector((state: IRootState) => state.archives);
  //获取右侧详情目录
  const catalogueList = useSelector((state: IRootState) => state.archives.catalogueList)
  // console.log(catalogueList);
  // console.log(catalogueList.toc);
  // 获取id
  let id = props.match.params.id;

  let catalogueLists = []
  if (typeof catalogueList.toc == 'string') {
    catalogueLists = JSON.parse(catalogueList.toc)
  }
  const [ind, setInd] = useState('')
  let html: any = []
  useEffect(() => {
    setInd(props.match.params.id)
    html = [
      ...document.querySelectorAll(
        `section h1,section h2,section h3,section h4,section h5,section h6`
      )
    ]
  }, [props])

  useEffect(() => {
    document.querySelector('#root')?.lastElementChild?.addEventListener('scroll', (e: any) => {
      html.forEach((ite: any) => {
        if (e.target.scrollTop > (ite as HTMLElement).offsetTop - 64) {
          ite.id && setInd(ite.id)
        }

      })
    })
  }, [html])


  useEffect(() => {
    dispatch({
      type: 'archives/getArchivesDetail',
      payload: id,
    });
  }, []);

  useEffect(() => {
    dispatch({
      type: "archives/getCatalogue",
      payload: id
    })
  }, [])

  const toolbar = (id: string) => {
    document.getElementById(id)!.scrollIntoView({
      behavior: "smooth",
      block: "start"
    })
  }

  return (
    <ImageView>
      <div className={styles.archiveDeatil}>
        <div className={styles.left}>
          <div className={styles.content}>
            <div className={styles.header}>
              <img
                src={archivesDetail.cover ? archivesDetail.cover : ''}
                alt=""
              />
              <h1>{archivesDetail.title}</h1>
              <p>
                <span>
                  发布于
                  <time>
                    {moment(archivesDetail.createAt).format(
                      'YYYY-MM-DD HH:mm:ss',
                    )}
                  </time>
                </span>
                <span> • </span>
                <span>阅读量 {archivesDetail.views}</span>
              </p>
            </div>
            <div className="markdown">
              <div
                dangerouslySetInnerHTML={{ __html: archivesDetail.html! }}
              ></div>
            </div>
          </div>


        </div>

        <div className={styles.rights_big}>
          <div className={styles.rights}>
            <ReadingList></ReadingList>
            <div className={styles.catalogue}>
              <div className={styles.cataloguebox}>目录</div>
              <ul>
                {
                  catalogueLists && catalogueLists.map((item: IArr) => {
                    return (
                      <li onClick={() => toolbar(item.id)}>{item.text}</li>
                    )
                  })
                }
              </ul>
            </div>
          </div>
        </div>



      </div>






    </ImageView>
  );
};
export default Detail;
