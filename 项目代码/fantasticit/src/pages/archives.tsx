import React from 'react';
import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { Link, useDispatch, useSelector, NavLink } from 'umi';
//引入样式
import style from './archives.less';
//引入推荐阅读组件
import ReadingList from '../components/articleList/ReadingList';
import Classification from "./../layouts/Classification"

export default function archives() {
  const dispatch = useDispatch();
  const archives = useSelector((state: IRootState) => state.archives.archives);
  // console.log(archives);
  useEffect(() => {
    dispatch({
      type: 'archives/getArchives',
    });
  }, []);

  let totalNum = 0;
  for (let key in archives) {
    for (let item in archives[key]) {
      totalNum += archives[key][item].length;
    }
  }
  return (
    <section className={style.archives}>
      <div className={style.left}>
        <div className={style.box}>
          <div className={style.header}>
            <h2 className="h2">归档</h2>
            <h5>
              共<span className={style.p}>{totalNum}</span>篇
            </h5>
          </div>
        </div>

        <div className={style.main}>
          {
            <ul>
              <li>
                {Object.keys(archives)
                  .sort((a, b) => Number(b) - Number(a))
                  .map((val, idx) => {
                    return (
                      <div key={val} className={style.mainbox}>
                        <h2 className={style.mainh2}>{val}</h2>
                        {Object.keys(archives[val]).map((v, i) => {
                          return (
                            <div
                              key={v}
                              className={style._172XrbegThYDU7WfEHTUpM}
                            >
                              <h3 className={style.mainh3}>{v}</h3>
                              <ul className={style.uls}>
                                {archives[val][v].map((item, index) => {
                                  return (
                                    <NavLink
                                      key={index}
                                      to={`/archives/${item.id}`}
                                    >
                                      <li className={style.lis}>
                                        <span className={style.lisspan}>
                                          <span>·</span>
                                          {item.createAt.slice(5, 10)}
                                        </span>
                                        <span className={style.content}>
                                          {item.title}
                                        </span>
                                      </li>
                                    </NavLink>
                                  );
                                })}
                              </ul>
                            </div>
                          );
                        })}
                      </div>
                    );
                  })}
              </li>
            </ul>
          }
        </div>
      </div>

      <div className={style.right}>
        <ReadingList></ReadingList>
        <Classification></Classification>
      </div>




    </section>
  );
}
