import { defineConfig } from 'umi';
// const px2rem = require('postcss-px2rem');

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  // 引入antd
  antd: {},
  // 引入dva
  dva: {
    immer: true,
    hmr: true,
  },
  //引入国际化
  locale: {},

  //publicPath:'./'
  //配置服务器上的子路径
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/1812B/wangjiaojiao/fantasticit/'
      : '/',
  //配置路由前缀
  base: '/1812B/wangjiaojiao/fantasticit',
  // 配置路由按需加载
  dynamicImport: {
    loading: '@/layouts/blogLayout',
  },
  //文件名加上hash后缀
  hash: true,
  //加上百度统计
  // analytics: {
  //   baidu: '5a66cxxxxxxxxxx9e13',76
  // },
  // 响应式 配置自适应http和https
  // extraPostCSSPlugins: [px2rem({remUnit: 75})],
  // scripts: ["//g.tbcdn.cn/mtb/lib-flexible/0.3.4/??flexible_css.js,flexible.js"],
  // 配置服务器上的子路径
  //路由懒加载的配置
  // hash: true,
});
